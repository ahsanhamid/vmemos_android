package push;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.vmemos.inc.R;
import com.vmemos.inc.FriendChatActivity;

import Model.GetVocals;
import Model.UpdateTokenRes;
import Util.AppManager;
import Util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GcmIntentService extends IntentService {

	public static int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	String TAG="GCM";

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		Log.i("MessageType ==== ",messageType);

//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//		try {
//			InstanceID myID = InstanceID.getInstance(this);
//			String registrationToken = null;
//
//			registrationToken = myID.getToken(
//					getString(R.string.gcm_defaultSenderId),
//					GoogleCloudMessaging.INSTANCE_ID_SCOPE,
//					null
//			);
//
//
//
//			sendRegistrationToServer(registrationToken);
//
////			// Subscribe to topic channels
////			subscribeTopics(registrationToken);
//
//			sharedPreferences.edit().putBoolean(Constants.SENT_TOKEN_TO_SERVER, true).apply();
//
//			Log.v("REGISTER", "GCM Registration Token: " + registrationToken);
//
//		} catch (IOException e) {
//			e.printStackTrace();
//			sharedPreferences.edit().putBoolean(Constants.SENT_TOKEN_TO_SERVER, false).apply();
//
//		}
//
//		// Notify UI that registration has completed, so the progress indicator can be hidden.
//		Intent registrationComplete = new Intent(Constants.REGISTRATION_COMPLETE);
//		LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);





		if (extras!=null && !extras.isEmpty()) {  // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that GCM
			 * will be extended in the future with new message types, just ignore
			 * any message types you're not interested in, or that you don't
			 * recognize.
			 */

			Log.i("Intent Data ==== ",extras.toString());

			sendNotification(extras);

//			if (GoogleCloudMessaging.
//					MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
////				sendNotification("Send error: " + extras.toString(),extras.getString("title"), messageType, messageType);
//				sendNotification(extras);
//			} else if (GoogleCloudMessaging.
//					MESSAGE_TYPE_DELETED.equals(messageType)) {
//
//				sendNotification(extras);
////				sendNotification("Deleted messages on server: " +
////						extras.toString(),extras.getString("title"), messageType, messageType);
//				// If it's a regular GCM message, do some work.
//			} else if (GoogleCloudMessaging.
//					MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//				// This loop represents the service doing some work.
//				/* for (int i=0; i<5; i++) {
//                    Log.i(TAG, "Working... " + (i+1)
//                            + "/5 @ " + SystemClock.elapsedRealtime());
//                    try {
//                        Thread.sleep(5000);
//                    } catch (InterruptedException e) {
//                    }
//                }*/
//				// Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
//				// Post notification of received message.
//				sendNotification(extras.getString("message"),extras.getString("title"),
//						extras.getString("notification_type"),extras.getString("fyt_notification_id"));
//				Log.i(TAG, "Received: " + extras.toString());
//			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
//	private void sendNotification(String msg,String title, String notification_type,
//			String notification_id) {
//
//		mNotificationManager = (NotificationManager)
//				this.getSystemService(Context.NOTIFICATION_SERVICE);
//		Intent notificationIntent;
//		/*  if(com.lookingfor.utils.Settings.getIsUserLoggedIn(this)){
//        	notificationIntent = new Intent(this, Splash.class);
//        //	notificationIntent.putExtra("screen", "notif");
//		 */       // }else{
//		notificationIntent = new Intent(this, Parent.class);
//		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		notificationIntent.putExtra("notification_id", notification_id);
//		notificationIntent.putExtra("type", notification_type);
//		notificationIntent.putExtra("Deleteable", "shouldbedeleted");
//		// }
//		com.fytfriends.android.utils.Settings.setIsPush(true, this);
//		//PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
//		///		notificationIntent, 0);
//		int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
//		NOTIFICATION_ID = uniqueInt;
//        PendingIntent contentIntent = PendingIntent.getActivity(this, uniqueInt, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//		// notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
//		NotificationCompat.Builder mBuilder =
//				new NotificationCompat.Builder(this)
//				.setContentTitle(title)
//				.setStyle(new NotificationCompat.BigTextStyle()
//						.bigText(msg))
//				.setContentText(msg)
//				.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//		mBuilder.setContentIntent(contentIntent);
//		mBuilder.setAutoCancel(true);
//		mBuilder.setTicker(msg);
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//			mBuilder.setColor(Color.parseColor("#444444"));
//			mBuilder.setSmallIcon(R.drawable.ic_notif_icon);
//		}else{
//			mBuilder.setSmallIcon(R.drawable.ic_launcher);
//		}
//		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
//	}


	private void sendRegistrationToServer(final String registrationToken) {

		String userId = "";
		SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
		if (pref.contains(Constants.kUserID)) {
			userId = pref.getString(Constants.kUserID, "");
		}

		Handler handler = new Handler(GcmIntentService.this.getMainLooper());
		final String finalUserId = userId;
		Runnable runnable = new Runnable() {
			@Override
			public void run() {

				AppManager.getInstance().restAPI.sendToken(finalUserId, registrationToken, Constants.DEVICE_TYPE, new Callback<UpdateTokenRes>() {
					@Override
					public void success(UpdateTokenRes updateTokenRes, Response response) {

						if (updateTokenRes.getErrorCode() == 1){
							Log.i("Registration","Token sent");
						}
						else {
							Log.i("Registration","Token sent Failed");
						}

					}

					@Override
					public void failure(RetrofitError error) {
						Log.i("Registration","Token sent Failed");
					}
				});
			}
		};
		handler.post(runnable);
	}

//	// [START subscribe_topics]
//	private void subscribeTopics(String token) throws IOException {
//		GcmPubSub pubSub = GcmPubSub.getInstance(this);
//		for (String topic : TOPICS) {
//			pubSub.subscribe(token, "/topics/" + topic, null);
//		}
//	}
//	// [END subscribe_topics]


	private void sendNotification(Bundle data) {


		SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
		if (pref.contains(Constants.kUserName) && pref.getString(Constants.kUserStatus, "").equalsIgnoreCase(Constants.kUserActive)) {
			Log.i("Notification", "sendNotification");

//		String threadId, frndId, isGroup, frndImage, groupImage, adminId, message;
//		threadId = data.getString("thread_id", "");
//		frndId = data.getString("friend_id", "");
//		isGroup = data.getString("is_group", "");
//		frndImage = data.getString("friend_img", "");
//		groupImage = data.getString("group_img", "");
//		adminId = data.getString("admin_id", "");
//		message = data.getString("message");
//
//
//		GetVocals obj = new GetVocals();
//			obj.setThreadId(threadId);
//			obj.setIsGroup(isGroup);
//			obj.setImage(frndImage);
//			obj.setGroupImage(groupImage);
//			obj.setAdminId(adminId);
//			obj.setReceiverId(frndId);
//
//			Gson gson = new Gson();
//			String value = gson.toJson(obj);
//
//
//		mNotificationManager = (NotificationManager)
//				this.getSystemService(Context.NOTIFICATION_SERVICE);
//
//		int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
//		NOTIFICATION_ID = uniqueInt;
//
//		Intent intent = new Intent(this, FriendChatActivity.class);
//		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		intent.putExtra("VOCAL", value);
//		intent.putExtra("FROM_PUSH", true);
//		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//				PendingIntent.FLAG_ONE_SHOT);
//
//		NotificationCompat.Builder mBuilder =
//				new NotificationCompat.Builder(this)
//						.setContentTitle("VMemos")
//						.setStyle(new NotificationCompat.BigTextStyle()
//								.bigText(""))
//						.setContentText("")
//						.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//		mBuilder.setContentIntent(pendingIntent);
//		mBuilder.setAutoCancel(true);
//		mBuilder.setTicker("");
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//			mBuilder.setColor(Color.parseColor("#444444"));
//			mBuilder.setSmallIcon(R.mipmap.logo);
//		}else{
//			mBuilder.setSmallIcon(R.mipmap.logo);
//		}
//
//
//		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


			//original
			boolean isChatAct = AppManager.getInstance().isChatActive();
			boolean isVocalAct = AppManager.getInstance().isVocalActive();

			String threadId, frndId, isGroup, frndImage, groupImage, adminId, message, senderName, chatTitle, sound = "";
			threadId = data.getString("thread_id", "");
			frndId = data.getString("friend_id", "");
			isGroup = data.getString("is_group", "");
			frndImage = data.getString("friend_img", "");
			groupImage = data.getString("group_img", "");
			adminId = data.getString("admin_id", "");
			message = data.getString("message");
			senderName = data.getString("sender_name");
			chatTitle = data.getString("chat_title");
			sound = data.getString("sound");

			//if you want to show any dialog directly.
			if (!isChatAct) {

				if (isVocalAct) {
					Intent registrationComplete = new Intent("vocal");
					LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
				}

//			Intent i = new Intent(this, PopUpActivity.class);
//			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			i.putExtra("BUNDLE", data);
//			startActivity(i);

				GetVocals obj = new GetVocals();
				obj.setThreadId(threadId);
				obj.setIsGroup(isGroup);
				obj.setImage(frndImage);
				obj.setGroupImage(groupImage);
				obj.setAdminId(adminId);
				obj.setReceiverId(frndId);
				obj.setName(senderName);
				obj.setGroupName(chatTitle);

				Gson gson = new Gson();
				String value = gson.toJson(obj);

				Intent intent = new Intent(this, FriendChatActivity.class);
//			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("VOCAL", value);
				intent.putExtra("FROM_PUSH", true);
				PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
						PendingIntent.FLAG_UPDATE_CURRENT); //FLAG_ONE_SHOT

//			Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


				mNotificationManager = (NotificationManager)
						this.getSystemService(Context.NOTIFICATION_SERVICE);

				int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
				NOTIFICATION_ID = uniqueInt;

				NotificationCompat.Builder mBuilder =
						new NotificationCompat.Builder(this)
								.setContentTitle("VMemos")
//							.setStyle(new NotificationCompat.BigTextStyle()
//									.bigText(message))
//							.setDefaults(Notification.DEFAULT_SOUND)
								.setContentText(message)
//							.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
								.setPriority(NotificationCompat.PRIORITY_MAX)
//							.setVibrate(new long[]{1, 1, 1})
								.setCategory(NotificationCompat.CATEGORY_MESSAGE);
				mBuilder.setContentIntent(pendingIntent);
				mBuilder.setAutoCancel(true);
				mBuilder.setTicker(message);


				AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

				switch (am.getRingerMode()) {
					case AudioManager.RINGER_MODE_SILENT:
						Log.i("MyApp", "Silent mode");
						break;
					case AudioManager.RINGER_MODE_VIBRATE:
						Log.i("MyApp", "Vibrate mode");
						mBuilder.setVibrate(new long[]{1, 1, 1});
						break;
					case AudioManager.RINGER_MODE_NORMAL:
						Log.i("MyApp", "Normal mode");

						try {

							mBuilder.setVibrate(new long[]{1, 1, 1});

							if (sound.split("\\.")[0].equalsIgnoreCase("tri-tone") || sound.split("\\.")[0].equalsIgnoreCase("bell")
									|| sound.split("\\.")[0].equalsIgnoreCase("glass")) {
								sound = sound.split("\\.")[0] + ".mp3";
							}

							MediaPlayer player = new MediaPlayer(); //MediaPlayer.create(AlertToneActivity.this,resID);
							String name = "alert_sound/" + sound.toLowerCase();
							AssetFileDescriptor descriptor = getAssets().openFd(name);
							player.reset();
							player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
							descriptor.close();

							player.prepare();
							player.start();

						} catch (Exception e) {
							e.printStackTrace();
//							Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
						}

						break;
				}


//			mBuilder.setFullScreenIntent(pendingIntent, true);

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

					mBuilder.setColor(Color.parseColor("#3ED037"));//getResources().getColor(android.R.color.transparent)
					mBuilder.setSmallIcon(R.mipmap.push);
//					Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.logo__);
//					mBuilder.setLargeIcon(bm);
					mBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
				} else {
					mBuilder.setSmallIcon(R.mipmap.push);
//					Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.logo__);
//					mBuilder.setLargeIcon(bm);
				}
				mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


//			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//					.setSmallIcon(R.mipmap.logo)
//					.setContentTitle("VMemos")
//					.setContentText(message)
//					.setAutoCancel(true)
//					.setSound(defaultSoundUri)
//					.setContentIntent(pendingIntent);
//
//			NotificationManager notificationManager =
//					(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//			notificationManager.notify("", 11 /* ID of notification */, notificationBuilder.build());

			} else {

				if (threadId.equalsIgnoreCase(AppManager.getInstance().getCurrentChatThreadID())) {
					GetVocals obj = new GetVocals();
					obj.setThreadId(threadId);
					obj.setIsGroup(isGroup);
					obj.setImage(frndImage);
					obj.setGroupImage(groupImage);
					obj.setAdminId(adminId);
					obj.setReceiverId(frndId);

					AppManager.getInstance().setChatPuchData(obj);

					Intent registrationComplete = new Intent("push");
					LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
				}


			}

		}
	}

}
