package push;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

///import com.lookingfor.model.BasicFields;
//import com.lookingfor.network.NetworkCommunicator;
//import com.lookingfor.utils.Settings;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
	Context c;
	@Override
	public void onReceive(Context context, Intent intent) {
		// Explicitly specify that GcmIntentService will handle the intent.
		ComponentName comp = new ComponentName(context.getPackageName(),
				GcmIntentService.class.getName());
		// Start the service, keeping the device awake while it is launching.
		startWakefulService(context, (intent.setComponent(comp)));
		setResultCode(Activity.RESULT_OK);
		Log.e("GCM", "onReceiveCalled()");
		c = context;
		// new AsyncGetPropertyOptions().execute();
	}
	/*	class AsyncGetPropertyOptions extends AsyncTask<String, Integer, BasicFields>{
		String mode;
		@Override
		protected void onPreExecute() {
			//showProgressDialog("", "Processing ...");
			super.onPreExecute();
		}
		@Override
		protected BasicFields doInBackground(String... params) {
			NetworkCommunicator comm = new NetworkCommunicator(c);
			return comm.GetBasicFields();
		}
		@Override
		protected void onPostExecute(BasicFields result) {
			super.onPostExecute(result);
			if(result!=null){
				if(result.error.equalsIgnoreCase("0")){
					Settings.setPropertyFields(result, c);
				}
			}
			//stopProgressDialog();
		}
	}*/

}
