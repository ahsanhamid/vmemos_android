package push;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by developerclan on 07/03/2016.
 */
public class TokenRefreshListenerService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        if (checkPlayServices()) {
//            Intent intent = new Intent(this, GcmIntentService.class);
//            startService(intent);
        }
        // [END refresh_token]
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
//            if (apiAvailability.isUserResolvableError(resultCode)) {
//                apiAvailability.getErrorDialog(this, resultCode, Constants.PLAY_SERVICES_RESOLUTION_REQUEST)
//                        .show();
//            } else {
//                Log.i("TabActivity", "This device is not supported.");
//                finish();
//            }
            return false;
        }
        Log.i("TokenRefresh", "This device is not supported.");
        return true;
    }

}
