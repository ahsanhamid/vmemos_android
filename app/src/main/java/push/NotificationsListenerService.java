package push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;
import com.vmemos.inc.R;
import com.vmemos.inc.FriendChatActivity;
import com.vmemos.inc.PopUpActivity;

import Model.GetVocals;
import Util.AppManager;

/**
 * Created by developerclan on 07/03/2016.
 */
public class NotificationsListenerService extends GcmListenerService {

    private static final String TAG = "NotificationsListener";

    @Override
    public void onMessageReceived(String from, Bundle data) {

        Log.i(TAG, "Intent Data: " + data.toString());

//        Toast.makeText(getApplicationContext(),"Intent Data: " + data.toString(),Toast.LENGTH_LONG).show();

        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(data);
        // [END_EXCLUDE]

    }


    private void sendNotification(Bundle data) {

        boolean isChatAct = AppManager.getInstance().isChatActive();
        boolean isVocalAct = AppManager.getInstance().isVocalActive();

        String threadId, frndId, isGroup, frndImage, groupImage, adminId, message;
        threadId = data.getString("thread_id", "");
        frndId = data.getString("friend_id", "");
        isGroup = data.getString("is_group", "");
        frndImage = data.getString("friend_img", "");
        groupImage = data.getString("group_img", "");
        adminId = data.getString("admin_id", "");
        message = data.getString("message");

        //if you want to show any dialog directly.
        if (!isChatAct) {

            if (isVocalAct){
                Intent registrationComplete = new Intent("vocal");
                LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
            }

            Intent i = new Intent(this, PopUpActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("BUNDLE", data);
            startActivity(i);

            GetVocals obj = new GetVocals();
            obj.setThreadId(threadId);
            obj.setIsGroup(isGroup);
            obj.setImage(frndImage);
            obj.setGroupImage(groupImage);
            obj.setAdminId(adminId);
            obj.setReceiverId(frndId);

            Gson gson = new Gson();
            String value = gson.toJson(obj);

            Intent intent = new Intent(this, FriendChatActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("VOCAL", value);
            intent.putExtra("FROM_PUSH", true);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.logo)
                    .setContentTitle("VMemos")
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify("", 11 /* ID of notification */, notificationBuilder.build());

        } else {

            if (threadId.equalsIgnoreCase(AppManager.getInstance().getCurrentChatThreadID())) {
                GetVocals obj = new GetVocals();
                obj.setThreadId(threadId);
                obj.setIsGroup(isGroup);
                obj.setImage(frndImage);
                obj.setGroupImage(groupImage);
                obj.setAdminId(adminId);
                obj.setReceiverId(frndId);

                AppManager.getInstance().setChatPuchData(obj);

                Intent registrationComplete = new Intent("push");
                LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
            }


        }

    }
}
