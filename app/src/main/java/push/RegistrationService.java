package push;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.vmemos.inc.R;

import java.io.IOException;

import Model.UpdateTokenRes;
import Util.AppManager;
import Util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by developerclan on 07/03/2016.
 */
public class RegistrationService extends IntentService {

    private static final String[] TOPICS = {"global"};

    public RegistrationService() {
        super("RegistrationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
        InstanceID myID = InstanceID.getInstance(this);
        String registrationToken = null;

            registrationToken = myID.getToken(
                    getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                    null
            );



            sendRegistrationToServer(registrationToken);

            // Subscribe to topic channels
            subscribeTopics(registrationToken);
            
            sharedPreferences.edit().putBoolean(Constants.SENT_TOKEN_TO_SERVER, true).apply();

            Log.v("REGISTER", "GCM Registration Token: " + registrationToken);

        } catch (IOException e) {
            e.printStackTrace();
            sharedPreferences.edit().putBoolean(Constants.SENT_TOKEN_TO_SERVER, false).apply();

        }

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Constants.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

//        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }


    //    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        Toast.makeText(this, "onTaskRemoved ", Toast.LENGTH_SHORT).show();
//        Intent restartService = new Intent(getApplicationContext(),
//                this.getClass());
//        restartService.setPackage(getPackageName());
//        PendingIntent restartServicePI = PendingIntent.getService(
//                getApplicationContext(), 1, restartService,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        //Restart the service once it has been killed android
//
//
//        AlarmManager alarmService = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 100, restartServicePI);
//    }

    private void sendRegistrationToServer(final String registrationToken) {

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)) {
            userId = pref.getString(Constants.kUserID, "");
        }

        Handler handler = new Handler(RegistrationService.this.getMainLooper());
        final String finalUserId = userId;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                AppManager.getInstance().restAPI.sendToken(finalUserId, registrationToken, Constants.DEVICE_TYPE, new Callback<UpdateTokenRes>() {
                    @Override
                    public void success(UpdateTokenRes updateTokenRes, Response response) {

                        if (updateTokenRes.getErrorCode() == 1){
                            Log.i("Registration","Token sent");
                        }
                        else {
                            Log.i("Registration","Token sent Failed");
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.i("Registration","Token sent Failed");
                    }
                });
            }
        };
        handler.post(runnable);
    }

    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]
}
