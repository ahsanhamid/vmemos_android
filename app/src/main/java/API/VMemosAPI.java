package API;

import java.util.List;

import Model.CaptchaRes;
import Model.CreateGroupRes;
import Model.CreateThreadRes;
import Model.DefaultRes;
import Model.FrndRequestRes;
import Model.GetUserGroupCurrentStatusRes;
import Model.GetVocalsRes;
import Model.GroupInfoRes;
import Model.PostVocalRes;
import Model.RecieveThreadRes;
import Model.RegisterRes;
import Model.SearchFrndsRes;
import Model.UpdateGroupRes;
import Model.UpdateSettingRes;
import Model.UpdateTokenRes;
import Util.Constants;
import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by devclan on 09/09/2015.
 */
public interface VMemosAPI {
    public String API = Constants.API_BASE_ADDRESS;

    @Multipart
    @POST("/registerUser")
    public void registerUser(@Part("phone_number") String phoneNunber,
                             @Part("username") String userName,
                             @Part("password") String pass,
                             @Part("email") String email,
                             @Part("name") String name,
                             @Part("token") String token,
                             @Part("device_type") String deviceType,
                             @Part("photo") TypedFile file,
                             Callback<RegisterRes> response);

    @POST("/refreshCaptcha")
    public void refreshCaptcha(@Query("user_id") String userId,
                               Callback<CaptchaRes> response);

    @POST("/verifyCaptcha")
    public void verifyCaptcha(@Query("user_id") String userId,
                              @Query("image_ids[]") List<String> ids,
                              Callback<DefaultRes> response);

    @POST("/loginApplicationUser")
    public void loginUser(@Query("username") String userName,
                          @Query("password") String pass,
                          @Query("token") String token,
                          @Query("device_type") String deviceType,
                          Callback<RegisterRes> response);

    @POST("/getAllFriendsAndRequests")
    public void getAllFrndsRequest(@Query("user_id") String userId,
                                   Callback<FrndRequestRes> response);

    @POST("/acceptFriendRequest")
    public void acceptFrndRequest(@Query("user_id") String userId,
                                  @Query("request_id") String reqId,
                                  Callback<DefaultRes> response);

    @POST("/rejectFriendRequest")
    public void rejectFrndRequest(@Query("user_id") String userId,
                                  @Query("request_id") String reqId,
                                  Callback<DefaultRes> response);

    @POST("/unFriend")
    public void removeFriend(@Query("user_id") String userId,
                             @Query("friend_id") String frndId,
                             Callback<DefaultRes> response);

    @POST("/searchUsersByUsername")
    public void searchFriends(@Query("username") String userName,
                              Callback<SearchFrndsRes> response);

    @POST("/sendFriendRequest")
    public void sendFriendReq(@Query("user_id") String userId,
                              @Query("friend_id") String frndId,
                              Callback<DefaultRes> response);

    @POST("/getAllVocals")
    public void getAllVocals(@Query("user_id") String userId,
                             Callback<GetVocalsRes> response);

    @POST("/receiveThread")
    public void receiveThread(@Query("user_id") String userId,
                              @Query("thread_id") String threadId,
                              @Query("friend_id") String frndId,
                              Callback<RecieveThreadRes> response);

    @POST("/updateVocalStatus")
    public void updateMsgStatus(@Query("user_id") String userId,
                                @Query("vocal_id") String msgId,
                                @Query("status") String status,
                                Callback<DefaultRes> response);

    @Multipart
    @POST("/postVocal")
    public void postVocal(@Query("sender_id") String userId,
                          @Query("receiver_id") String recId,
                          @Query("thread_id") String threadId,
                          @Part("audio_file") TypedFile file,
                          Callback<PostVocalRes> response);

    @POST("/getGroupInfo")
    public void getGroupInfo(@Query("thread_id") String threadId,
                             Callback<GroupInfoRes> response);

    @POST("/makeGroupAdmin")
    public void makeGroupAdmin(@Query("thread_id") String threadId,
                             @Query("user_id") String frndId,
                             Callback<GroupInfoRes> response);

    @POST("/getUserGroupCurrentStatus")
    public void getUserGroupCurrentStatus(@Query("thread_id") String threadId,
                               @Query("user_id") String userId,
                               Callback<GetUserGroupCurrentStatusRes> response);

    @Multipart
    @POST("/updateGroupSettings")
    public void createGroup(@Part("admin_id") String adminId,
                            @Part("user_id") String userId,
                            @Part("group_name") String name,
                            @Query("thread_id") String threadId,
                            @Query("participant_ids") String ids,
                            @Part("group_image") TypedFile file,
                              Callback<CreateGroupRes> response);

    @Multipart
    @POST("/updateGroupSettings")
    public void updateGroup(@Part("admin_id") String adminId,
                            @Part("user_id") String userId,
                            @Part("group_name") String name,
                            @Query("thread_id") String threadId,
                            @Query("is_image_del") String isImgDel,
                            @Query("participant_ids") String ids,
                            @Query("newly_added_ids") String new_ids,
                            @Query("removed_ids") String remove_ids,
                            @Part("group_image") TypedFile file,
                            Callback<CreateGroupRes> response);

    @Multipart
    @POST("/updateGroupSettings")
    public void updateGroupWithoutImage(@Part("admin_id") String adminId,
                            @Part("user_id") String userId,
                            @Part("group_name") String name,
                            @Query("thread_id") String threadId,
                            @Query("participant_ids") String ids,
                            @Query("newly_added_ids") String new_ids,
                            @Query("removed_ids") String remove_ids,
                            Callback<CreateGroupRes> response);

    @Multipart
    @POST("/updateGroupIconAndImage")
    public void updateGroupIconAndImage(@Part("admin_id") String adminId,
                                        @Part("user_id") String userId,
                                        @Part("group_name") String name,
                                        @Query("thread_id") String threadId,
                                        @Query("is_image_del") String isImgDel,
                                        @Part("group_image") TypedFile file,
                                        Callback<UpdateGroupRes> response);

    @Multipart
    @POST("/updateGroupIconAndImage")
    public void updateGroupIconAndImageWithoutImage(@Part("admin_id") String adminId,
                                        @Part("user_id") String userId,
                                        @Part("group_name") String name,
                                        @Query("thread_id") String threadId,
                                        Callback<UpdateGroupRes> response);

    @POST("/exitGroup")
    public void exitGroup(@Query("user_id") String userId,
                              @Query("thread_id") String threadId,
                              Callback<GroupInfoRes> response);

    @Multipart
    @POST("/updateProfile")
    public void updateProfile(@Part("user_id") String userId,
                          @Query("name") String name,
                          @Query("status") String status,
                          @Part("photo") TypedFile file,
                          Callback<RegisterRes> response);

    @POST("/updateEmail")
    public void updateInfo(@Query("user_id") String userId,
                                @Query("email") String email,
                                @Query("password") String pass,
                                Callback<DefaultRes> response);

    @POST("/updateNotificationSettings")
    public void updateSettings(@Query("user_id") String userId,
                              @Query("show_messsage_push") String msgPush,
                              @Query("show_group_messsage_push") String groupPush,
                               @Query("messsage_push_notification") String msgNotify,
                               @Query("group_push_notification") String groupNotify,
                              Callback<UpdateSettingRes> response);

    @POST("/updateDeviceToken")
    public void sendToken(@Query("user_id") String userId,
                           @Query("token") String token,
                           @Query("device_type") String deviceType,
                           Callback<UpdateTokenRes> response);

    @POST("/forgotPassword")
    public void forgotPassword(@Query("user_email") String email,
                          Callback<UpdateTokenRes> response);

    @POST("/createThread")
    public void createThread(@Query("sender_id") String userId,
                             @Query("receiver_id") String recieverId,
                               Callback<CreateThreadRes> response);

}
