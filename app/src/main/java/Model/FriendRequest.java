package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FriendRequest implements Item {

    @SerializedName("request_id")
    @Expose
    private String requestId;
    @SerializedName("user_id1")
    @Expose
    private String userId1;
    @SerializedName("username1")
    @Expose
    private String username1;
    @SerializedName("name1")
    @Expose
    private String name1;
    @SerializedName("image1")
    @Expose
    private String image1;
    @SerializedName("status1")
    @Expose
    private String status1;
    @SerializedName("user_id2")
    @Expose
    private String userId2;
    @SerializedName("username2")
    @Expose
    private String username2;
    @SerializedName("name2")
    @Expose
    private String name2;
    @SerializedName("image2")
    @Expose
    private String image2;
    @SerializedName("status2")
    @Expose
    private String status2;

    private boolean isFrnd;

    private String isAdmin;

    /**
     *
     * @return
     * The requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     *
     * @param requestId
     * The request_id
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     *
     * @return
     * The userId1
     */
    public String getUserId1() {
        return userId1;
    }

    /**
     *
     * @param userId1
     * The user_id1
     */
    public void setUserId1(String userId1) {
        this.userId1 = userId1;
    }

    /**
     *
     * @return
     * The username1
     */
    public String getUsername() {
        return username1;
    }

    /**
     *
     * @param username1
     * The username1
     */
    public void setUsername(String username1) {
        this.username1 = username1;
    }

    /**
     *
     * @return
     * The name1
     */
    public String getName() {
        return name1;
    }

    /**
     *
     * @param name1
     * The name1
     */
    public void setName(String name1) {
        this.name1 = name1;
    }

    /**
     *
     * @return
     * The image1
     */
    public String getImage() {
        return image1;
    }

    /**
     *
     * @param image1
     * The image1
     */
    public void setImage(String image1) {
        this.image1 = image1;
    }

    /**
     *
     * @return
     * The status1
     */
    public String getStatus() {
        return status1;
    }

    /**
     *
     * @param status1
     * The status1
     */
    public void setStatus(String status1) {
        this.status1 = status1;
    }

    /**
     *
     * @return
     * The userId2
     */
    public String getUserId2() {
        return userId2;
    }

    /**
     *
     * @param userId2
     * The user_id2
     */
    public void setUserId2(String userId2) {
        this.userId2 = userId2;
    }

    /**
     *
     * @return
     * The username2
     */
    public String getFrndUsername() {
        return username2;
    }

    /**
     *
     * @param username2
     * The username2
     */
    public void setFrndUsername(String username2) {
        this.username2 = username2;
    }

    /**
     *
     * @return
     * The name2
     */
    public String getFrndName() {
        return name2;
    }

    /**
     *
     * @param name2
     * The name2
     */
    public void setFrndName(String name2) {
        this.name2 = name2;
    }

    /**
     *
     * @return
     * The image2
     */
    public String getFrndImage() {
        return image2;
    }

    /**
     *
     * @param image2
     * The image2
     */
    public void setFrndImage(String image2) {
        this.image2 = image2;
    }

    /**
     *
     * @return
     * The status2
     */
    public String getFrndStatus() {
        return status2;
    }

    /**
     *
     * @param status2
     * The status2
     */
    public void setFrndStatus(String status2) {
        this.status2 = status2;
    }

    public boolean isFrnd() {
        return isFrnd;
    }

    public void setIsFrnd(boolean isFrnd) {
        this.isFrnd = isFrnd;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}