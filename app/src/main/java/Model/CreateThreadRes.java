package Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateThreadRes {

    @SerializedName("thread_id")
    @Expose
    private String threadId;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;

    /**
     * @return The threadId
     */
    public String getThreadId() {
        return threadId;
    }

    /**
     * @param threadId The thread_id
     */
    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode The error_code
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

}