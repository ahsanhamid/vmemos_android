package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateSetting {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("email_address")
    @Expose
    private Object emailAddress;
    @SerializedName("email_verified")
    @Expose
    private String emailVerified;
    @SerializedName("email_verification_code")
    @Expose
    private Object emailVerificationCode;
    @SerializedName("phone_number")
    @Expose
    private Object phoneNumber;
    @SerializedName("phone_number_verified")
    @Expose
    private String phoneNumberVerified;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("message_push")
    @Expose
    private String messagePush;
    @SerializedName("message_push_sound")
    @Expose
    private String messagePushSound;
    @SerializedName("group_message_push")
    @Expose
    private String groupMessagePush;
    @SerializedName("group_message_push_sound")
    @Expose
    private String groupMessagePushSound;
    @SerializedName("user_contacts")
    @Expose
    private Object userContacts;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The emailAddress
     */
    public Object getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @param emailAddress
     * The email_address
     */
    public void setEmailAddress(Object emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     *
     * @return
     * The emailVerified
     */
    public String getEmailVerified() {
        return emailVerified;
    }

    /**
     *
     * @param emailVerified
     * The email_verified
     */
    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    /**
     *
     * @return
     * The emailVerificationCode
     */
    public Object getEmailVerificationCode() {
        return emailVerificationCode;
    }

    /**
     *
     * @param emailVerificationCode
     * The email_verification_code
     */
    public void setEmailVerificationCode(Object emailVerificationCode) {
        this.emailVerificationCode = emailVerificationCode;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    public Object getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phone_number
     */
    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The phoneNumberVerified
     */
    public String getPhoneNumberVerified() {
        return phoneNumberVerified;
    }

    /**
     *
     * @param phoneNumberVerified
     * The phone_number_verified
     */
    public void setPhoneNumberVerified(String phoneNumberVerified) {
        this.phoneNumberVerified = phoneNumberVerified;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The messagePush
     */
    public String getMessagePush() {
        return messagePush;
    }

    /**
     *
     * @param messagePush
     * The message_push
     */
    public void setMessagePush(String messagePush) {
        this.messagePush = messagePush;
    }

    /**
     *
     * @return
     * The messagePushSound
     */
    public String getMessagePushSound() {
        return messagePushSound;
    }

    /**
     *
     * @param messagePushSound
     * The message_push_sound
     */
    public void setMessagePushSound(String messagePushSound) {
        this.messagePushSound = messagePushSound;
    }

    /**
     *
     * @return
     * The groupMessagePush
     */
    public String getGroupMessagePush() {
        return groupMessagePush;
    }

    /**
     *
     * @param groupMessagePush
     * The group_message_push
     */
    public void setGroupMessagePush(String groupMessagePush) {
        this.groupMessagePush = groupMessagePush;
    }

    /**
     *
     * @return
     * The groupMessagePushSound
     */
    public String getGroupMessagePushSound() {
        return groupMessagePushSound;
    }

    /**
     *
     * @param groupMessagePushSound
     * The group_message_push_sound
     */
    public void setGroupMessagePushSound(String groupMessagePushSound) {
        this.groupMessagePushSound = groupMessagePushSound;
    }

    /**
     *
     * @return
     * The userContacts
     */
    public Object getUserContacts() {
        return userContacts;
    }

    /**
     *
     * @param userContacts
     * The user_contacts
     */
    public void setUserContacts(Object userContacts) {
        this.userContacts = userContacts;
    }

}