package Model;

/**
 * Created by developerclan on 24/02/2016.
 */
public class ContactModel {

    private String name;
    private String phone;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIndicator(){
        return Character.toString(name.charAt(0)) + Character.toString(Character.toLowerCase(name.charAt(0)));
    }
}
