package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RegisterRes {

    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("user")
    @Expose
    private List<User> user = new ArrayList<User>();
    @SerializedName("notification_sounds")
    @Expose
    private List<NotificationSound> notificationSounds = new ArrayList<NotificationSound>();

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The error_code
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The user
     */
    public List<User> getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(List<User> user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The notificationSounds
     */
    public List<NotificationSound> getNotificationSounds() {
        return notificationSounds;
    }

    /**
     *
     * @param notificationSounds
     * The notification_sounds
     */
    public void setNotificationSounds(List<NotificationSound> notificationSounds) {
        this.notificationSounds = notificationSounds;
    }

}