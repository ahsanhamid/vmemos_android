package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserGroupCurrentStatusRes {

    @SerializedName("user_group_current_status")
    @Expose
    private String userGroupCurrentStatus;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;

    /**
     *
     * @return
     * The userGroupCurrentStatus
     */
    public String getUserGroupCurrentStatus() {
        return userGroupCurrentStatus;
    }

    /**
     *
     * @param userGroupCurrentStatus
     * The user_group_current_status
     */
    public void setUserGroupCurrentStatus(String userGroupCurrentStatus) {
        this.userGroupCurrentStatus = userGroupCurrentStatus;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The errorCode
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

}