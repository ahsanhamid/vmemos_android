package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecieveThread {

    private int frndChatId;
    private String downLoadStatus;
    private double duration;
    private String isDownLoadError;
    private String isResendError;
    private String recieverId;

    @SerializedName("message_id")
    @Expose
    private String msgId;
    @SerializedName("vmemo_participant_tbl_id")
    @Expose
    private String vmemoParticipantTblId;
    @SerializedName("partcipant_id")
    @Expose
    private String partcipantId;
    @SerializedName("message_thread_id")
    @Expose
    private String threadId;
    @SerializedName("is_admin")
    @Expose
    private String isAdmin;
    @SerializedName("chat_title")
    @Expose
    private String chatTitle;
    @SerializedName("chat_image")
    @Expose
    private String chatImage;
    @SerializedName("file_path")
    @Expose
    private String filePath;
    @SerializedName("message_status")
    @Expose
    private String msgStatus;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("sender_image")
    @Expose
    private String senderImage;
    @SerializedName("sender_name")
    @Expose
    private String senderName;
    @SerializedName("is_group")
    @Expose
    private String isGroup;
    @SerializedName("assigned_color")
    @Expose
    private String assignedColor;
    @SerializedName("new_message_status")
    @Expose
    private String newMessageStatus;
    @SerializedName("chat_image_thumb")
    @Expose
    private String chatImageThumb;
    @SerializedName("message_text")
    @Expose
    private String msgText;
    @SerializedName("user_group_status_on_msg_post")
    @Expose
    private String userGroupStatusOnMsgPost;
    @SerializedName("user_group_current_status")
    @Expose
    private String userGroupCurrentStatus;
    private String text_message_status;

    public int getFrndChatId() {
        return frndChatId;
    }

    public void setFrndChatId(int frndChatId) {
        this.frndChatId = frndChatId;
    }

    public String getDownLoadStatus() {
        return downLoadStatus;
    }

    public void setDownLoadStatus(String downLoadStatus) {
        this.downLoadStatus = downLoadStatus;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getIsDownLoadError() {
        return isDownLoadError;
    }

    public void setIsDownLoadError(String isDownLoadError) {
        this.isDownLoadError = isDownLoadError;
    }

    public String getIsResendError() {
        return isResendError;
    }

    public void setIsResendError(String isResendError) {
        this.isResendError = isResendError;
    }

    public String getRecieverId() {
        return recieverId;
    }

    public void setRecieverId(String recieverId) {
        this.recieverId = recieverId;
    }

    /**
     *
     * @return
     * The messageId
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     *
     * @param messageId
     * The message_id
     */
    public void setMsgId(String messageId) {
        this.msgId = messageId;
    }

    /**
     *
     * @return
     * The vmemoParticipantTblId
     */
    public String getVmemoParticipantTblId() {
        return vmemoParticipantTblId;
    }

    /**
     *
     * @param vmemoParticipantTblId
     * The vmemo_participant_tbl_id
     */
    public void setVmemoParticipantTblId(String vmemoParticipantTblId) {
        this.vmemoParticipantTblId = vmemoParticipantTblId;
    }

    /**
     *
     * @return
     * The partcipantId
     */
    public String getPartcipantId() {
        return partcipantId;
    }

    /**
     *
     * @param partcipantId
     * The partcipant_id
     */
    public void setPartcipantId(String partcipantId) {
        this.partcipantId = partcipantId;
    }

    /**
     *
     * @return
     * The messageThreadId
     */
    public String getThreadId() {
        return threadId;
    }

    /**
     *
     * @param messageThreadId
     * The message_thread_id
     */
    public void setThreadId(String messageThreadId) {
        this.threadId = messageThreadId;
    }

    /**
     *
     * @return
     * The isAdmin
     */
    public String getIsAdmin() {
        return isAdmin;
    }

    /**
     *
     * @param isAdmin
     * The is_admin
     */
    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     *
     * @return
     * The chatTitle
     */
    public String getChatTitle() {
        return chatTitle;
    }

    /**
     *
     * @param chatTitle
     * The chat_title
     */
    public void setChatTitle(String chatTitle) {
        this.chatTitle = chatTitle;
    }

    /**
     *
     * @return
     * The chatImage
     */
    public String getChatImage() {
        return chatImage;
    }

    /**
     *
     * @param chatImage
     * The chat_image
     */
    public void setChatImage(String chatImage) {
        this.chatImage = chatImage;
    }

    /**
     *
     * @return
     * The filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     *
     * @param filePath
     * The file_path
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     *
     * @return
     * The messageStatus
     */
    public String getMsgStatus() {
        return msgStatus;
    }

    /**
     *
     * @param messageStatus
     * The message_status
     */
    public void setMsgStatus(String messageStatus) {
        this.msgStatus = messageStatus;
    }

    /**
     *
     * @return
     * The senderId
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     *
     * @param senderId
     * The sender_id
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     *
     * @return
     * The senderImage
     */
    public String getSenderImage() {
        return senderImage;
    }

    /**
     *
     * @param senderImage
     * The sender_image
     */
    public void setSenderImage(String senderImage) {
        this.senderImage = senderImage;
    }

    /**
     *
     * @return
     * The senderName
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     *
     * @param senderName
     * The sender_name
     */
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    /**
     *
     * @return
     * The isGroup
     */
    public String getIsGroup() {
        return isGroup;
    }

    /**
     *
     * @param isGroup
     * The is_group
     */
    public void setIsGroup(String isGroup) {
        this.isGroup = isGroup;
    }

    /**
     *
     * @return
     * The assignedColor
     */
    public String getColor() {
        return assignedColor;
    }

    /**
     *
     * @param assignedColor
     * The assigned_color
     */
    public void setColor(String assignedColor) {
        this.assignedColor = assignedColor;
    }

    /**
     *
     * @return
     * The newMessageStatus
     */
    public String getNewMessageStatus() {
        return newMessageStatus;
    }

    /**
     *
     * @param newMessageStatus
     * The new_message_status
     */
    public void setNewMessageStatus(String newMessageStatus) {
        this.newMessageStatus = newMessageStatus;
    }

    /**
     *
     * @return
     * The chatImageThumb
     */
    public String getChatImageThumb() {
        return chatImageThumb;
    }

    /**
     *
     * @param chatImageThumb
     * The chat_image_thumb
     */
    public void setChatImageThumb(String chatImageThumb) {
        this.chatImageThumb = chatImageThumb;
    }


    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getUserGroupStatusOnMsgPost() {
        return userGroupStatusOnMsgPost;
    }

    public void setUserGroupStatusOnMsgPost(String userGroupStatusOnMsgPost) {
        this.userGroupStatusOnMsgPost = userGroupStatusOnMsgPost;
    }

    public String getUserGroupCurrentStatus() {
        return userGroupCurrentStatus;
    }

    public void setUserGroupCurrentStatus(String userGroupCurrentStatus) {
        this.userGroupCurrentStatus = userGroupCurrentStatus;
    }

    public String getTextMessageStatus() {
        return text_message_status;
    }

    public void setTextMessageStatus(String text_message_status) {
        this.text_message_status = text_message_status;
    }
}