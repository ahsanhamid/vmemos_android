package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateSettingRes {

    @SerializedName("user_settings")
    @Expose
    private List<User> updateSettings = new ArrayList<User>();
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;

    /**
     *
     * @return
     * The userSettings
     */
    public List<User> getUpdateSettings() {
        return updateSettings;
    }

    /**
     *
     * @param updateSettings
     * The user_settings
     */
    public void setUpdateSettings(List<User> updateSettings) {
        this.updateSettings = updateSettings;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The errorCode
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

}