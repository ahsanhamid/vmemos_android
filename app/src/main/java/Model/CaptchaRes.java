package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CaptchaRes {

    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("captcha_array")
    @Expose
    private List<CaptchaArray> captchaArray = new ArrayList<CaptchaArray>();

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The error_code
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The captchaArray
     */
    public List<CaptchaArray> getCaptchaArray() {
        return captchaArray;
    }

    /**
     *
     * @param captchaArray
     * The captcha_array
     */
    public void setCaptchaArray(List<CaptchaArray> captchaArray) {
        this.captchaArray = captchaArray;
    }

}