package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchFrndsRes {

    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("records")
    @Expose
    private List<SearchFrnd> searchFrnds = new ArrayList<SearchFrnd>();

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The error_code
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The records
     */
    public List<SearchFrnd> getSearchFrnds() {
        return searchFrnds;
    }

    /**
     *
     * @param records
     * The records
     */
    public void setSearchFrnds(List<SearchFrnd> records) {
        this.searchFrnds = records;
    }

}