package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("user_id")
    @Expose
    private String id;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("sms_verification_code")
    @Expose
    private String smsVerificationCode;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("captcha_ids")
    @Expose
    private String captchaIds;
    @SerializedName("id")
    @Expose
    private String userId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email_address")
    @Expose
    private String emailAddress;
    @SerializedName("vocal_name")
    @Expose
    private String vocalName;
    @SerializedName("message_push")
    @Expose
    private String messagePush;
    @SerializedName("message_push_sound")
    @Expose
    private String messagePushSound;
    @SerializedName("group_message_push")
    @Expose
    private String groupMessagePush;
    @SerializedName("group_message_push_sound")
    @Expose
    private String groupMessagePushSound;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The statusMessage
     */
    public String  getStatusMessage() {
        return statusMessage;
    }

    /**
     *
     * @param statusMessage
     * The status_message
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phone_number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The smsVerificationCode
     */
    public String getSmsVerificationCode() {
        return smsVerificationCode;
    }

    /**
     *
     * @param smsVerificationCode
     * The sms_verification_code
     */
    public void setSmsVerificationCode(String smsVerificationCode) {
        this.smsVerificationCode = smsVerificationCode;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The userStatus
     */
    public String getUserStatus() {
        return userStatus;
    }

    /**
     *
     * @param userStatus
     * The user_status
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    /**
     *
     * @return
     * The captchaIds
     */
    public String getCaptchaIds() {
        return captchaIds;
    }

    /**
     *
     * @param captchaIds
     * The captcha_ids
     */
    public void setCaptchaIds(String captchaIds) {
        this.captchaIds = captchaIds;
    }

    /**
     *
     * @return
     * The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @param emailAddress
     * The email_address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     *
     * @return
     * The vocalName
     */
    public String getVocalName() {
        return vocalName;
    }

    /**
     *
     * @param vocalName
     * The vocal_name
     */
    public void setVocalName(String vocalName) {
        this.vocalName = vocalName;
    }

    /**
     *
     * @return
     * The messagePush
     */
    public String getMessagePush() {
        return messagePush;
    }

    /**
     *
     * @param messagePush
     * The message_push
     */
    public void setMessagePush(String messagePush) {
        this.messagePush = messagePush;
    }

    /**
     *
     * @return
     * The messagePushSound
     */
    public String getMessagePushSound() {
        return messagePushSound;
    }

    /**
     *
     * @param messagePushSound
     * The message_push_sound
     */
    public void setMessagePushSound(String messagePushSound) {
        this.messagePushSound = messagePushSound;
    }

    /**
     *
     * @return
     * The groupMessagePush
     */
    public String getGroupMessagePush() {
        return groupMessagePush;
    }

    /**
     *
     * @param groupMessagePush
     * The group_message_push
     */
    public void setGroupMessagePush(String groupMessagePush) {
        this.groupMessagePush = groupMessagePush;
    }

    /**
     *
     * @return
     * The groupMessagePushSound
     */
    public String getGroupMessagePushSound() {
        return groupMessagePushSound;
    }

    /**
     *
     * @param groupMessagePushSound
     * The group_message_push_sound
     */
    public void setGroupMessagePushSound(String groupMessagePushSound) {
        this.groupMessagePushSound = groupMessagePushSound;
    }

}