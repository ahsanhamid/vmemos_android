package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupInfo {

    @SerializedName("member_name")
    @Expose
    private String memberName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("participant_thread_id")
    @Expose
    private String participantThreadId;
    @SerializedName("is_admin")
    @Expose
    private String isAdmin;
    @SerializedName("chat_title")
    @Expose
    private String chatTitle;
    @SerializedName("chat_image")
    @Expose
    private String chatImage;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_group")
    @Expose
    private String isGroup;
    @SerializedName("assigned_color")
    @Expose
    private String assignedColor;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("admin_id")
    @Expose
    private String adminId;

    /**
     *
     * @return
     * The memberName
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     *
     * @param memberName
     * The member_name
     */
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The userImage
     */
    public String getUserImage() {
        return userImage;
    }

    /**
     *
     * @param userImage
     * The user_image
     */
    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The participantThreadId
     */
    public String getParticipantThreadId() {
        return participantThreadId;
    }

    /**
     *
     * @param participantThreadId
     * The participant_thread_id
     */
    public void setParticipantThreadId(String participantThreadId) {
        this.participantThreadId = participantThreadId;
    }

    /**
     *
     * @return
     * The isAdmin
     */
    public String getIsAdmin() {
        return isAdmin;
    }

    /**
     *
     * @param isAdmin
     * The is_admin
     */
    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     *
     * @return
     * The chatTitle
     */
    public String getChatTitle() {
        return chatTitle;
    }

    /**
     *
     * @param chatTitle
     * The chat_title
     */
    public void setChatTitle(String chatTitle) {
        this.chatTitle = chatTitle;
    }

    /**
     *
     * @return
     * The chatImage
     */
    public String getChatImage() {
        return chatImage;
    }

    /**
     *
     * @param chatImage
     * The chat_image
     */
    public void setChatImage(String chatImage) {
        this.chatImage = chatImage;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The isGroup
     */
    public String getIsGroup() {
        return isGroup;
    }

    /**
     *
     * @param isGroup
     * The is_group
     */
    public void setIsGroup(String isGroup) {
        this.isGroup = isGroup;
    }

    /**
     *
     * @return
     * The assignedColor
     */
    public String getAssignedColor() {
        return assignedColor;
    }

    /**
     *
     * @param assignedColor
     * The assigned_color
     */
    public void setAssignedColor(String assignedColor) {
        this.assignedColor = assignedColor;
    }

    /**
     *
     * @return
     * The userStatus
     */
    public String getUserStatus() {
        return userStatus;
    }

    /**
     *
     * @param userStatus
     * The user_status
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    /**
     *
     * @return
     * The adminId
     */
    public String getAdminId() {
        return adminId;
    }

    /**
     *
     * @param adminId
     * The admin_id
     */
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

}