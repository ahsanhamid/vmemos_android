package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CaptchaArray {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("is_correct")
    @Expose
    private String isCorrect;
    @SerializedName("image_status")
    @Expose
    private String imageStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The isCorrect
     */
    public String getIsCorrect() {
        return isCorrect;
    }

    /**
     *
     * @param isCorrect
     * The is_correct
     */
    public void setIsCorrect(String isCorrect) {
        this.isCorrect = isCorrect;
    }

    /**
     *
     * @return
     * The imageStatus
     */
    public String getImageStatus() {
        return imageStatus;
    }

    /**
     *
     * @param imageStatus
     * The image_status
     */
    public void setImageStatus(String imageStatus) {
        this.imageStatus = imageStatus;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}