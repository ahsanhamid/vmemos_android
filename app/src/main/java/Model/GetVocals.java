package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetVocals {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String receiverId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("message_thrad_id")
    @Expose
    private String threadId;
    @SerializedName("unread_vocal_count")
    @Expose
    private String unreadVocalCount;
    @SerializedName("chat_title")
    @Expose
    private String groupName;
    @SerializedName("chat_image")
    @Expose
    private String groupImage;
    @SerializedName("is_group")
    @Expose
    private String isGroup;
    @SerializedName("admin_id")
    @Expose
    private String adminId;
    @SerializedName("total_vocal_count")
    @Expose
    private String totalVocalCount;
    @SerializedName("user_group_current_status")
    @Expose
    private String userGroupCurrentStatus;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The id
     */
    public String getReceiverId() {
        return receiverId;
    }

    /**
     *
     * @param receiverId
     * The id
     */
    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }
    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The messageThradId
     */
    public String getThreadId() {
        return threadId;
    }

    /**
     *
     * @param threadId
     * The message_thrad_id
     */
    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    /**
     *
     * @return
     * The unreadVocalCount
     */
    public String getUnreadVocalCount() {
        return unreadVocalCount;
    }

    /**
     *
     * @param unreadVocalCount
     * The unread_vocal_count
     */
    public void setUnreadVocalCount(String unreadVocalCount) {
        this.unreadVocalCount = unreadVocalCount;
    }

    /**
     *
     * @return
     * The chatTitle
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     *
     * @param groupName
     * The chat_title
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     *
     * @return
     * The chatImage
     */
    public String getGroupImage() {
        return groupImage;
    }

    /**
     *
     * @param groupImage
     * The chat_image
     */
    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    /**
     *
     * @return
     * The isGroup
     */
    public String getIsGroup() {
        return isGroup;
    }

    /**
     *
     * @param isGroup
     * The is_group
     */
    public void setIsGroup(String isGroup) {
        this.isGroup = isGroup;
    }

    /**
     *
     * @return
     * The adminId
     */
    public String getAdminId() {
        return adminId;
    }

    /**
     *
     * @param adminId
     * The admin_id
     */
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    /**
     *
     * @return
     * The totalVocalCount
     */
    public String getTotalVocalCount() {
        return totalVocalCount;
    }

    /**
     *
     * @param totalVocalCount
     * The totalVocalCount
     */
    public void setTotalVocalCount(String totalVocalCount) {
        this.totalVocalCount = totalVocalCount;
    }

    /**
     *
     * @return
     * The userGroupCurrentStatus
     */
    public String getUserGroupCurrentStatus() {
        return userGroupCurrentStatus;
    }

    /**
     *
     * @param userGroupCurrentStatus
     * The userGroupCurrentStatus
     */
    public void setUserGroupCurrentStatus(String userGroupCurrentStatus) {
        this.userGroupCurrentStatus = userGroupCurrentStatus;
    }
}