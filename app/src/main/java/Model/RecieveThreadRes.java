package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecieveThreadRes {

    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("thread")
    @Expose
    private List<RecieveThread> recieveThreads = new ArrayList<RecieveThread>();
    @SerializedName("msg")
    @Expose
    private String msg;

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The error_code
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *
     * @return
     * The thread
     */
    public List<RecieveThread> getRecieveThread() {
        return recieveThreads;
    }

    /**
     *
     * @param recieveThreads
     * The thread
     */
    public void setRecieveThread(List<RecieveThread> recieveThreads) {
        this.recieveThreads = recieveThreads;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

}