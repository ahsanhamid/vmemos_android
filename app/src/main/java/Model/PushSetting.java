package Model;

/**
 * Created by developerclan on 03/03/2016.
 */
public class PushSetting {

    private String msgPush;
    private String msgPushSound;
    private String groupMsgPush;
    private String groupMsgPushSound;


    public String getMsgPush() {
        return msgPush;
    }

    public void setMsgPush(String msgPush) {
        this.msgPush = msgPush;
    }

    public String getMsgPushSound() {
        return msgPushSound;
    }

    public void setMsgPushSound(String msgPushSound) {
        this.msgPushSound = msgPushSound;
    }

    public String getGroupMsgPush() {
        return groupMsgPush;
    }

    public void setGroupMsgPush(String groupMsgPush) {
        this.groupMsgPush = groupMsgPush;
    }

    public String getGroupMsgPushSound() {
        return groupMsgPushSound;
    }

    public void setGroupMsgPushSound(String groupMsgPushSound) {
        this.groupMsgPushSound = groupMsgPushSound;
    }
}
