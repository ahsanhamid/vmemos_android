package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostVocalRes {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("thread_id")
    @Expose
    private String threadId;
    @SerializedName("msg_id")
    @Expose
    private String msgId;
    @SerializedName("push_msg")
    @Expose
    private String push_msg;

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The error_code
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *
     * @return
     * The threadId
     */
    public String getThreadId() {
        return threadId;
    }

    /**
     *
     * @param threadId
     * The thread_id
     */
    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    /**
     *
     * @return
     * The msgId
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     *
     * @param msgId
     * The msg_id
     */
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    /**
     *
     * @return
     * The msgId
     */
    public String getPushMsg() {
        return push_msg;
    }

    /**
     *
     * @param push_msg
     * The msg_id
     */
    public void setPushMsg(String push_msg) {
        this.push_msg = push_msg;
    }

}