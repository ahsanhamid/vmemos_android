package Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FrndRequestRes {

    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("friends")
    @Expose
    private List<FriendRequest> friends = new ArrayList<FriendRequest>();
    @SerializedName("friendship_requests")
    @Expose
    private List<FriendRequest> friendRequests = new ArrayList<FriendRequest>();

    /**
     *
     * @return
     * The errorCode
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode
     * The error_code
     */
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The friends
     */
    public List<FriendRequest> getFriends() {
        return friends;
    }

    /**
     *
     * @param friends
     * The friends
     */
    public void setFriends(List<FriendRequest> friends) {
        this.friends = friends;
    }

    /**
     *
     * @return
     * The friendshipRequests
     */
    public List<FriendRequest> getFriendRequests() {
        return friendRequests;
    }

    /**
     *
     * @param friendRequests
     * The friendship_requests
     */
    public void setFriendRequests(List<FriendRequest> friendRequests) {
        this.friendRequests = friendRequests;
    }

}