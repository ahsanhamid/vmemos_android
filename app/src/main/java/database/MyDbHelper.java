package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import Model.FriendRequest;
import Model.FrndRequestRes;
import Model.GetVocals;
import Model.RecieveThread;
import Model.VocalsModel;
import Util.Constants;

public class MyDbHelper extends SQLiteOpenHelper {

    SQLiteDatabase db;

    private static final String DATABASE_ALTER_FRNDCHAT_1 = "ALTER TABLE "
            + Constants.TBL_FRIEND_CHAT + " ADD COLUMN " + Constants.KEY_MSG_TEXT + " text;";
    private static final String DATABASE_ALTER_FRNDCHAT_2 = "ALTER TABLE "
            + Constants.TBL_FRIEND_CHAT + " ADD COLUMN " + Constants.KEY_TEXT_MSG_STATUS + " text;";
    private static final String DATABASE_ALTER_FRNDCHAT_3 = "ALTER TABLE "
            + Constants.TBL_FRIEND_CHAT + " ADD COLUMN " + Constants.KEY_USER_GROUP_STATUS_ON_MSG_POST + " text;";

    public MyDbHelper(Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        String tblFriendChat = "create table " + Constants.TBL_FRIEND_CHAT + " (" + Constants.KEY_FRIEND_CHAT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Constants.KEY_CHAT_TITLE + " text," + Constants.KEY_COLOR + " text," + Constants.KEY_DOWNLOAD_STATUS + " text," + Constants.KEY_DURATION + " REAL," +
                Constants.KEY_FILE_PATH + " text, " + Constants.KEY_IS_DOWNLOAD_ERROR + " text," + Constants.KEY_IS_GROUP + " text," + Constants.KEY_IS_RESEND_ERROR + " text," + Constants.KEY_MSG_ID + " text," +
                Constants.KEY_MSG_STATUS + " text, " + Constants.KEY_RECIEVER_ID + " text," + Constants.KEY_SENDER_ID + " text," + Constants.KEY_SENDER_NAME + " text," + Constants.KEY_THREAD_ID + " text," +
                Constants.KEY_MSG_TEXT + " text," + Constants.KEY_TEXT_MSG_STATUS + " text," + Constants.KEY_USER_GROUP_STATUS_ON_MSG_POST + " text);";

        String tblJsonRes = "create table " + Constants.TBL_JSON_RESPONCE + " (" + Constants.KEY_USER_ID + " text ," + Constants.KEY_URL + " text ," + Constants.KEY_RESPONCE + " text);";

        db.execSQL(tblFriendChat);
        db.execSQL(tblJsonRes);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        if (oldVersion < Constants.DB_VERSION) {
            db.execSQL(DATABASE_ALTER_FRNDCHAT_1);
            db.execSQL(DATABASE_ALTER_FRNDCHAT_2);
            db.execSQL(DATABASE_ALTER_FRNDCHAT_3);
        }

    }

    public Boolean addNewResponceList(VocalsModel model) {

        try {
            db = this.getWritableDatabase();

            db.execSQL("Delete from " + Constants.TBL_JSON_RESPONCE + " WHERE " + Constants.KEY_URL + " = '" + model.getUrl()+ "' AND " + Constants.KEY_USER_ID + " = " + model.getUserId());

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_USER_ID, model.getUserId());
            cv.put(Constants.KEY_URL, model.getUrl());
            cv.put(Constants.KEY_RESPONCE, model.getResponce());
            db.insert(Constants.TBL_JSON_RESPONCE, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }
        db.close();
        return true;
    }

    public List<GetVocals> getVocalsData(String userId, String url) {
        // TODO Auto-generated method stub
        List<GetVocals> list = new ArrayList<>();
        VocalsModel model = new VocalsModel();
        try {

            db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("select " + Constants.KEY_RESPONCE + " from " + Constants.TBL_JSON_RESPONCE + " WHERE " + Constants.KEY_URL + " = '" + url + "' AND " + Constants.KEY_USER_ID + " = " + userId, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {

//					model.setUserId(cursor.getString(0));
//					model.setUrl(cursor.getString(1));
//					model.setResponce(cursor.getString(2));

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<GetVocals>>() {
                    }.getType();
                    list = gson.fromJson(cursor.getString(0), type);

                }
                while (cursor.moveToNext());
            }
            if (cursor != null) {
                cursor.close();
            }
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        return list;
    }

    public FrndRequestRes getFrndsData(String userId, String url) {
        // TODO Auto-generated method stub
        List<FriendRequest> list = new ArrayList<>();
        FrndRequestRes res = null;
        try {

            db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("select " + Constants.KEY_RESPONCE + " from " + Constants.TBL_JSON_RESPONCE + " WHERE " + Constants.KEY_URL + " = '" + url + "' AND " + Constants.KEY_USER_ID + " = " + userId, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {

//					model.setUserId(cursor.getString(0));
//					model.setUrl(cursor.getString(1));
//					model.setResponce(cursor.getString(2));

                    Gson gson = new Gson();

                    res = gson.fromJson(cursor.getString(0), FrndRequestRes.class);


//                    Type type = new TypeToken<List<FriendRequest>>() {
//                    }.getType();
//                    list = gson.fromJson(cursor.getString(0), type);

                }
                while (cursor.moveToNext());
            }
            if (cursor != null) {
                cursor.close();
            }
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        return res;
    }

    public boolean alreadyExistVocals(String threadId, String msgId, String msgStatus, boolean isText) {

        boolean flag = false;
        try {

            db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("select * from " + Constants.TBL_FRIEND_CHAT + " WHERE " + Constants.KEY_THREAD_ID + " = '" + threadId + "' AND " + Constants.KEY_MSG_ID + " = " + msgId, null);

            if (cursor != null && cursor.moveToFirst()) {
                do {

                    int id = cursor.getInt(0);

                    updateMsgStatus(id, msgStatus, isText);

                    flag = true;
                }
                while (cursor.moveToNext());
            } else {
                flag = false;
            }

//			if (cursor!=null && cursor.getCount() > 0){
//				flag = true;
//			}
//			else {
//				flag = false;
//			}

            if (cursor != null) {
                cursor.close();
            }
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }

        return flag;
    }

    private void updateMsgStatus(int id, String msgStatus, boolean isText) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            if (isText) {
                cv.put(Constants.KEY_TEXT_MSG_STATUS, msgStatus);
            }
            else {
                cv.put(Constants.KEY_MSG_STATUS, msgStatus);
            }
            String where = Constants.KEY_FRIEND_CHAT_ID + " = ?";
            db.update(Constants.TBL_FRIEND_CHAT, cv, where, new String[]{String.valueOf(id)});
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();
    }

    public boolean insertNewVocal(RecieveThread thread) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_SENDER_ID, thread.getSenderId());
            cv.put(Constants.KEY_THREAD_ID, thread.getThreadId());
            cv.put(Constants.KEY_MSG_STATUS, thread.getMsgStatus());
            cv.put(Constants.KEY_MSG_ID, thread.getMsgId());
            cv.put(Constants.KEY_CHAT_TITLE, thread.getChatTitle());
            cv.put(Constants.KEY_SENDER_NAME, thread.getSenderName());
            cv.put(Constants.KEY_FILE_PATH, thread.getFilePath());
            cv.put(Constants.KEY_COLOR, thread.getColor());
            cv.put(Constants.KEY_IS_GROUP, thread.getIsGroup());
            cv.put(Constants.KEY_DOWNLOAD_STATUS, "0");
            cv.put(Constants.KEY_TEXT_MSG_STATUS, thread.getTextMessageStatus());
            cv.put(Constants.KEY_USER_GROUP_STATUS_ON_MSG_POST, thread.getUserGroupStatusOnMsgPost());
            cv.put(Constants.KEY_MSG_TEXT, thread.getMsgText());

            db.insert(Constants.TBL_FRIEND_CHAT, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return false;
        }
        db.close();
        return true;
    }

    public List<RecieveThread> getVocalsLocally(String threadId) {

        List<RecieveThread> list = new ArrayList<RecieveThread>();
        try {

            db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("select * from " + Constants.TBL_FRIEND_CHAT +" WHERE "+Constants.KEY_THREAD_ID+" = "+threadId, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {

                    RecieveThread model = new RecieveThread();
                    model.setFrndChatId(cursor.getInt(0));
                    model.setChatTitle(cursor.getString(1));
                    model.setColor(cursor.getString(2));
                    model.setDownLoadStatus(cursor.getString(3));
                    model.setDuration(cursor.getDouble(4));
                    model.setFilePath(cursor.getString(5));
                    model.setIsDownLoadError(cursor.getString(6));
                    model.setIsGroup(cursor.getString(7));
                    model.setIsResendError(cursor.getString(8));
                    model.setMsgId(cursor.getString(9));
                    model.setMsgStatus(cursor.getString(10));
                    model.setRecieverId(cursor.getString(11));
                    model.setSenderId(cursor.getString(12));
                    model.setSenderName(cursor.getString(13));
                    model.setThreadId(cursor.getString(14));
                    model.setMsgText(cursor.getString(15));
                    model.setTextMessageStatus(cursor.getString(16));
                    model.setUserGroupStatusOnMsgPost(cursor.getString(17));

                    list.add(model);
                }
                while (cursor.moveToNext());
            }
            if (cursor!=null){
                cursor.close();
            }

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }

        return list;
    }

    public void updateMessageStatusLocal(String msgStatus, String msgId, String threadId) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_MSG_STATUS, msgStatus);

            String where = Constants.KEY_MSG_ID + " = ? AND "+Constants.KEY_THREAD_ID+" = ?";
            db.update(Constants.TBL_FRIEND_CHAT, cv, where, new String[]{String.valueOf(msgId), String.valueOf(threadId)});
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();
    }

    public void updateDownloadStatus(int frndChatId, String fileName, int dur, String msgStatus, String msgId) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_MSG_STATUS, msgStatus);
            cv.put(Constants.KEY_DOWNLOAD_STATUS, "1");
            cv.put(Constants.KEY_FILE_PATH, fileName);
            cv.put(Constants.KEY_DURATION, dur);

            String where = Constants.KEY_FRIEND_CHAT_ID + " = ?";
            db.update(Constants.TBL_FRIEND_CHAT, cv, where, new String[]{String.valueOf(frndChatId)});
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();

    }

    public void updateDownloadStatus(int frndChatId) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_IS_DOWNLOAD_ERROR, Constants.FALSE);

            String where = Constants.KEY_FRIEND_CHAT_ID + " = ?";
            db.update(Constants.TBL_FRIEND_CHAT, cv, where, new String[]{String.valueOf(frndChatId)});
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();

    }

    public void updateDownloadFailStatus(int frndChatId) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_IS_DOWNLOAD_ERROR, Constants.TRUE);

            String where = Constants.KEY_FRIEND_CHAT_ID + " = ?";
            db.update(Constants.TBL_FRIEND_CHAT, cv, where, new String[]{String.valueOf(frndChatId)});
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();

    }

    public boolean alreadyExistVocalID(int chatId) {

        boolean flag = false;
        try {

            db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("select * from " + Constants.TBL_FRIEND_CHAT + " WHERE " + Constants.KEY_FRIEND_CHAT_ID + " = " + chatId, null);

            if (cursor.getCount()>0) {
                flag = true;
            }

            cursor.close();

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }

        return flag;
    }


    public long insertUserVocal(String senderId, String receiverId, String threadId, String fileName, double dur, String downLoadStatus) {

        long id = -1;
        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_SENDER_ID, senderId);
            cv.put(Constants.KEY_RECIEVER_ID, receiverId);
            cv.put(Constants.KEY_THREAD_ID, threadId);
            cv.put(Constants.KEY_FILE_PATH, fileName);
            cv.put(Constants.KEY_DURATION, dur);
            cv.put(Constants.KEY_DOWNLOAD_STATUS, downLoadStatus);

            id = db.insert(Constants.TBL_FRIEND_CHAT, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
//            return false;
        }
        db.close();
        return id;

    }

    public void updateUserVocal(int chatId, String msgStatus, String threadId, String sendError, String msgId) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_MSG_STATUS, msgStatus);
            cv.put(Constants.KEY_THREAD_ID, threadId);
            cv.put(Constants.KEY_IS_RESEND_ERROR, sendError);
            cv.put(Constants.KEY_MSG_ID, msgId);

            String where = Constants.KEY_FRIEND_CHAT_ID + " = ?";
            db.update(Constants.TBL_FRIEND_CHAT, cv, where, new String[]{String.valueOf(chatId)});
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();

    }

    public void updateUploadFailStatusLocally(int chatId,String status) {

        try {
            db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.KEY_IS_RESEND_ERROR, status);

            String where = Constants.KEY_FRIEND_CHAT_ID + " = ?";
            db.update(Constants.TBL_FRIEND_CHAT, cv, where, new String[]{String.valueOf(chatId)});
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();

    }

    public void deleteThread(String threadId) {

        try {
            db = this.getWritableDatabase();

            db.execSQL("Delete from " + Constants.TBL_FRIEND_CHAT + " WHERE " + Constants.KEY_THREAD_ID + " = '" + threadId + "'");

        } catch (Exception e) {
            e.printStackTrace();
            db.close();
        }
        db.close();
    }
}


