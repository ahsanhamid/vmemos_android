package com.vmemos.inc;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.gson.Gson;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Picasso;
import com.timqi.sectorprogressview.SectorProgressView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import Model.DefaultRes;
import Model.FriendRequest;
import Model.FrndRequestRes;
import Model.GetUserGroupCurrentStatusRes;
import Model.GetVocals;
import Model.PostVocalRes;
import Model.RecieveThread;
import Model.VocalsModel;
import ThirdParty.CircleProgressBar;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import Util.NetworkUtil;
import database.MyDbHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import tyrantgit.explosionfield.ExplosionField;

public class FriendChatActivity extends Activity {

    CustomAdapter adapter;
    String fileName;
    MediaRecorder recorder;
    MediaPlayer player;
    boolean isRecording;
    CircleProgressBar recordView;
    CountDownTimer recordAnimation;
    CountDownTimer playAnimation;
    CountDownTimer afterPlayAnimation;
    SectorProgressView afterPlayView;
    ImageView clockView;
    ImageView img;
    ImageView animVocal;
    CircleImageView imgHeader;
    float recordTime;
    float afterPlayTime;
    GetVocals getVocals;
    TextView heading;
    MyDbHelper dbHelper;
//    XListView listView;
    ListView listView;
    String currentPlayingMsgId;
    String currentPlayingMsgStatus;
    boolean isUserMsg;
    RelativeLayout inActiveView;

    SwipyRefreshLayout swipyRefreshLayout;

    int startX, startY, height, width;

    List<RecieveThread> arrVocals;

    View explodeView;
    String coloCode;
    int animVocalLoc[];
    int animVocalTop;
    boolean isFromPush = false;
    Context context;
    LinearLayout tabView;

    Timer timer;
    TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    ObjectAnimator anim;
    View header;
    int pageNo;
    Timer timerForUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_friend_chat);

        AppManager.getInstance().setFont(this);
        dbHelper = new MyDbHelper(this);

        recordTime = 0;
        afterPlayTime = 0;
        isRecording = false;

        heading = (TextView) findViewById(R.id.heading);
        TextView text = (TextView) findViewById(R.id.text);
        text.setTypeface(AppManager.getInstance().getTextMsgTypeface());
        Button back = (Button) findViewById(R.id.back);
        listView = (ListView) findViewById(R.id.listView);
        swipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        swipyRefreshLayout.setColorSchemeResources(R.color.white);
//        swipyRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.green);
        img = (ImageView) findViewById(R.id.img);
        imgHeader = (CircleImageView) findViewById(R.id.imgProfile);
        animVocal = (ImageView) findViewById(R.id.animVocal);
        recordView = (CircleProgressBar) findViewById(R.id.custom_progressBar);
        afterPlayView = (SectorProgressView) findViewById(R.id.spv);
        clockView = (ImageView) findViewById(R.id.clockView);
        clockView.setVisibility(View.INVISIBLE);
        afterPlayView.setVisibility(View.INVISIBLE);

        ///// add header in listview

        LayoutInflater inflater = getLayoutInflater();
        header = inflater.inflate(R.layout.custom_header, listView, false);
//        listView.addHeaderView(header, null, false);


        ////////////

        tabView = (LinearLayout) findViewById(R.id.tab);
        inActiveView = (RelativeLayout) findViewById(R.id.rel);
        inActiveView.setVisibility(View.INVISIBLE);
        Button btnVocal = (Button) findViewById(R.id.btnVocal);
        Button btnContact = (Button) findViewById(R.id.btnContcts);
        Button btnFrnds = (Button) findViewById(R.id.btnFrnds);


        recordView.setBackgroundColor(getResources().getColor(R.color.recordViewTrack));
        recordView.setColor(getResources().getColor(R.color.recordViewProgress));

        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnContact.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnFrnds.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnVocal.setTypeface(AppManager.getInstance().getRegularTypeface());
        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Gson gson = new Gson();
            getVocals = gson.fromJson(bundle.getString("VOCAL"), GetVocals.class);

            if (bundle.containsKey("FROM_PUSH")) {
                isFromPush = bundle.getBoolean("FROM_PUSH");
            }
        }

        if (isFromPush) {
            context = this;
        } else {
            context = this; //getParent();
        }

        arrVocals = new ArrayList<>();
        adapter = new CustomAdapter(context, R.layout.custom_frndchat_cell, arrVocals);
        listView.setAdapter(adapter);
//        listView.setPullLoadEnable(true);
//        listView.setPullRefreshEnable(false);
//        listView.computeScroll();


        if (getVocals.getIsGroup().equalsIgnoreCase("1")) {
            heading.setText(Functions.decodeFromNonLossyAscii(getVocals.getGroupName()));
            String url = Functions.getImage(context, "thumb_" + getVocals.getGroupImage());
            Picasso.with(FriendChatActivity.this).load(new File(url)).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgHeader);
        } else {
            heading.setText(Functions.decodeFromNonLossyAscii(getVocals.getName()));
            String url = Functions.getImage(context, "thumb_" + getVocals.getImage());
            Picasso.with(FriendChatActivity.this).load(new File(url)).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgHeader);
        }

        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageNo = pageNo + 1;
                header.setEnabled(false);
//                new AsyncRecieveThread().execute(String.valueOf(pageNo),Constants.TRUE);
                new AsyncRecieveThread().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,String.valueOf(pageNo),Constants.TRUE);
            }
        });

//        listView.setXListViewListener(new XListView.IXListViewListener() {
//            @Override
//            public void onRefresh() {
//
//            }
//
//            @Override
//            public void onLoadMore() {
//                if (!getVocals.getThreadId().equalsIgnoreCase("")) {
//
////                    AsyncTask.execute(new Runnable() {
////                        @Override
////                        public void run() {
////                            //TODO your background code
////                            receiveThreadData();
////                        }
////                    });
//
//                    new AsyncRecieveThread().execute(String.valueOf(pageNo));
//
////                    receiveThreadData();
//                }
//            }
//        });

        swipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.BOTTOM){
                    if (!getVocals.getThreadId().equalsIgnoreCase("")) {


//                        new AsyncRecieveThread().execute(String.valueOf(pageNo),Constants.FALSE);
                        new AsyncRecieveThread().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,String.valueOf(pageNo),Constants.FALSE);

                    }
                }
            }
        });

        recordView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                switch (event.getAction() & MotionEvent.ACTION_MASK) {


//                if (event.getAction() == MotionEvent.ACTION_DOWN)

                    case MotionEvent.ACTION_DOWN: {

                        Log.i("FrndChat", "ACtion down");

                        try {
                            stopPlaying();
                            stopPlayAnimation();
                            recordTime = 0;
                            recordView.setProgress(0);

                            if (!isRecording) {
                                Log.i("FrndChat", "ACtion down if");
                                recorder = new MediaRecorder();
                                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                                recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                                recorder.setMaxDuration(Constants.AUDIO_DURATION_LIMIT * 1000);
                                recorder.setOutputFile(getFilePath());
                                recorder.prepare();
                                recorder.start();

                            }

                            startTimer();


//                            startRecordAnimatiomn();

                            Thread th = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Looper.prepare();
                                    startRecordAnimatiomn();
                                }
                            });
                            th.start();

//                        isRecording = true;

                            recorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                                @Override
                                public void onInfo(MediaRecorder mr, int what, int extra) {
                                    if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                                        if (isRecording) {
                                            stopRecording();
                                            stopRecordAnimation();
                                            recordTime = 0;
//                                        isRecording = false;
                                            recordView.setProgress(0);
                                            Log.e("Record", "recording STOP");
//                                        Toast.makeText(getApplicationContext(),"recording STOP",Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            });


                        } catch (Exception e) {
                            Log.i("FrndChat", "ACtion DOWN EXC");
                            e.printStackTrace();
                            stopRecordAnimation();
                        }
                    }
                    break;
                    case MotionEvent.ACTION_UP:
//                if (event.getAction() == MotionEvent.ACTION_UP)
                    {

                        Log.i("FrndChat", "ACtion UP");

                        try {

                            if (isRecording) {

                                Log.i("FrndChat", "ACtion UP IF");

                                isRecording = false;
                                stopRecordAnimation();
//                                recordTime = 0;
//                                recordView.setProgress(0);

                                stopRecording();
//                            playFile(fileName);

                                // Send Vocal
//                                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.FILE_PATH;
                                String path = getFilesDir().getAbsolutePath() + "/" + Constants.FILE_PATH;
                                MediaPlayer mp = new MediaPlayer();
                                mp.setDataSource(path + "/" + fileName);
                                mp.prepare();
                                int dur = mp.getDuration();
//                            Toast.makeText(getApplicationContext(),"DUR === "+dur, Toast.LENGTH_LONG).show();
                                mp.release();

                                String userId = "";
                                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                                if (pref.contains(Constants.kUserID)) {
                                    userId = pref.getString(Constants.kUserID, "");
                                }

                                if (dur > 400) {
                                    recordTime = 0;
                                    recordView.setProgress(0);
                                    saveAndPostVocal(userId, getVocals.getReceiverId(), path, fileName, dur, getVocals.getThreadId(), -1);
                                }

                            } else {
                                Log.i("FrndChat", "ACtion UP ELSE");
                                stoptimertask();
                                stopRecordAnimation();
                                stopRecording();
                            }

                        } catch (Exception e) {
                            Log.i("FrndChat", "ACtion UP EXC");
                            e.printStackTrace();
                            stoptimertask();
                            stopRecordAnimation();
                        }
                    }
                    break;
                }
                return false;
            }
        });

    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 400); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                isRecording = true;
                Log.i("isRecording", "true");
            }
        };
    }

    private BroadcastReceiver onNotice = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // intent can contain anydata
            Log.d("sohail", "onReceive called");

            reloadView(AppManager.getInstance().getChatPuchData());

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        AppManager.getInstance().setCropImage(null);

        pageNo = 1;

        if (getVocals.getThreadId() != null && !getVocals.getThreadId().equalsIgnoreCase("")) {

            ///// tiemr for user current status /////
            if (getVocals.getIsGroup().equalsIgnoreCase("1")) {

                timerForUser = new Timer();
                timerForUser.schedule(new MyTimerTask(), 1000, 5000);

            }
            /////// END ///////

            final List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());
            if (list.size() > 0) {
                if (list.size() >= Constants.NUM_OF_ITEMS_TO_ADD) {
                    listView.removeHeaderView(header);
                    listView.addHeaderView(header, null, false);
                } else {
                    listView.removeHeaderView(header);
                }
                List<RecieveThread> sortedList = showHideListViewHeader(list);
                arrVocals.clear();
                arrVocals.addAll(sortedList);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });


//                listView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        setListViewBottom(false);
//
//                    }
//                }, 50);

                setListViewBottom(false);

            } else {
//                listView.removeHeaderView(header);
            }

            AppManager.getInstance().setCurrentChatThreadID(getVocals.getThreadId());

            IntentFilter iff = new IntentFilter("push");
            LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, iff);

            AppManager.getInstance().setIsChatActive(true);


//            new AsyncRecieveThread().execute(String.valueOf(pageNo),Constants.FALSE);
            new AsyncRecieveThread().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,String.valueOf(pageNo),Constants.FALSE);

            if (isFromPush) {
                tabView.setVisibility(View.GONE); //hide tab
                inActiveView.setVisibility(View.INVISIBLE);
            } else {
                tabView.setVisibility(View.GONE);

                if (getVocals.getIsGroup().equalsIgnoreCase("1") && (getVocals.getUserGroupCurrentStatus() == null || !getVocals.getUserGroupCurrentStatus().equalsIgnoreCase("0"))) {
                    inActiveView.setVisibility(View.INVISIBLE);
                } else if (getVocals.getIsGroup().equalsIgnoreCase("1")) {
                    inActiveView.setVisibility(View.VISIBLE);
                } else {
                    inActiveView.setVisibility(View.INVISIBLE);
                }

            }

//            receiveThreadData();

        }


    }

    public void reloadView(GetVocals getVocals) {
        this.getVocals = getVocals;

        if (getVocals.getThreadId() != null && !getVocals.getThreadId().equalsIgnoreCase("")) {

//            final List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());
//            if (list.size() > 0) {
//                arrVocals.clear();
//                arrVocals.addAll(list);
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        adapter.notifyDataSetChanged();
//                    }
//                });
//
//
//                listView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        setListViewBottom(false);
//
//                    }
//                }, 50);
//
//            }
//
            AppManager.getInstance().setIsChatActive(true);

//            AsyncTask.execute(new Runnable() {
//                @Override
//                public void run() {
//                    //TODO your background code
//
//                    receiveThreadData();
//                }
//            });

//            new AsyncRecieveThread().execute(String.valueOf(pageNo),Constants.FALSE);

            new AsyncRecieveThread().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,String.valueOf(pageNo),Constants.FALSE);

        }
    }

    private void setListViewBottom(boolean flag) {
//        int p = listView.getLastVisiblePosition();
//        int count = adapter.getCount();
//        if (p < count+1) {


        listView.post(new Runnable() {
            public void run() {
                listView.setSelection(listView.getCount() - 1);
            }
        });


//            if (flag) {
//                listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
//            }
//            else {
//                listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_NORMAL);
//            }
//            listView.setStackFromBottom(true);
////        } else {
////            listView.setStackFromBottom(false);
////        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppManager.getInstance().setIsChatActive(false);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(onNotice);

//        stopAfterPlayAnimation();
        stopPlayAnimation();
        afterPlayTime = 0;

        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }

        if (timerForUser != null) {
            timerForUser.cancel();
            timerForUser = null;
        }

        if (afterPlayAnimation != null && !currentPlayingMsgId.equalsIgnoreCase("")) {
            stopAfterPlayAnimation();
            updateMessageStatus(Constants.kStatusDelete, currentPlayingMsgId, false);
        }
    }

    private void stopRecording() {
        if (recorder != null) {
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
        }
    }

    private void playFile(String fileName) {
        try {
            player = new MediaPlayer();
//            player.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.FILE_PATH + "/" + fileName);
            player.setDataSource(getFilesDir().getAbsolutePath() + "/" + Constants.FILE_PATH + "/" + fileName);
            player.prepare();
            player.start();

            stopPlayAnimation();
            startPlayAnimatiomn();

            Log.e("FileDur", String.valueOf(player.getDuration()));
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlaying();
                    stopPlayAnimation();
                    if (!currentPlayingMsgStatus.equalsIgnoreCase(Constants.kStatusSaved) && !isUserMsg) {

                        Thread th = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Looper.prepare();
                                startAfterPlayAnimatiomn();
                            }
                        });
                        th.start();

                    } else {
                        reverseAnim(false);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopPlaying() {
        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }
    }

    private String getFilePath() {
//        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.FILE_PATH;
        String path = getFilesDir().getAbsolutePath() + "/" + Constants.FILE_PATH;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        Date now = new Date();
        String strDate = sdf.format(now);
        strDate = strDate.replace(".", "_");
        strDate = strDate.replace(":", "_");
        strDate = strDate.replace(" ", "_");
        String name = strDate + "audio.m4a";
        fileName = name;
        String fileName = "";
        try {
            File directory = new File(path);

            if (!directory.exists()) {
                directory.mkdirs();
            }

            File file = new File(path, name);

            if (!file.exists()) {
                file.createNewFile();
                fileName = file.getAbsolutePath();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileName;
    }

    private void startRecordAnimatiomn() {
        recordAnimation = new CountDownTimer(Constants.AUDIO_DURATION_LIMIT * 1000, 49) {

            public void onTick(final long millisUntilFinished) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        Log.e("recordAnim", String.valueOf(Constants.AUDIO_DURATION_LIMIT * 1000 - millisUntilFinished));
//                        recordTime += 0.494;
//
//                        recordView.setProgress(recordTime);
//
//                    }
//                });

                Log.e("recordAnim", String.valueOf(Constants.AUDIO_DURATION_LIMIT * 1000 - millisUntilFinished));
                recordTime += 0.494;

                FriendChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recordView.setProgress(recordTime);
                    }
                });


            }

            public void onFinish() {
//                Toast.makeText(getApplicationContext(),"finish",Toast.LENGTH_LONG).show();
//                stopRecording();
            }
        }.start();
        Looper.loop();
    }

    private void stopRecordAnimation() {
        if (recordAnimation != null) {
            recordAnimation.cancel();
            recordAnimation = null;
        }
    }

    private void startPlayAnimatiomn() {
        playAnimation = new CountDownTimer(Constants.AUDIO_DURATION_LIMIT * 1000, 500) {
            double playAnimTime = 0.5;

            public void onTick(long millisUntilFinished) {

                if (playAnimTime == 0.5) {
                    img.setBackgroundResource(R.drawable.record_btn_one_wave);
                    playAnimTime += 0.5;
                } else if (playAnimTime == 1) {
                    img.setBackgroundResource(R.drawable.record_btn_two_wave);
                    playAnimTime += 0.5;
                } else if (playAnimTime == 1.5) {
                    img.setBackgroundResource(R.drawable.record_btn);
                    playAnimTime += 0.5;
                } else {
                    img.setBackgroundResource(R.drawable.record_btn_nowave);
                    playAnimTime = 0.5;
                }
            }

            public void onFinish() {
                img.setBackgroundResource(R.drawable.record_btn);
            }
        }.start();
    }

    private void stopPlayAnimation() {
        if (playAnimation != null) {
            img.setBackgroundResource(R.drawable.record_btn);
            playAnimation.cancel();
            playAnimation = null;
        }
    }

    private void startAfterPlayAnimatiomn() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                afterPlayView.setVisibility(View.VISIBLE);
                clockView.setVisibility(View.VISIBLE);
                afterPlayView.bringToFront();

                try {
                    player = new MediaPlayer();
                    AssetFileDescriptor descriptor = getAssets().openFd("alert_sound/timerSound.mp3");
                    player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                    descriptor.close();

                    player.prepare();
                    player.setLooping(true);
                    player.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        afterPlayAnimation = new CountDownTimer(Constants.AUDIO_DURATION_LIMIT * 1000, 49) {

            public void onTick(long millisUntilFinished) {
                afterPlayTime += 0.5;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        afterPlayView.setPercent(afterPlayTime);
                    }
                });

                Log.i("timer", String.valueOf(afterPlayTime));
            }

            public void onFinish() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        afterPlayView.setVisibility(View.INVISIBLE);
                        clockView.setVisibility(View.INVISIBLE);
                    }
                });

                Log.i("timer finish", String.valueOf(afterPlayTime));
                afterPlayTime = 0;
                if (player != null) {
                    player.stop();
                    player.release();
                    player = null;
                }

//                try {
//                    MediaPlayer player = new MediaPlayer();
//                    AssetFileDescriptor descriptor = getAssets().openFd("alert_sound/delete.mp3");
//                    player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
//                    descriptor.close();
//
//                    player.prepare();
//                    player.start();
//                }catch (Exception e){
//                    e.printStackTrace();
//                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        reverseAnim(true);
                    }
                });


//                new ParticleSystem(FriendChatActivity.this, 100, R.drawable.star_green, 800,coloCode)
//                        .setSpeedRange(0.1f, 0.25f)
//                        .oneShot(explodeView, 100);

                //delete File
                updateMessageStatus(Constants.kStatusDelete, currentPlayingMsgId, true);
            }
        }.start();
        Looper.loop();
    }

    private void stopAfterPlayAnimation() {
        if (afterPlayAnimation != null) {
            afterPlayAnimation.cancel();
            afterPlayAnimation = null;
            afterPlayView.setVisibility(View.INVISIBLE);
            clockView.setVisibility(View.INVISIBLE);
            if (player != null) {
                player.stop();
                player.release();
                player = null;
            }
        }
    }

    private void saveAndPostVocal(final String userId, final String receiverId, final String path, final String fileName, final double dur, final String threadId, final int frndChatId) {

        Handler mainHandler = new Handler(FriendChatActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                int newChatId;
                if (frndChatId == -1) { // && !dbHelper.alreadyExistVocalID(frndChatId)

                    newChatId = (int) dbHelper.insertUserVocal(userId, receiverId, threadId, fileName, dur, "1");

                } else {
                    newChatId = frndChatId;
                }

                List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());

                reloadListView(list);
                setListViewBottom(true);

                TypedFile typedFile = null;
                typedFile = new TypedFile("audio/m4a", new File(path + "/" + fileName));

                final int finalNewChatId = newChatId;
                AppManager.getInstance().restAPI.postVocal(userId, receiverId, threadId, typedFile, new Callback<PostVocalRes>() {
                    @Override
                    public void success(PostVocalRes postVocalRes, Response response) {

                        if (postVocalRes.getErrorCode() == 0) {

                            getVocals.setThreadId(postVocalRes.getThreadId());
                            String msgId = postVocalRes.getMsgId();

                            dbHelper.updateUserVocal(finalNewChatId, "1", getVocals.getThreadId(), Constants.FALSE, msgId);


                        } else {
                            dbHelper.updateUploadFailStatusLocally(finalNewChatId, Constants.TRUE);
                        }

                        List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());

                        reloadListView(list);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        dbHelper.updateUploadFailStatusLocally(finalNewChatId, Constants.TRUE);

                        List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());

                        reloadListView(list);
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    private List<RecieveThread> showHideListViewHeader(List<RecieveThread> list) {

        Collections.sort(list, new Comparator<RecieveThread>() {
            @Override
            public int compare(RecieveThread r1, RecieveThread r2) {
                String msgId1 = r1.getMsgId();
                String msgId2 = r2.getMsgId();
                if (msgId1 != null && msgId2 != null) {
                    return msgId1.compareToIgnoreCase(msgId2);
                } else {
                    return 0;
                }
            }
        });

        List<RecieveThread> result = new ArrayList<>();
        int pos = pageNo * Constants.NUM_OF_ITEMS_TO_ADD;
        if (list.size() >= pos) {
//            listView.removeHeaderView(header);
//            listView.addHeaderView(header, null, false);
            int selectPos = list.size() - pos;
            for (int i = selectPos; i < list.size(); i++) {
                result.add(list.get(i));
            }
        } else {
//            listView.removeHeaderView(header);
            for (int i = 0; i < list.size(); i++) {
                result.add(list.get(i));
            }
        }
        return result;
    }


    private void reloadListView(List<RecieveThread> list) {
        if (list.size() > 0) {
            List<RecieveThread> sortedList = showHideListViewHeader(list);
            arrVocals.clear();
            arrVocals.addAll(sortedList);
            adapter.notifyDataSetChanged();
        }
    }

    private void updateMessageStatus(final String msgStatus, final String msgId, final boolean reload) {

        Handler mainHandler = new Handler(FriendChatActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                AppManager.getInstance().restAPI.updateMsgStatus(userId, msgId, msgStatus, new Callback<DefaultRes>() {
                    @Override
                    public void success(DefaultRes defaultRes, Response response) {

                        if (defaultRes.getErrorCode() == 0) {

                            dbHelper.updateMessageStatusLocal(msgStatus, msgId, getVocals.getThreadId());

                            if (reload) {
                                List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());

                                reloadListView(list);
                            }
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                if (isFromPush) {
                    goToTabActivity(0);
                } else {
                    finish();
                }
                break;
            case R.id.spv:
                stopAfterPlayAnimation();
                afterPlayTime = 0;
                reverseAnim(false);
                updateMessageStatus(Constants.kStatusSaved, currentPlayingMsgId, true);
                break;
            case R.id.heading:
                if (getVocals.getIsGroup().equalsIgnoreCase("1")) {
                    getAllFrndsAndRequestAPI();

                    Gson gson = new Gson();
                    String value = gson.toJson(getVocals);

                    if (!isFromPush) {
                        Intent intent = new Intent(context, NewGroupActivity.class);
                        intent.putExtra("VOCAL", value);
                        intent.putExtra("BOOL", false);
                        startActivity(intent);
                    }
                }
                break;
            case R.id.imgProfile:
                String url;
                if (getVocals.getIsGroup().equalsIgnoreCase("0")) {
                    url = Functions.getImage(context, getVocals.getImage());
                } else {
                    url = Functions.getImage(context, getVocals.getGroupImage());
                }
                Intent intent = new Intent(FriendChatActivity.this, ImageActivity.class);
                intent.putExtra(Constants.FROM_CHAT, true);
                intent.putExtra(Constants.URL, url);
                startActivity(intent);
                break;
            case R.id.btnVocal:
                goToTabActivity(0);
                break;
            case R.id.btnFrnds:
                goToTabActivity(1);
                break;
            case R.id.btnContcts:
                goToTabActivity(2);
                break;

        }
    }

    private void goToTabActivity(int i) {

        Intent intent = new Intent(FriendChatActivity.this, TabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);//.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("INDEX", i);
        startActivity(intent);

        try {
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    class CustomAdapter extends ArrayAdapter<RecieveThread> {

        Context context;
        List<RecieveThread> list;

        public CustomAdapter(Context context, int resource, List<RecieveThread> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        private class ViewHolder {
            TextView name;
            TextView bar;
            TextView status;
            ImageView imgVocal;
            ImageView imgExplode;
            CircleProgressBar progressBar;
            CircularProgressView progressView;
            Button btnUploadDownload;
        }

        private void setHeight(RelativeLayout tv, int h) {

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)tv.getLayoutParams();

            if (h == 0) {
                params.height = h;
                params.setMargins(0, 0, 0, 0);
            }
            else {
                params.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
                params.setMargins(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.text_view_margin));
            }
            tv.setLayoutParams(params);

        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {


//            View textView = null;

            final RecieveThread thread = list.get(position);

            String userId = "";
            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.kUserID)) {
                userId = pref.getString(Constants.kUserID, "");
            }

            view = LayoutInflater.from(context).inflate(R.layout.custom_text_msg_cell, parent, false);
            RelativeLayout rela = (RelativeLayout) view.findViewById(R.id.rel);
            setHeight(rela, 0);

            if (thread.getMsgText() != null && !thread.getMsgText().equalsIgnoreCase("") && !thread.getMsgText().equalsIgnoreCase("null")) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_text_msg_cell, parent, false);
                TextView tv = (TextView) view.findViewById(R.id.tv_text);
                tv.setTypeface(AppManager.getInstance().getTextMsgTypeface());
                RelativeLayout rel = (RelativeLayout) view.findViewById(R.id.rel);
                setHeight(rel, 0);


                if ((thread.getUserGroupStatusOnMsgPost() == null || !thread.getUserGroupStatusOnMsgPost().equalsIgnoreCase("0")) && thread.getTextMessageStatus().equalsIgnoreCase("3")) {
                    tv.setBackgroundColor(Color.parseColor("#EAEAE9"));
                    setHeight(rel, getResources().getDimensionPixelSize(R.dimen.text_view_height));
                    String[] arr = thread.getMsgText().split(" ");
                    String name = arr[0];
                    String text;
                    if (name.equalsIgnoreCase(AppManager.getInstance().getUser().getUsername())) {
                        text = thread.getMsgText().replace(name, "You");
                    } else {
                        text = thread.getMsgText();
                    }

                    tv.setText(Functions.decodeFromNonLossyAscii(text));
                    return view;

                } else if (thread.getSenderId().equalsIgnoreCase(userId) && (thread.getUserGroupStatusOnMsgPost() == null || !thread.getUserGroupStatusOnMsgPost().equalsIgnoreCase("0")) && thread.getTextMessageStatus().equalsIgnoreCase("1")) {
                    tv.setBackgroundColor(Color.parseColor("#EAEAE9"));
                    setHeight(rel, getResources().getDimensionPixelSize(R.dimen.text_view_height));
                    String[] arr = thread.getMsgText().split(" ");
                    String name = arr[0];
                    String text;
                    if (name.equalsIgnoreCase(AppManager.getInstance().getUser().getUsername())) {
                        text = thread.getMsgText().replace(name, "You");
                    } else {
                        text = thread.getMsgText();
                    }

                    tv.setText(Functions.decodeFromNonLossyAscii(text));
                    return view;
                }

            } else if ((thread.getUserGroupStatusOnMsgPost() == null || !thread.getUserGroupStatusOnMsgPost().equalsIgnoreCase("0"))) {
// && thread.getFilePath() != null && !thread.getFilePath().equalsIgnoreCase("null") && !thread.getFilePath().equalsIgnoreCase("")
                ViewHolder holder = null;

                if (view == null) {
                    view = LayoutInflater.from(context).inflate(R.layout.custom_frndchat_cell, parent, false);
                    holder = new ViewHolder();

                    holder.name = (TextView) view.findViewById(R.id.tv_name);
                    holder.bar = (TextView) view.findViewById(R.id.lbl);
                    holder.status = (TextView) view.findViewById(R.id.tv_status);
                    holder.imgVocal = (ImageView) view.findViewById(R.id.imgVocal);
                    holder.imgExplode = (ImageView) view.findViewById(R.id.imgExplode);
                    holder.progressBar = (CircleProgressBar) view.findViewById(R.id.custom_progressBar);
                    holder.progressView = (CircularProgressView) view.findViewById(R.id.progress_view);
                    holder.btnUploadDownload = (Button) view.findViewById(R.id.btnUploadDownload);

                    view.setTag(holder);
                } else {

                    holder = (ViewHolder) view.getTag();

                    if (holder == null) {
                        holder = new ViewHolder();

                        view = LayoutInflater.from(context).inflate(R.layout.custom_frndchat_cell, parent, false);
                        holder.name = (TextView) view.findViewById(R.id.tv_name);
                        holder.bar = (TextView) view.findViewById(R.id.lbl);
                        holder.status = (TextView) view.findViewById(R.id.tv_status);
                        holder.imgVocal = (ImageView) view.findViewById(R.id.imgVocal);
                        holder.imgExplode = (ImageView) view.findViewById(R.id.imgExplode);
                        holder.progressBar = (CircleProgressBar) view.findViewById(R.id.custom_progressBar);
                        holder.progressView = (CircularProgressView) view.findViewById(R.id.progress_view);
                        holder.btnUploadDownload = (Button) view.findViewById(R.id.btnUploadDownload);

                        view.setTag(holder);
                    }
                }


                holder.progressView.bringToFront();
                holder.progressView.setColor(Color.BLACK);
                holder.progressView.setThickness(5);


                if (thread.getSenderId().equalsIgnoreCase(userId)) {


                    holder.name.setText("Me");
                    holder.name.setTextColor(getResources().getColor(R.color.green));
                    holder.status.setTextColor(getResources().getColor(R.color.green));
                    holder.bar.setBackgroundColor(getResources().getColor(R.color.green));
                    holder.imgVocal.setImageResource(R.drawable.me_smiley_icon);
                    holder.imgExplode.setImageResource(R.drawable.me_smiley_icon);
                    holder.progressBar.setBackgroundColor(getResources().getColor(android.R.color.transparent));//me_emoji_bg
//                holder.progressBar.setBackground(setborder(getResources().getColor(R.color.me_emoji_bg)));
                    holder.progressBar.setColor(getResources().getColor(R.color.me_emoji_pg));
                    holder.progressBar.setProgress((float) thread.getDuration() / 100);
                    holder.btnUploadDownload.setBackgroundResource(R.drawable.re_send);

                    Drawable drawable = context.getResources().getDrawable(R.drawable.shape);
                    if (drawable != null) {
                        drawable.setColorFilter(Color.parseColor("#a8f880"), PorterDuff.Mode.SRC_ATOP);
                    }
                    holder.progressBar.setBackground(drawable);

                    if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusUpload)) {
                        holder.status.setText("Uploaded");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusDelivered)) {
                        holder.status.setText("Delivered");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusListened)) {
                        holder.status.setText("Listened");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusSaved)) {
                        holder.status.setText("Saved");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusDelete)) {
                        holder.status.setText("Deleted");
                        showViews(holder, false);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else {
                        holder.status.setText("Uploading");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    }

                    if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusSaved)) {
                        holder.bar.getLayoutParams().width = 5;
                        holder.bar.setVisibility(View.VISIBLE);
                        holder.bar.requestLayout();
                    }

                } else {
                    if (thread.getIsGroup() != null && thread.getIsGroup().equalsIgnoreCase("0")) {
                        holder.name.setText(thread.getChatTitle());
                    } else {
                        holder.name.setText(thread.getSenderName());
                    }
                    holder.name.setTextColor(getResources().getColor(R.color.blue));
                    holder.status.setTextColor(getResources().getColor(R.color.blue));
                    holder.bar.setBackgroundColor(getResources().getColor(R.color.blue));
                    holder.imgVocal.setImageResource(R.drawable.onefriend_smiley_icon);
                    holder.imgExplode.setImageResource(R.drawable.onefriend_smiley_icon);
                    holder.progressBar.setBackgroundColor(getResources().getColor(android.R.color.transparent)); //frnd_emoji_bg
//                holder.progressBar.setBackground(setborder(getResources().getColor(R.color.frnd_emoji_bg)));
                    holder.progressBar.setColor(getResources().getColor(R.color.frnd_emoji_pg));
                    holder.progressBar.setProgress((float) thread.getDuration() / 100);
                    holder.btnUploadDownload.setBackgroundResource(R.drawable.re_download);

                    Drawable drawable = context.getResources().getDrawable(R.drawable.shape);
                    if (drawable != null) {
                        drawable.setColorFilter(Color.parseColor("#abddf3"), PorterDuff.Mode.SRC_ATOP);
                    }
                    holder.progressBar.setBackground(drawable);

                    if (thread.getIsGroup() != null && thread.getIsGroup().equalsIgnoreCase("1") && thread.getColor() != null && !thread.getColor().equalsIgnoreCase("")) {

                        int resourceId = getResources().getIdentifier("e_" + thread.getColor().substring(1, thread.getColor().length()).toLowerCase(), "raw", context.getPackageName());
                        holder.imgVocal.setImageResource(resourceId);
                        holder.imgExplode.setImageResource(resourceId);

                        int resId = getResources().getIdentifier("re_" + thread.getColor().substring(1, thread.getColor().length()).toLowerCase(), "raw", context.getPackageName());
                        holder.btnUploadDownload.setBackgroundResource(resId);

                        if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#003300")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#658560"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#658560"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#001d00"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#260317")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#62515a"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#62515a"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#17000d"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#690696")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#997da3"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#997da3"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#37074c"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#811828")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#93777b"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#93777b"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#50030f"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#0000EE")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#a3a3fb"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#a3a3fb"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#01017e"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#3d72b4")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#87a2c2"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#87a2c2"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#12427e"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#8B2A8B")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#cd89cd"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#cd89cd"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#660666"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#CD3F00")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#d5a38e"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#d5a38e"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#902c00"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#ff0400")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#df8d8c"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#df8d8c"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#9b0200"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#FF6600")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#fbab75"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#fbab75"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#a74300"));
                        } else if (thread.getColor() != null && thread.getColor().equalsIgnoreCase("#ffd302")) {
//                        holder.progressBar.setBackgroundColor(Color.parseColor("#eadca5"));
                            if (drawable != null) {
                                drawable.setColorFilter(Color.parseColor("#eadca5"), PorterDuff.Mode.SRC_ATOP);
                            }
                            holder.progressBar.setColor(Color.parseColor("#a08401"));
                        }

                        holder.progressBar.setBackground(drawable);

                        holder.name.setTextColor(Color.parseColor(thread.getColor()));
                        holder.status.setTextColor(Color.parseColor(thread.getColor()));
                        holder.bar.setBackgroundColor(Color.parseColor(thread.getColor()));

                    }

                    if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusDelivered)) {
                        holder.status.setText("Received");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusListened)) {
                        holder.status.setText("Listened");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusSaved)) {
                        holder.status.setText("Saved");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusDelete)) {
                        holder.status.setText("Deleted");
                        showViews(holder, false);
                        holder.bar.setVisibility(View.INVISIBLE);
                    } else {
                        holder.status.setText("Downloading");
                        showViews(holder, true);
                        holder.bar.setVisibility(View.INVISIBLE);
                    }

                    if (thread.getMsgStatus() != null && thread.getMsgStatus().equalsIgnoreCase(Constants.kStatusSaved)) {
                        holder.bar.getLayoutParams().width = 10;
                        holder.bar.setVisibility(View.VISIBLE);
                        holder.bar.requestLayout();
                    }

                }

                holder.imgExplode.setVisibility(View.GONE);

                final String finalUserId = userId;
                final ViewHolder finalHolder = holder;
                final String finalUserId1 = userId;

                holder.imgVocal.setOnClickListener(new View.OnClickListener() {
                    int pos = position;
                    ViewHolder hold = finalHolder;

                    @Override
                    public void onClick(View v) {

                        if (inActiveView.getVisibility() == View.VISIBLE) {
                            return;
                        }

                        if (list.get(pos).getMsgStatus() != null && !list.get(pos).getMsgStatus().equalsIgnoreCase(Constants.kStatusDelete)) {


                            if (list.get(pos).getSenderId().equalsIgnoreCase(finalUserId)) {
                                isUserMsg = true;
                            } else {
                                isUserMsg = false;
                            }

                            if (!list.get(pos).getMsgId().equalsIgnoreCase(currentPlayingMsgId)) {
                                if (afterPlayAnimation != null && !currentPlayingMsgId.equalsIgnoreCase("")) {
                                    stopAfterPlayAnimation();
                                    updateMessageStatus(Constants.kStatusDelete, currentPlayingMsgId, true);
                                }

                                currentPlayingMsgId = list.get(pos).getMsgId();

                            }

                            currentPlayingMsgStatus = list.get(pos).getMsgStatus();

                            afterPlayView.setVisibility(View.INVISIBLE);
                            stopAfterPlayAnimation();
                            stopPlaying();
                            afterPlayTime = 0;

                            hold.imgExplode.setVisibility(View.VISIBLE);
                            explodeView = hold.imgExplode;

                            if (list.get(pos).getSenderId().equalsIgnoreCase(finalUserId1)) {
                                animVocal.setImageResource(R.drawable.me_smiley_icon);
                            } else {
                                if (list.get(pos).getIsGroup() != null && list.get(pos).getIsGroup().equalsIgnoreCase("1")) {
                                    int resourceId = getResources().getIdentifier("e_" + list.get(pos).getColor().substring(1, list.get(pos).getColor().length()).toLowerCase(), "raw", context.getPackageName());
                                    animVocal.setImageResource(resourceId);
                                } else {
                                    animVocal.setImageResource(R.drawable.onefriend_smiley_icon);
                                }
                            }


                            animVocal.setVisibility(View.VISIBLE);
                            animVocal.clearAnimation();

                            int[] loc = new int[2];
                            v.getLocationOnScreen(loc);

                            startX = loc[0];
                            startY = loc[1];
                            height = v.getHeight();
                            width = v.getWidth();

                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(v.getWidth(), v.getHeight());
                            params.leftMargin = getRelativeLeft(v); //(int)Functions.getTopLeftCorner(v).x; //getRelativeLeft(v);
                            params.topMargin = loc[1] - (v.getHeight() / 2);

                            animVocal.setLayoutParams(params);
                            animVocal.requestLayout();

                            /////
                            RelativeLayout root = (RelativeLayout) findViewById(R.id.rootLayout);
                            DisplayMetrics dm = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(dm);
                            int statusBarOffset = dm.heightPixels - root.getMeasuredHeight();


                            int[] locRec = new int[2];
                            recordView.getLocationOnScreen(locRec);

                            int xDest = dm.widthPixels / 2;
                            xDest -= (v.getMeasuredWidth() / 2);
                            xDest -= loc[0];
                            int yDest = dm.heightPixels - (dm.heightPixels - locRec[1]) + (recordView.getHeight() / 4);// - locRec[1]; //(animVocal.getMeasuredHeight()/2) - statusBarOffset
                            yDest -= loc[1];

// Set up the path we're animating along
                            AnimatorPath path = new AnimatorPath();
                            path.moveTo(0, 0);
//        path.lineTo(0, 300);
                            path.curveTo(xDest, yDest / 2, xDest, yDest, xDest, yDest);

                            // Set up the animation
                            final ObjectAnimator anim = ObjectAnimator.ofObject(FriendChatActivity.this, "buttonLoc",
                                    new PathEvaluator(), path.getPoints().toArray());
                            anim.setDuration(800);
                            anim.addListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    animVocal.bringToFront();
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    animVocal.clearAnimation();

                                    animVocal.setVisibility(View.INVISIBLE);
                                    playFile(list.get(pos).getFilePath());
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });

                            animVocal.setScaleX(1.7f);
                            animVocal.setScaleY(1.7f);

                            anim.start();


//                        final TranslateAnimation anim = new TranslateAnimation( 0, xDest , 0, yDest);
//                        anim.setDuration(600);
////                        anim.setRepeatCount(1);
////                        anim.setRepeatMode(Animation.REVERSE);
////                        anim.setFillAfter(true);
//                        animVocal.setScaleX(1.7f);
//                        animVocal.setScaleY(1.7f);
//                        animVocal.startAnimation(anim);

//                        Animation.AnimationListener animationListener = new Animation.AnimationListener() {
//                            @Override
//                            public void onAnimationStart(Animation animation) {
//                                animVocal.bringToFront();
//                            }
//
//                            @Override
//                            public void onAnimationEnd(Animation animation) {
//
//                                animVocal.clearAnimation();
//
//                                animVocal.setVisibility(View.INVISIBLE);
//                                playFile(list.get(pos).getFilePath());
//                            }
//
//                            @Override
//                            public void onAnimationRepeat(Animation animation) {
//
//                            }
//                        };
//
//                        anim.setAnimationListener(animationListener);

                        }

                    }
                });

                holder.btnUploadDownload.setOnClickListener(new View.OnClickListener() {
                    int pos = position;

                    @Override
                    public void onClick(View v) {

                        RecieveThread resend = list.get(pos);

                        String isErrorInPostVocal = resend.getIsResendError();
                        String isErrorInDownLoadVocal = resend.getIsDownLoadError();

                        if (isErrorInPostVocal != null && isErrorInPostVocal.equalsIgnoreCase(Constants.TRUE)) {
                            dbHelper.updateUploadFailStatusLocally(resend.getFrndChatId(), Constants.FALSE);

//                            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.FILE_PATH;
                            String path = getFilesDir().getAbsolutePath() + "/" + Constants.FILE_PATH;


                            String userId = "";
                            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                            if (pref.contains(Constants.kUserID)) {
                                userId = pref.getString(Constants.kUserID, "");
                            }

                            saveAndPostVocal(userId, resend.getRecieverId(), path, resend.getFilePath(), resend.getDuration(), resend.getThreadId(), resend.getFrndChatId());

                        } else if (isErrorInDownLoadVocal == null || isErrorInDownLoadVocal.equalsIgnoreCase(Constants.TRUE)) {

                            dbHelper.updateDownloadStatus(resend.getFrndChatId());

//                        if (NetworkUtil.getConnectivityStatus(FriendChatActivity.this) == 0) {
//
//                            dbHelper.updateDownloadFailStatus(resend.getFrndChatId());
//                        } else {
//                            //DOWNLOAD AUDIO
//                            String url = Constants.VOCAL_BASE_ADDRESS + resend.getThreadId() + "/" + fileName;
//                            new DownloadAudio().execute(url, resend.getFilePath(), resend.getMsgId(), String.valueOf(resend.getFrndChatId()));
//                        }

                            List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());

                            reloadListView(list);
                        }

                    }
                });

                showHideViews(holder, position);

                return view;
            }
            return view;
        }


        private ShapeDrawable setborder(int color) {
            // Initializing a ShapeDrawable
            ShapeDrawable sd = new ShapeDrawable();

            // Specify the shape of ShapeDrawable
            OvalShape shape = new OvalShape();

            sd.setShape(shape);

            // Specify the border color of shape
            sd.getPaint().setColor(color);

            // Set the border width
            sd.getPaint().setStrokeWidth(Functions.GetPixalFromDP(FriendChatActivity.this, 1));
            sd.getPaint().setAntiAlias(true);

            // Specify the style is a Stroke
            sd.getPaint().setStyle(Paint.Style.STROKE);

            return sd;
        }

        private void showHideViews(ViewHolder holder, int position) {

            RecieveThread thread = arrVocals.get(position);
            int frndChatId = thread.getFrndChatId();
            String msgId = thread.getMsgId();
            String status = thread.getMsgStatus();
            String senderId = thread.getSenderId();
            String threadId = thread.getThreadId();
            String fileName = thread.getFilePath();
            String downloadStatus = thread.getDownLoadStatus();
            String isErrorInPostVocal = thread.getIsResendError();
            String isErrorInDownloadVocal = thread.getIsDownLoadError();

            String userId = "";
            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.kUserID)) {
                userId = pref.getString(Constants.kUserID, "");
            }

            if ((status != null) && (status.equalsIgnoreCase("11") || status.equalsIgnoreCase("1") || status.equalsIgnoreCase("2") ||
                    status.equalsIgnoreCase("3") || status.equalsIgnoreCase("4")) && downloadStatus.equalsIgnoreCase("1")) {

                if (!senderId.equalsIgnoreCase(userId) && ((status.equalsIgnoreCase("11") || status.equalsIgnoreCase("1")))) {
                    updateMessageStatus(Constants.kStatusDelivered, msgId, false);
                    dbHelper.updateMessageStatusLocal(Constants.kStatusDelivered, msgId, threadId);
                }

                holder.progressView.setVisibility(View.INVISIBLE);
                holder.imgVocal.setVisibility(View.VISIBLE);
                holder.imgVocal.setEnabled(true);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.btnUploadDownload.setVisibility(View.INVISIBLE);

            } else if ((isErrorInPostVocal != null && isErrorInPostVocal.equalsIgnoreCase(Constants.TRUE)) || (isErrorInDownloadVocal != null && isErrorInDownloadVocal.equalsIgnoreCase(Constants.TRUE))) {

                holder.progressView.setVisibility(View.INVISIBLE);
                holder.imgVocal.setVisibility(View.INVISIBLE);
                holder.progressBar.setVisibility(View.INVISIBLE);
                holder.btnUploadDownload.setVisibility(View.VISIBLE);

            } else {

                holder.progressView.setVisibility(View.VISIBLE);
                holder.imgVocal.setVisibility(View.VISIBLE);
                holder.imgVocal.setEnabled(false);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.btnUploadDownload.setVisibility(View.INVISIBLE);

                if (status != null && !status.equalsIgnoreCase(Constants.kStatusDelete)) {
                    if (NetworkUtil.getConnectivityStatus(FriendChatActivity.this) == 0) {

                        dbHelper.updateDownloadFailStatus(frndChatId);
                    } else {
                        //DOWNLOAD AUDIO
                        String url = Constants.VOCAL_BASE_ADDRESS + threadId + "/" + fileName;
                        new DownloadAudio().execute(url, fileName, msgId, String.valueOf(frndChatId), status);
                    }
                }

                List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());

                reloadListView(list);


            }

            if (status != null && status.equalsIgnoreCase("5")) {
                showViews(holder, false);
            }

        }


        private void showViews(ViewHolder holder, boolean flag) {

            if (flag) {
                holder.bar.setVisibility(View.VISIBLE);
                holder.btnUploadDownload.setVisibility(View.VISIBLE);
                holder.imgVocal.setVisibility(View.VISIBLE);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.progressView.setVisibility(View.VISIBLE);
            } else {
                holder.bar.setVisibility(View.GONE);
                holder.btnUploadDownload.setVisibility(View.GONE);
                holder.imgVocal.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.GONE);
                holder.progressView.setVisibility(View.GONE);
            }
        }
    }

    /**
     * We need this setter to translate between the information the animator
     * produces (a new "PathPoint" describing the current animated location)
     * and the information that the button requires (an xy location). The
     * setter will be called by the ObjectAnimator given the 'buttonLoc'
     * property string.
     */
    public void setButtonLoc(PathPoint newLoc) {
        animVocal.setTranslationX(newLoc.mX);
        animVocal.setTranslationY(newLoc.mY);
    }

    private void reverseAnim(final boolean flag) {

        int[] locRec = new int[2];
        recordView.getLocationOnScreen(locRec);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        params.leftMargin = getRelativeLeft(recordView) + (recordView.getHeight() / 4); //(int)Functions.getTopLeftCorner(v).x; //getRelativeLeft(v);
        params.topMargin = locRec[1];

        animVocal.setLayoutParams(params);
        animVocal.requestLayout();


        RelativeLayout root = (RelativeLayout) findViewById(R.id.rootLayout);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int statusBarOffset = dm.heightPixels - root.getMeasuredHeight();


        int xDest = dm.widthPixels / 2;
        xDest -= width / 2;
        xDest -= startX;
//        xDest -= loc[0];
        int yDest = dm.heightPixels - startY - (dm.heightPixels - locRec[1]); //dm.heightPixels - (dm.heightPixels - startY) + (recordView.getHeight()/4);// - locRec[1]; //(animVocal.getMeasuredHeight()/2) - statusBarOffset
        yDest += recordView.getWidth() / 4;

        AnimatorPath path = new AnimatorPath();
        path.moveTo(0, 0);
//        path.lineTo(0, 300);

//        path.curveTo(xDest, yDest/2, xDest, yDest, xDest, yDest);

        path.curveTo(-xDest / 4, -yDest / 2, -xDest / 2, -yDest, -xDest, -yDest);

        animVocal.setVisibility(View.VISIBLE);

        // Set up the animation
        final ObjectAnimator anim = ObjectAnimator.ofObject(FriendChatActivity.this, "buttonLoc",
                new PathEvaluator(), path.getPoints().toArray());
        anim.setDuration(800);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                animVocal.bringToFront();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animVocal.clearAnimation();
                animVocal.setVisibility(View.INVISIBLE);

                if (flag) {

                    try {
                        MediaPlayer player = new MediaPlayer();
                        AssetFileDescriptor descriptor = getAssets().openFd("alert_sound/delete.mp3");
                        player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                        descriptor.close();

                        player.prepare();
                        player.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    ExplosionField explosionField = ExplosionField.attach2Window(FriendChatActivity.this); //new ExplosionField(FriendChatActivity.this);
                    explosionField.explode(explodeView);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        anim.start();

//        final TranslateAnimation anim = new TranslateAnimation( 0, -xDest , 0, -yDest);
//        anim.setDuration(600);
////        animVocal.setScaleX(1.7f);
////        animVocal.setScaleY(1.7f);
//        animVocal.startAnimation(anim);
//
//
//        Animation.AnimationListener animationListener = new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//                animVocal.bringToFront();
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//
//                animVocal.clearAnimation();
//
//                if (flag) {
//                    ExplosionField explosionField = ExplosionField.attach2Window(FriendChatActivity.this); //new ExplosionField(FriendChatActivity.this);
//                    explosionField.explode(explodeView);
//                }
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        };
//
//        anim.setAnimationListener(animationListener);


    }

    private int getRelativeLeft(View myView) {
        try {
            if (myView.getParent() == myView.getRootView())
                return myView.getLeft();
            else
                return myView.getLeft() + getRelativeLeft((View) myView.getParent());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private class DownloadAudio extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {

            String downloadpath = arg0[0];
            String fileName = arg0[1];
            String msgId = arg0[2];
            String frndChatId = arg0[3];
            String status = arg0[4];
            try {
                URL url = new URL(downloadpath);
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                if (inputStream != null) {
                    byte[] byteImage = Functions.convertToByte(inputStream);

                    saveFileToExternalStorage(byteImage, fileName, msgId, Integer.valueOf(frndChatId), status);

                    List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());
                    if (list.size() > 0) {
                        List<RecieveThread> sortedList = showHideListViewHeader(list);
                        arrVocals.clear();
                        arrVocals.addAll(sortedList);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                adapter.notifyDataSetChanged();
                            }
                        });
                    }

                }
            } catch (Exception io) {
                io.printStackTrace();

                dbHelper.updateDownloadFailStatus(Integer.valueOf(frndChatId));

            }

            return null;
        }

        private void saveFileToExternalStorage(byte[] audioFile, String name, String msgId, int frndChatId, String status) {

//            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Constants.FILE_PATH;
            String path = getFilesDir().getAbsolutePath() + "/" + Constants.FILE_PATH;

            try {
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdirs();
                }

                OutputStream fOut = null;
                File file = new File(path, name);

                if (!file.exists()) {
                    file.createNewFile();
                }
                fOut = new FileOutputStream(file);

// 100 means no compression, the lower you go, the stronger the compression
//            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.write(audioFile);
                fOut.flush();
                fOut.close();

                MediaPlayer mp = new MediaPlayer();
                mp.setDataSource(path + "/" + name);
                mp.prepare();
                int dur = mp.getDuration();
                mp.release();

                if (status.equalsIgnoreCase(Constants.kStatusSaved)) {
                    dbHelper.updateDownloadStatus(frndChatId, name, dur, Constants.kStatusSaved, msgId);
                } else {
                    updateMessageStatus(Constants.kStatusDelivered, msgId, false);

                    dbHelper.updateDownloadStatus(frndChatId, name, dur, Constants.kStatusDelivered, msgId);
                }


            } catch (Exception e) {
                Log.e("saveToExternalStorage()", e.getMessage());
                dbHelper.updateDownloadFailStatus(frndChatId);
            }
        }

    }

    private void getAllFrndsAndRequestAPI() {


        Handler mainHandler = new Handler(FriendChatActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

//                final ProgressHUD progressHUD = ProgressHUD.show(FriendsActivity.this, "", true, true, null);
                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                final String finalUserId = userId;
                AppManager.getInstance().restAPI.getAllFrndsRequest(userId, new Callback<FrndRequestRes>() {
                    @Override
                    public void success(FrndRequestRes frndRequestRes, Response response) {
//                        progressHUD.dismiss();

                        if (frndRequestRes.getErrorCode() == 0) {

                            List<FriendRequest> arrFrnds = new ArrayList<FriendRequest>();
                            List<FriendRequest> arrFrndRequests = new ArrayList<FriendRequest>();

                            for (int i = 0; i < frndRequestRes.getFriends().size(); i++) {
                                FriendRequest requestRes = frndRequestRes.getFriends().get(i);
                                if (finalUserId.equalsIgnoreCase(requestRes.getUserId1())) {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId1());
                                    friend.setName(requestRes.getName());
                                    friend.setUsername(requestRes.getUsername());
                                    friend.setStatus(requestRes.getStatus());
                                    friend.setImage(requestRes.getImage());

                                    friend.setUserId2(requestRes.getUserId2());
                                    friend.setFrndName(requestRes.getFrndName());
                                    friend.setFrndUsername(requestRes.getFrndUsername());
                                    friend.setFrndStatus(requestRes.getFrndStatus());
                                    friend.setFrndImage(requestRes.getFrndImage());
                                    friend.setIsFrnd(true);

                                    arrFrnds.add(friend);

                                } else {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId2());
                                    friend.setName(requestRes.getFrndName());
                                    friend.setUsername(requestRes.getFrndUsername());
                                    friend.setStatus(requestRes.getFrndStatus());
                                    friend.setImage(requestRes.getFrndImage());

                                    friend.setUserId2(requestRes.getUserId1());
                                    friend.setFrndName(requestRes.getName());
                                    friend.setFrndUsername(requestRes.getUsername());
                                    friend.setFrndStatus(requestRes.getStatus());
                                    friend.setFrndImage(requestRes.getImage());
                                    friend.setIsFrnd(true);

                                    arrFrnds.add(friend);
                                }

                            }

                            for (int i = 0; i < frndRequestRes.getFriendRequests().size(); i++) {
                                FriendRequest requestRes = frndRequestRes.getFriendRequests().get(i);
                                if (finalUserId.equalsIgnoreCase(requestRes.getUserId1())) {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId1());
                                    friend.setName(requestRes.getName());
                                    friend.setUsername(requestRes.getUsername());
                                    friend.setStatus(requestRes.getStatus());
                                    friend.setImage(requestRes.getImage());

                                    friend.setUserId2(requestRes.getUserId2());
                                    friend.setFrndName(requestRes.getFrndName());
                                    friend.setFrndUsername(requestRes.getFrndUsername());
                                    friend.setFrndStatus(requestRes.getFrndStatus());
                                    friend.setFrndImage(requestRes.getFrndImage());
                                    friend.setIsFrnd(false);

                                    arrFrndRequests.add(friend);

                                } else {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId2());
                                    friend.setName(requestRes.getFrndName());
                                    friend.setUsername(requestRes.getFrndUsername());
                                    friend.setStatus(requestRes.getFrndStatus());
                                    friend.setImage(requestRes.getFrndImage());

                                    friend.setUserId2(requestRes.getUserId1());
                                    friend.setFrndName(requestRes.getName());
                                    friend.setFrndUsername(requestRes.getUsername());
                                    friend.setFrndStatus(requestRes.getStatus());
                                    friend.setFrndImage(requestRes.getImage());
                                    friend.setIsFrnd(false);

                                    arrFrndRequests.add(friend);
                                }

                            }

                            Collections.sort(arrFrnds, new CustomSortComparator());
                            Collections.sort(arrFrndRequests, new CustomSortComparator());

                            FrndRequestRes requestRes = new FrndRequestRes();
                            requestRes.setFriends(arrFrnds);
                            requestRes.setFriendRequests(arrFrndRequests);

                            Gson gson = new Gson();
                            String value = gson.toJson(requestRes);

//                            Gson gson = new Gson();
//                            String value = gson.toJson(arrFrnds);

                            VocalsModel model = new VocalsModel();
                            model.setUserId(finalUserId);
                            model.setUrl(Constants.kgetFriendRequest);
                            model.setResponce(value);

                            dbHelper.addNewResponceList(model);


                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

//                          progressHUD.dismiss();
//                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    private class CustomSortComparator implements Comparator<FriendRequest> {
        @Override
        public int compare(FriendRequest obj1, FriendRequest obj2) {
            return obj1.getFrndUsername().compareTo(obj2.getFrndUsername());
        }
    }


    class AsyncRecieveThread extends AsyncTask<String, String, String> {

        ProgressHUD progressHUD = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();



        }

        @Override
        protected String doInBackground(String... params) {

            if (params[1].equalsIgnoreCase(Constants.TRUE)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                    }
                });
            }

            final String pagNo = params[0];
            String userId = "";
            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.kUserID)) {
                userId = pref.getString(Constants.kUserID, "");
            }

            final String thrdId = getVocals.getThreadId();
            String frndId = "";
            if (getVocals.getIsGroup().equalsIgnoreCase("0")) {
                frndId = getVocals.getReceiverId();
            }


            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("user_id", userId));
            nameValuePairs.add(new BasicNameValuePair("thread_id", thrdId));
            nameValuePairs.add(new BasicNameValuePair("friend_id", frndId));
            nameValuePairs.add(new BasicNameValuePair("page_num", pagNo));

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.API_BASE_ADDRESS + "/receiveThread");
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse postResponse = httpClient.execute(httpPost);

                HttpEntity getResEntity = postResponse.getEntity();
                String res = EntityUtils.toString(getResEntity);

                JSONObject jsonObject = new JSONObject(res);

                if (jsonObject.getInt("error_code") == 0) {

                    final JSONArray jsonArray = jsonObject.getJSONArray("thread");

                    final List<RecieveThread> threads = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        RecieveThread thread = new RecieveThread();
                        thread.setMsgId(object.getString("message_id"));
                        thread.setVmemoParticipantTblId(object.getString("vmemo_participant_tbl_id"));
                        thread.setPartcipantId(object.getString("partcipant_id"));
                        thread.setThreadId(object.getString("message_thread_id"));
                        thread.setIsAdmin(object.getString("is_admin"));
                        thread.setChatTitle(object.getString("chat_title"));
                        thread.setChatImage(object.getString("chat_image"));
                        thread.setFilePath(object.getString("file_path"));
                        thread.setMsgStatus(object.getString("message_status"));
                        thread.setSenderId(object.getString("sender_id"));
                        thread.setSenderImage(object.getString("sender_image"));
                        thread.setSenderName(object.getString("sender_name"));
                        thread.setIsGroup(object.getString("is_group"));
                        thread.setColor(object.getString("assigned_color"));
                        thread.setNewMessageStatus(object.getString("new_message_status"));
                        thread.setChatImageThumb(object.getString("chat_image_thumb"));
                        thread.setMsgText(object.getString("message_text"));
                        thread.setTextMessageStatus(object.getString("text_message_status"));
                        thread.setUserGroupCurrentStatus(object.getString("user_group_current_status"));
                        thread.setUserGroupStatusOnMsgPost(object.getString("user_group_status_on_msg_post"));


                        threads.add(thread);

                    }

                    if (threads.size() > 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                heading.setText(Functions.decodeFromNonLossyAscii(threads.get(0).getChatTitle()));
                                String url = Constants.CHAT_IMG_BASE_ADDRESS + threads.get(0).getChatImageThumb();
                                Picasso.with(FriendChatActivity.this).load(url).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgHeader);


                                if (threads.get(0).getIsGroup().equalsIgnoreCase("1") && (threads.get(0).getUserGroupCurrentStatus() == null || !threads.get(0).getUserGroupCurrentStatus().equalsIgnoreCase("0"))) {
                                    inActiveView.setVisibility(View.INVISIBLE);
                                } else if (threads.get(0).getIsGroup().equalsIgnoreCase("1")) {
                                    inActiveView.setVisibility(View.VISIBLE);
                                } else {
                                    inActiveView.setVisibility(View.INVISIBLE);
                                }

                                if (jsonArray.length() >= Constants.NUM_OF_ITEMS_TO_ADD) {
                                    listView.removeHeaderView(header);
                                    listView.addHeaderView(header, null, false);
                                } else {
                                    listView.removeHeaderView(header);
                                    pageNo = pageNo+1;
                                }

                            }
                        });

                    }

                    for (int i = 0; i < threads.size(); i++) {

                        RecieveThread thread = threads.get(i);
                        String threadId = thread.getThreadId();
                        String msgId = thread.getMsgId();
                        String msgStatus = thread.getMsgStatus();
                        String textMsgStatus = thread.getTextMessageStatus();

                        if (thread.getMsgText() != null && !thread.getMsgText().equalsIgnoreCase("null") && !thread.getMsgText().equalsIgnoreCase("")) {
                            if (!dbHelper.alreadyExistVocals(threadId, msgId, textMsgStatus, true)) {
                                if (dbHelper.insertNewVocal(thread)) {
                                }
                            }
                        }
                        else if (!thread.getSenderId().equalsIgnoreCase(userId)) {

                            if (!dbHelper.alreadyExistVocals(threadId, msgId, msgStatus, false)) {
                                if (dbHelper.insertNewVocal(thread)) {

//                                            List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());
//                                            reloadListView(list);

//                                            if (list.size() > 0) {
//                                                arrVocals.clear();
//                                                arrVocals.addAll(list);
//                                                Log.e("VMemos2 ==== ", String.valueOf(list.size()));
//                                                adapter.notifyDataSetChanged();
//                                            }
                                }
                            }
                        } else {

                            if (msgStatus.equalsIgnoreCase("11")) {
                                dbHelper.updateMessageStatusLocal("1", msgId, threadId);
                                updateMessageStatus("1", msgId, false);
                            } else {
                                dbHelper.updateMessageStatusLocal(msgStatus, msgId, threadId);
                            }


//                                    if (list.size() > 0) {
//                                        arrVocals.clear();
//                                        arrVocals.addAll(list);
//                                        Log.e("VMemos3 ==== ", String.valueOf(list.size()));
//                                        adapter.notifyDataSetChanged();
//                                    }

                        }

                    }
                    Log.e("before fetch data", "");

                    final List<RecieveThread> list = dbHelper.getVocalsLocally(getVocals.getThreadId());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            reloadListView(list);
                        }
                    });


                    Log.e("after fetch data", "");


                }

            } catch (JSONException e) {
                if (pageNo > 1) {
                    pageNo = pageNo - 1;
                }
                httpPost.abort();
            } catch (IOException e) {
                if (pageNo > 1) {
                    pageNo = pageNo - 1;
                }
                httpPost.abort();
            }
            catch (Exception e)
            {

            }

            return "";

          /*  NetworkCommunicator comm = new NetworkCommunicator(mActivity);
            return comm.UpdateToken();*/
        }

        @Override
        protected void onPostExecute(String result) {

            Log.i("onPostExecute", "onPostExecute");

            if (progressHUD != null) {
                progressHUD.dismiss();
            }

            header.setEnabled(true);

//            listView.stopLoadMore();
            swipyRefreshLayout.setRefreshing(false);
        }
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            if (getVocals.getIsGroup().equalsIgnoreCase("1") && getVocals.getThreadId() != null && !getVocals.getThreadId().equalsIgnoreCase("")) {

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                getUserGroupCurrentStatusAPI(getVocals.getThreadId(), userId);

            }
        }

    }

    private void getUserGroupCurrentStatusAPI(final String threadId, final String userId) {

        Handler handler = new Handler(FriendChatActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {


                AppManager.getInstance().restAPI.getUserGroupCurrentStatus(threadId, userId, new Callback<GetUserGroupCurrentStatusRes>() {
                    @Override
                    public void success(GetUserGroupCurrentStatusRes groupInfoRes, Response response) {

                        if (groupInfoRes.getErrorCode() == 0) {

                            if (groupInfoRes.getUserGroupCurrentStatus() == null || !groupInfoRes.getUserGroupCurrentStatus().equalsIgnoreCase("0")) {
                                inActiveView.setVisibility(View.INVISIBLE);
                            } else {
                                inActiveView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            inActiveView.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

            }
        };
        handler.post(runnable);

    }

}