package com.vmemos.inc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vmemos.inc.R;

import Util.Constants;
import Util.NetworkUtil;

/**
 * Created by developerclan on 24/02/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        String status = NetworkUtil.getConnectivityStatusString(context);

        if (SettingActivity.tvStatus != null) {
            if (status.equalsIgnoreCase(Constants.CONNECTED)) {
                SettingActivity.tvStatus.setText(Constants.CONNECTED);
                SettingActivity.tvStatus.setTextColor(context.getResources().getColor(R.color.green));
            } else {
                SettingActivity.tvStatus.setText(Constants.NotCONNECTED);
                SettingActivity.tvStatus.setTextColor(context.getResources().getColor(R.color.red));
            }
        }

//        Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
    }
}