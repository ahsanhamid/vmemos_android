package com.vmemos.inc;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import Util.AppManager;

public class WebActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_web);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        TextView text = (TextView) findViewById(R.id.text);
        TextView tvLink = (TextView) findViewById(R.id.tvLink);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvLink.setTypeface(AppManager.getInstance().getRegularTypeface());

        SpannableString content = new SpannableString("http://vmemos.com/download_app");//http://vmemos.com/#download_outer
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvLink.setText(content);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.tvLink:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://vmemos.com/download_app"));
                startActivity(browserIntent);
                break;
        }
    }
}
