package com.vmemos.inc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vmemos.inc.R;

import Model.DefaultRes;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class OtherInfoActivity extends Activity {

    EditText txtEmail,txtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_other_info);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        Button done = (Button) findViewById(R.id.btnDone);
        txtEmail = (EditText) findViewById(R.id.et_email);
        txtPass = (EditText) findViewById(R.id.et_pass);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtEmail.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtPass.setTypeface(AppManager.getInstance().getRegularTypeface());

        txtEmail.setText(AppManager.getInstance().getUser().getEmailAddress());

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.btnDone:

                if (txtPass.getText().toString().length()<5){
                    Toast.makeText(getApplicationContext(), "Password should have atleast 5 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String email;
                if (AppManager.getInstance().getUser().getEmailAddress().equalsIgnoreCase(txtEmail.getText().toString())){
                    email = "";
                }
                else {
                    email = txtEmail.getText().toString();
                }

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                changeInfo(userId,email,txtPass.getText().toString());
                break;
        }
    }

    private void changeInfo(final String userId, final String email, final String pass) {

        Handler mainHandler = new Handler(OtherInfoActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(OtherInfoActivity.this, "", true, true, null,false);

                AppManager.getInstance().restAPI.updateInfo(userId, email, pass, new Callback<DefaultRes>() {
                    @Override
                    public void success(DefaultRes defaultRes, Response response) {
                        if (defaultRes.getErrorCode() == 0){
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg()+"/Please logIn again!", Toast.LENGTH_LONG).show();

                            clearAppData();

                            Intent intent = new Intent(OtherInfoActivity.this, SignInActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        }
                        else {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);
    }

    private void clearAppData(){
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        pref.edit().clear().apply();
    }

}
