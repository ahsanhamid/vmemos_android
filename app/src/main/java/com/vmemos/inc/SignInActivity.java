package com.vmemos.inc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vmemos.inc.R;

import Model.RegisterRes;
import Model.User;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignInActivity extends Activity {

    EditText txtUserName,txtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sign_in);

        AppManager.getInstance().setFont(this);

        Button btnSignIn = (Button) findViewById(R.id.btn_signin);
        Button btnForgot = (Button) findViewById(R.id.btn_forgot);
        Button btnReg = (Button) findViewById(R.id.btn_reg);
        txtUserName = (EditText) findViewById(R.id.et_username);
        txtPass = (EditText) findViewById(R.id.et_pass);

        btnSignIn.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnForgot.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnReg.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtUserName.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtPass.setTypeface(AppManager.getInstance().getRegularTypeface());

    }


    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_signin:
//                startActivity(new Intent(SignInActivity.this,TabActivity.class));
                signInAPI();
                break;
            case R.id.btn_forgot:
                startActivity(new Intent(SignInActivity.this,ForgotPassActivity.class));
                break;
            case R.id.btn_reg:
                startActivity(new Intent(SignInActivity.this,RegisterActivity.class));
                break;
        }
    }

    private void signInAPI(){

        Handler mainHandler = new Handler(SignInActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(SignInActivity.this, "", true, true, null,false);

                AppManager.getInstance().restAPI.loginUser(txtUserName.getText().toString(), txtPass.getText().toString(), "", "", new Callback<RegisterRes>() {
                    @Override
                    public void success(RegisterRes signInRes, Response response) {

                        progressHUD.dismiss();
                        if (signInRes.getErrorCode()==0){

                            AppManager.getInstance().setUser(signInRes.getUser().get(0));
                            AppManager.getInstance().setNotificationSounds(signInRes.getNotificationSounds());
                            AppManager.getInstance().setPushSetting(Functions.getPushSettings(signInRes.getUser().get(0)));

                            User user = AppManager.getInstance().getUser();

                            if (user.getUserStatus().equalsIgnoreCase(Constants.kUserInActive)){
                                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString(Constants.kUserID, user.getUserId());
                                editor.apply();

                                startActivity(new Intent(SignInActivity.this, ImageCaptchaActivity.class));
                                return;
                            }

                            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(Constants.kUserID,user.getUserId());
                            editor.putString(Constants.kUserName,txtUserName.getText().toString());
                            editor.putString(Constants.kPassword,txtPass.getText().toString());
                            editor.putString(Constants.kUserStatus,user.getUserStatus());

                            Gson gson = new Gson();
                            String userArr = gson.toJson(user);
                            editor.putString(Constants.kUserArray, userArr);
                            String soundArr = gson.toJson(AppManager.getInstance().getNotificationSounds());
                            editor.putString(Constants.kSounds, soundArr);
                            editor.apply();

                            startActivity(new Intent(SignInActivity.this,TabActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP));;//.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            try {
                                finish();
                            }catch (Exception e){
                                e.printStackTrace();
                            }


                        }
                        else {
                            Toast.makeText(getApplicationContext(),signInRes.getMsg(),Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(),error.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

}
