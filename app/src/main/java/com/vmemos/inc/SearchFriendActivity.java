package com.vmemos.inc;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.vmemos.inc.R;

import java.util.ArrayList;
import java.util.List;

import Model.DefaultRes;
import Model.SearchFrnd;
import Model.SearchFrndsRes;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchFriendActivity extends Activity {

    EditText txtSearch;
    ListView listView;
    Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search_friend);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.btnBack);
        Button search = (Button) findViewById(R.id.btnSearch);
        txtSearch = (EditText) findViewById(R.id.et_search);
        listView = (ListView) findViewById(R.id.listView);

        final ImageView btnClear = (ImageView) findViewById(R.id.imgClear);
        btnClear.setVisibility(View.INVISIBLE);

        context = SearchFriendActivity.this;

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        search.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtSearch.setTypeface(AppManager.getInstance().getRegularTypeface());

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtSearch.getText().toString().equalsIgnoreCase("")) {
                    btnClear.setVisibility(View.INVISIBLE);
                }
                else {
                    btnClear.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.btnSearch:
                if (txtSearch.getText().toString().equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter UserName!", Toast.LENGTH_SHORT).show();
                    return;
                }

                searchFrnds(txtSearch.getText().toString());
                break;
            case R.id.imgClear:
                txtSearch.setText("");
                break;
        }
    }

    private void searchFrnds(final String userName) {

        Handler mainHandler = new Handler(SearchFriendActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                AppManager.getInstance().restAPI.searchFriends(userName, new Callback<SearchFrndsRes>() {
                    @Override
                    public void success(SearchFrndsRes searchFrndsRes, Response response) {
                        progressHUD.dismiss();

                        if (searchFrndsRes.getErrorCode() == 0){

                            String userId = "";
                            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                            if (pref.contains(Constants.kUserID)) {
                                userId = pref.getString(Constants.kUserID, "");
                            }

                            List<SearchFrnd> searchFrnds = new ArrayList<SearchFrnd>();
                            for (int i=0;i<searchFrndsRes.getSearchFrnds().size();i++){
                                if (!userId.equalsIgnoreCase(searchFrndsRes.getSearchFrnds().get(i).getFrndId())){
                                    searchFrnds.add(searchFrndsRes.getSearchFrnds().get(i));
                                }
                            }

                            CustomAdapter adapter = new CustomAdapter(context,R.layout.custom_contact_cell,searchFrnds);
                            listView.setAdapter(adapter);

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    class CustomAdapter extends ArrayAdapter<SearchFrnd> {

        Context context;
        List<SearchFrnd> list;

        public CustomAdapter(Context context, int resource, List<SearchFrnd> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        private class ViewHolder {
            TextView name;
            ImageView imgProfile;
            ImageView btnAdd;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder = null;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_contact_cell, parent, false);

                holder = new ViewHolder();
                holder.name = (TextView) view.findViewById(R.id.etName);
                holder.imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
                holder.btnAdd = (ImageView) view.findViewById(R.id.btnAdd);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.name.setTypeface(AppManager.getInstance().getRegularTypeface());
            holder.name.setText(list.get(position).getUsername());

            String url = Constants.IMG_BASE_ADDRESS + list.get(position).getFrndId() + "/thumb_" + list.get(position).getImage();

            Picasso.with(SearchFriendActivity.this).load(url).placeholder(R.drawable.friends_profile_pic_placeholder).into(holder.imgProfile);

            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                int pos = position;
                @Override
                public void onClick(View v) {

                    String userId="";
                    SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                    if (pref.contains(Constants.kUserID)){
                        userId = pref.getString(Constants.kUserID,"");
                    }

                    sendFrndReq(userId,list.get(pos).getFrndId());

                }
            });


            return view;
        }
    }

    private void sendFrndReq(final String userId, final String frndId) {

        Handler mainHandler = new Handler(SearchFriendActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                AppManager.getInstance().restAPI.sendFriendReq(userId, frndId, new Callback<DefaultRes>() {
                    @Override
                    public void success(DefaultRes defaultRes, Response response) {
                        progressHUD.dismiss();
                        if (defaultRes.getErrorCode() == 0) {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }


}
