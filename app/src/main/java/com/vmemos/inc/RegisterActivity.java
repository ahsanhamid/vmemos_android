package com.vmemos.inc;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.squarecamera.CameraActivity;
import com.desmond.squarecamera.ImageUtility;
import com.google.gson.Gson;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.vmemos.inc.R;

import java.io.ByteArrayOutputStream;
import java.io.File;

import Model.RegisterRes;
import Model.User;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class RegisterActivity extends Activity implements
        ImageChooserListener {

    EditText txtName, txtUserName, txtEmail, txtPass;
    CheckBox chkAgree;
    CircleImageView img;
    //    String tempImagePath = "";
    File tempImageFile = null;
    private int MY_PERMISSIONS_REQUEST = 12;
    boolean isCapture;

    Point mSize;

    ImageChooserManager imageChooserManager;
    String filePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);

        AppManager.getInstance().setFont(this);

        AppManager.getInstance().setCropImage(null);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.btnBack);
        Button signUp = (Button) findViewById(R.id.btn_signup);
        TextView text = (TextView) findViewById(R.id.text);
        TextView term = (TextView) findViewById(R.id.lbl_term);
        txtName = (EditText) findViewById(R.id.et_name);
        txtUserName = (EditText) findViewById(R.id.et_username);
        txtEmail = (EditText) findViewById(R.id.et_email);
        txtPass = (EditText) findViewById(R.id.et_pass);
        chkAgree = (CheckBox) findViewById(R.id.chk_agree);
        img = (CircleImageView) findViewById(R.id.imgProfile);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        signUp.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtName.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtUserName.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtEmail.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtPass.setTypeface(AppManager.getInstance().getRegularTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        term.setTypeface(AppManager.getInstance().getRegularTypeface());

        Display display = getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);


        term.setText(Html.fromHtml("I agree to " + "<font color=\"#3ED037\">" + "terms and conditions" + "</font>"));
        //term.setText(Html.fromHtml("<font color=\"#3ED037\">" + "I agree to " + "</font>" + "<font color=\"#3ED037\">" + "terms and conditions" + "</font>"));
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bitmap bitmap = AppManager.getInstance().getCropImage();
        if (bitmap != null) {
            img.setImageBitmap(bitmap);
        }
        tempImageFile = AppManager.getInstance().getImgFile();

    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                registerUser();
//                startActivity(new Intent(RegisterActivity.this,ImageCaptchaActivity.class));
                break;
            case R.id.relBack:
                finish();
                break;
            case R.id.imgProfile:
                chooseImage();
                break;
            case R.id.lbl_term:
                Intent intent = new Intent(RegisterActivity.this, AboutActivity.class);
                intent.putExtra("FLAG", false);
                startActivity(intent);
                break;
        }
    }

    private void registerUser() {
        if (txtName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Enter name!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (txtUserName.getText().toString().length() < 3) {
            Toast.makeText(getApplicationContext(), "Username should have atleast 3 characters!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (txtEmail.getText().toString().equalsIgnoreCase("") || !Functions.isValidEmail(txtEmail.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Enter valid email!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (txtPass.getText().toString().length() < 5) {
            Toast.makeText(getApplicationContext(), "Password should have atleast 5 characters!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!chkAgree.isChecked()) {
            return;
        }

        TypedFile typedImage = null;
        if (tempImageFile != null) {
            typedImage = new TypedFile("image/jpeg", tempImageFile);
        }

        Handler mainHandler = new Handler(RegisterActivity.this.getMainLooper());
        final TypedFile finalTypedImage = typedImage;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(RegisterActivity.this, "", true, true, null,false);

                String name = Functions.encodeToNonLossyAscii(txtName.getText().toString());


                AppManager.getInstance().restAPI.registerUser("", txtUserName.getText().toString(), txtPass.getText().toString(),
                        txtEmail.getText().toString(), name, "", "", finalTypedImage, new Callback<RegisterRes>() {
                            @Override
                            public void success(RegisterRes registerRes, Response response) {

                                progressHUD.dismiss();

                                if (registerRes.getErrorCode() == 0) {

                                    AppManager.getInstance().setUser(registerRes.getUser().get(0));
                                    AppManager.getInstance().setNotificationSounds(registerRes.getNotificationSounds());
                                    AppManager.getInstance().setPushSetting(Functions.getPushSettings(registerRes.getUser().get(0)));

                                    User user = AppManager.getInstance().getUser();
                                    SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString(Constants.kUserID, user.getUserId());
                                    editor.putString(Constants.kUserName, txtUserName.getText().toString());
                                    editor.putString(Constants.kPassword, txtPass.getText().toString());
                                    editor.putString(Constants.kUserStatus, user.getUserStatus());

                                    Gson gson = new Gson();
                                    String userArr = gson.toJson(user);
                                    editor.putString(Constants.kUserArray, userArr);
                                    String soundArr = gson.toJson(AppManager.getInstance().getNotificationSounds());
                                    editor.putString(Constants.kSounds, soundArr);

                                    editor.apply();

                                    clear();

//                                    File file = new File(tempImagePath);
                                    if (tempImageFile != null) {
                                        boolean deleted = tempImageFile.delete();
                                    }
                                    startActivity(new Intent(RegisterActivity.this, ImageCaptchaActivity.class));


                                } else {
                                    Toast.makeText(getApplicationContext(), registerRes.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressHUD.dismiss();
                                Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        };
        mainHandler.post(runnable);

    }

    private void clear() {
        txtName.setText("");
        txtPass.setText("");
        txtUserName.setText("");
        txtEmail.setText("");

    }

    private void chooseImage() {



        // Here, thisActivity is the current activity
        if ((ContextCompat.checkSelfPermission(RegisterActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(RegisterActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(RegisterActivity.this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }

            if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(RegisterActivity.this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }

        } else {
            selectImage();
        }

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Delete Photo", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this, AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    isCapture = true;
                    Intent startCustomCameraIntent = new Intent(RegisterActivity.this, CameraActivity.class);
                    startActivityForResult(startCustomCameraIntent, 1);
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, 1);
                } else if (items[item].equals("Choose from Library")) {
                    isCapture = false;

                    chooseImageFromGallary();


                } else if (items[item].equals("Delete Photo")) {

                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.friends_profile_pic_placeholder);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    largeIcon.compress(Bitmap.CompressFormat.PNG, 80, stream);
                    byte[] byteArray = stream.toByteArray();
                    Functions.saveImageToExternalStorage(RegisterActivity.this,byteArray, "profile_pic");
                    img.setImageBitmap(largeIcon);
                    AppManager.getInstance().setImg(largeIcon);
                    tempImageFile = new File(Functions.getImage(RegisterActivity.this,"profile_pic"));
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void chooseImageFromGallary() {
        imageChooserManager = new ImageChooserManager(this,
                291, false);
        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
//            pbar.setVisibility(View.VISIBLE);
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onImageChosen(final ChosenImage image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                String originalFilePath = image.getFilePathOriginal();

                Intent intent = new Intent(RegisterActivity.this, CropActivity.class);
                intent.putExtra("Path", originalFilePath);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onError(String reason) {

    }

    @Override
    public void onImagesChosen(ChosenImages images) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 12:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    selectImage();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
        }

        // other 'case' lines to check for other
        // permissions this app might request

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if ((requestCode == 1) && (resultCode == RESULT_OK)) {
            // When the user is done picking a picture, let's start the CropImage Activity,
            // setting the output image file and size to 200x200 pixels square.



            Uri photoUri = data.getData();
            // Get the bitmap in according to the width of the device
            Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(photoUri.getPath(), mSize.x, mSize.x);



            Intent intent = new Intent(RegisterActivity.this, CropActivity.class);
            intent.putExtra("Path", photoUri.getPath());
            startActivity(intent);


        } else if ((requestCode == 291) && (resultCode == RESULT_OK)) {


            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);

        }
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, 291, false);
        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }


    private String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor;
        if (Build.VERSION.SDK_INT >= 19) {
            // Will return "image:x*"
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection, sel, new String[]{id}, null);
        } else {
            cursor = getContentResolver().query(uri, projection, null, null, null);
        }
        String path = null;
        try {
            int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            path = cursor.getString(column_index).toString();
            cursor.close();
        } catch (NullPointerException e) {

        }
        return path;

    }
}
