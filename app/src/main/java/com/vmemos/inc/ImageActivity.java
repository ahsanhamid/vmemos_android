package com.vmemos.inc;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.desmond.squarecamera.CameraActivity;
import com.desmond.squarecamera.ImageUtility;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;

import Util.AppManager;
import Util.Constants;
import Util.Functions;

public class ImageActivity extends Activity implements
        ImageChooserListener {

    boolean isFromGroup=false,isFromChat=false,isFromFrnd=false,isFromProfile=false,isAdmin = false;
    String imgUrl;
    ImageView img;
    boolean isCapture;
    private int MY_PERMISSIONS_REQUEST = 12;

    Point mSize;

    ImageChooserManager imageChooserManager;
    String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_image);

        AppManager.getInstance().setFont(this);

//        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.friends_profile_pic_placeholder);
//        AppManager.getInstance().setCropImage(null);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        Button btnEdit = (Button) findViewById(R.id.btnEdit);
        img = (ImageView) findViewById(R.id.img);

        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        btnEdit.setTypeface(AppManager.getInstance().getRegularTypeface());


        Display display = getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);


        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            if (bundle.containsKey(Constants.FROM_GROUP)){
                isFromGroup = bundle.getBoolean(Constants.FROM_GROUP);
                isAdmin = bundle.getBoolean("ADMIN");
                imgUrl = bundle.getString(Constants.URL);
            }
            else if (bundle.containsKey(Constants.FROM_CHAT)){
                isFromChat = bundle.getBoolean(Constants.FROM_CHAT);
                imgUrl = bundle.getString(Constants.URL);
            }
            else if (bundle.containsKey(Constants.FROM_FRND)){
                isFromFrnd = bundle.getBoolean(Constants.FROM_FRND);
                imgUrl = bundle.getString(Constants.URL);
            }
            else if (bundle.containsKey(Constants.FROM_PROFILE)){
                isFromProfile = bundle.getBoolean(Constants.FROM_PROFILE);
                imgUrl = bundle.getString(Constants.URL);
            }
        }

        if (isFromGroup){
            Picasso.with(ImageActivity.this).load(new File(imgUrl)).placeholder(R.drawable.friends_profile_pic_placeholder).into(img);
            if (isAdmin) {
                btnEdit.setVisibility(View.VISIBLE);
            }
            else {
                btnEdit.setVisibility(View.INVISIBLE);
            }
            return;
        }
        if (isFromChat){
            Picasso.with(ImageActivity.this).load(new File(imgUrl)).placeholder(R.drawable.friends_profile_pic_placeholder).into(img);
            btnEdit.setVisibility(View.INVISIBLE);
            return;
        }
        if (isFromFrnd){
            Picasso.with(ImageActivity.this).load(new File(imgUrl)).placeholder(R.drawable.friends_profile_pic_placeholder).into(img);
            btnEdit.setVisibility(View.INVISIBLE);
            return;
        }
        if (isFromProfile){
            Picasso.with(ImageActivity.this).load(new File(imgUrl)).placeholder(R.drawable.friends_profile_pic_placeholder).into(img);
            btnEdit.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        Bitmap bitmap = AppManager.getInstance().getCropImage();
        if (bitmap!=null) {
            img.setImageBitmap(bitmap);
        }

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.btnEdit:
                chooseImage();
                break;
        }
    }

    private void chooseImage() {


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(ImageActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ImageActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(ImageActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else {
            selectImage();
        }



    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 12:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    selectImage();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request

    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Delete Photo", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(ImageActivity.this, AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    isCapture = true;
                    Intent startCustomCameraIntent = new Intent(ImageActivity.this, CameraActivity.class);
                    startActivityForResult(startCustomCameraIntent, 1);

                } else if (items[item].equals("Choose from Library")) {
                    isCapture = false;

                    chooseImageFromGallary();


                } else if (items[item].equals("Delete Photo")) {

                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.friends_profile_pic_placeholder);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    largeIcon.compress(Bitmap.CompressFormat.PNG, 80, stream);
                    byte[] byteArray = stream.toByteArray();
                    Functions.saveImageToExternalStorage(ImageActivity.this,byteArray,"profile_pic");
                    img.setImageBitmap(largeIcon);
                    AppManager.getInstance().setImg(largeIcon);
                    AppManager.getInstance().setCropImage(largeIcon);// null
                    AppManager.getInstance().setIsImageChanged(true);
                    AppManager.getInstance().setIsImageDelete(true);
                    AppManager.getInstance().setImgFile(new File(Functions.getImage(ImageActivity.this,"profile_pic")));
                }else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void chooseImageFromGallary() {
        imageChooserManager = new ImageChooserManager(this,
                291, false);
        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
//            pbar.setVisibility(View.VISIBLE);
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        File croppedImageFile = new File(getFilesDir(), "test.jpg");

        if ((requestCode == 1) && (resultCode == RESULT_OK)) {
            // When the user is done picking a picture, let's start the CropImage Activity,
            // setting the output image file and size to 200x200 pixels square.

//            if (isCapture) {

            Uri photoUri = data.getData();
            // Get the bitmap in according to the width of the device
            Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(photoUri.getPath(), mSize.x, mSize.x);


                Intent intent = new Intent(ImageActivity.this,CropActivity.class);
                intent.putExtra("Path",photoUri.getPath());
                startActivity(intent);


        }
        else if ((requestCode == 291) && (resultCode == RESULT_OK)) {


            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);

        }

    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, 291, false);
        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    @Override
    public void onImageChosen(final ChosenImage image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                String originalFilePath = image.getFilePathOriginal();

                Intent intent = new Intent(ImageActivity.this, CropActivity.class);
                intent.putExtra("Path", originalFilePath);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onError(String reason) {

    }

    @Override
    public void onImagesChosen(ChosenImages images) {

    }


    private String getPath(Uri uri) {

        if (uri != null) {
            String[] projection = {MediaStore.Images.Media.DATA};
            String path = null;
            try {
            Cursor cursor;
            if (Build.VERSION.SDK_INT >= 19) {
                // Will return "image:x*"
                String wholeID = DocumentsContract.getDocumentId(uri);
                // Split at colon, use second item in the array
                String id = wholeID.split(":")[1];
                // where id is equal to
                String sel = MediaStore.Images.Media._ID + "=?";

                cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        projection, sel, new String[]{id}, null);
            } else {
                cursor = getContentResolver().query(uri, projection, null, null, null);
            }


                int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                path = cursor.getString(column_index).toString();
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return path;
        }
    return "";
    }
}
