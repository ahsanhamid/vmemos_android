package com.vmemos.inc;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.vmemos.inc.R;

import java.io.IOException;

import Model.UpdateTokenRes;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TabActivity extends android.app.TabActivity {

    public static TabHost tabHost;
    private BroadcastReceiver broadcastReceiver;
    private boolean isReceiverRegistered = false;
    private static final String TAG = "TabActivity";

    SharedPreferences mySharedPreferences;

    String SENDER_ID;

    GoogleCloudMessaging gcm;
    String regid;

    boolean haveContactPermission=false,haveAudioPermission=false,haveReadPermission=false,haveWritePermission=false,haveCameraPermission=false;

    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private static final int PERMISSION_REQUEST_ALL = 0;
    private static final int PERMISSION_REQUEST_SINGLE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE); //FEATURE_ACTION_BAR
        setContentView(R.layout.activity_tab);

        AppManager.getInstance().setFont(this);

        SENDER_ID = getString(R.string.gcm_defaultSenderId);



        int mode = Activity.MODE_PRIVATE;
        mySharedPreferences = getSharedPreferences(
                Constants.MYPREFS, mode);


        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(this);
            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }

//        String token = mySharedPreferences.getString(Constants.PROPERTY_REG_ID,"");
//
//        sendRegistrationToServer(token);

//        broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                SharedPreferences sharedPreferences =
//                        PreferenceManager.getDefaultSharedPreferences(context);
//                boolean sentToken = sharedPreferences
//                        .getBoolean(Constants.SENT_TOKEN_TO_SERVER, false);
//                if (sentToken) {
//                    Log.v(TAG,"Token Sent Successfully");
//                } else {
//                    Log.v(TAG, "Error in Token sent");
//                }
//            }
//        };

        // Registering BroadcastReceiver
//        registerReceiver();

//        if (checkPlayServices()) {
//            Intent intent = new Intent(this, GcmIntentService.class);
//            startService(intent);
//        }


        int index = 0;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("INDEX")){

            index = bundle.getInt("INDEX");

        }


        tabHost = (TabHost) findViewById(android.R.id.tabhost);
//        tabHost.getTabWidget().setBackgroundColor(getResources().getColor(R.color.tab_bg));


        tabHost.addTab(tabHost.newTabSpec("tab1")
                .setIndicator(prepareTabView("Vocals", R.drawable.tab_selector_vocal))
                .setContent(new Intent(this, TabGroup1Activity.class)));


        tabHost.addTab(tabHost.newTabSpec("tab2")
                .setIndicator(prepareTabView("Friends", R.drawable.tab_selector_friend))
                .setContent(new Intent(this, TabGroup2Activity.class)));

        tabHost.addTab(tabHost.newTabSpec("tab3")
                .setIndicator(prepareTabView("Contacts", R.drawable.tab_selector_contact))
                .setContent(new Intent(this, TabGroup3Activity.class)));

        tabHost.setCurrentTab(index);


        tabHost.getTabWidget().setStripEnabled(false);

        Display display = getWindowManager().getDefaultDisplay();
        final int width = display.getWidth();

        for (int i = 0; i < 3; i++) {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((width / 3), Functions.GetPixalFromDP(TabActivity.this,50));
            params.setMargins(1,2,1,0);
            tabHost.getTabWidget().getChildAt(i).setLayoutParams(params);

        }

        setSelectedTabColor();

        getAllPermissions();



        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                setSelectedTabColor();
            }
        });


    }

    private String getRegistrationId(Context context) {
		/* final SharedPreferences prefs = getGcmPreferences(context);*/




        String registrationId = mySharedPreferences.getString(Constants.PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        //  int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (Functions.getAppVersion(this) != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void sendRegistrationToServer(final String registrationToken) {

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)) {
            userId = pref.getString(Constants.kUserID, "");
        }

        Handler handler = new Handler(TabActivity.this.getMainLooper());
        final String finalUserId = userId;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                AppManager.getInstance().restAPI.sendToken(finalUserId, registrationToken, Constants.DEVICE_TYPE, new Callback<UpdateTokenRes>() {
                    @Override
                    public void success(UpdateTokenRes updateTokenRes, Response response) {

                        if (updateTokenRes.getErrorCode() == 1){
                            Log.i("Registration","Token sent");
                        }
                        else {
                            Log.i("Registration","Token sent Failed");
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.i("Registration","Token sent Failed");
                    }
                });
            }
        };
        handler.post(runnable);
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(TabActivity.this);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    // sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
//                    pm.setString(PreferenceManager.PROPERTY_REG_ID, regid);

//                    String registrationId = mySharedPreferences.getString(Constants.PROPERTY_REG_ID, "");
                    int appVersion = mySharedPreferences.getInt(Constants.PROPERTY_APP_VERSION, Integer.MIN_VALUE);

                    SharedPreferences.Editor editor = mySharedPreferences.edit();
                    editor.putString(Constants.PROPERTY_REG_ID, regid);
                    editor.putInt(Constants.PROPERTY_APP_VERSION, appVersion);
                    editor.commit();


                    String token = mySharedPreferences.getString(Constants.PROPERTY_REG_ID,"");

                    sendRegistrationToServer(token);

//                    PreferenceManager.setAppVersion(getAppVersion(mActivity), mActivity);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                // mDisplay.append(msg + "\n");
            }
        }.execute(null, null, null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    private void setSelectedTabColor() {
        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.white)); //unselected

        int tab = tabHost.getCurrentTab();
        tabHost.getTabWidget().getChildAt(tab).setBackgroundColor(getResources().getColor(R.color.green));//selected
    }

    private  View prepareTabView(String text, int resId) {
        View view = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imgFriend);
        TextView tv = (TextView) view.findViewById(R.id.etUserName);
        tv.setTypeface(AppManager.getInstance().getRegularTypeface());
        iv.setImageResource(resId);
        tv.setText(text);
        return view;
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, Constants.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TabActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                    new IntentFilter(Constants.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    private void getAllPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Verify that all required contact permissions have been granted.
            // BEGIN_INCLUDE(startCamera)
            // Check if the Camera permission has been granted
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                haveAudioPermission = true;
            } if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                haveContactPermission = true;
            }
            else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                haveReadPermission = true;
            }
            else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                haveWritePermission = true;
            }
            else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                haveCameraPermission = true;
            }

            if(!haveAudioPermission && !haveReadPermission && !haveWritePermission && !haveCameraPermission) {
                // Permission is missing and must be requested.
                requestAllPermissions();
            }
            else {

                if (!haveAudioPermission) {
                    requestSinglePermissions(Manifest.permission.RECORD_AUDIO);
                } else if (!haveReadPermission) {
                    requestSinglePermissions(Manifest.permission.READ_EXTERNAL_STORAGE);
                } else if (!haveWritePermission) {
                    requestSinglePermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                } else if (!haveCameraPermission) {
                    requestSinglePermissions(Manifest.permission.CAMERA);
                }
            }

            // END_INCLUDE(startCamera)

        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
//            resolver = getParent().getContentResolver();
//            phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
//
//            new LoadContact().execute();
        }
    }

    private void requestAllPermissions() {
        // Permission has not been granted and must be requested.
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
//            // Provide an additional rationale to the user if the permission was not granted
//            // and the user would benefit from additional context for the use of the permission.
//            // Display a SnackBar with a button to request the missing permission.
////            Snackbar.make(mLayout, "READ_CONTACTS access is required to display the CONTACTS preview.",
////                    Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////                    // Request the permission
////                    ActivityCompat.requestPermissions(activity,
////                            new String[]{Manifest.permission.READ_CONTACTS},
////                            PERMISSION_REQUEST_CONTACTS);
////                }
////            }).show();
//
//            ActivityCompat.requestPermissions(this,
//                    PERMISSIONS_CONTACT,
//                    PERMISSION_REQUEST_ALL);
//
//        } else
        {

            ActivityCompat.requestPermissions(this, PERMISSIONS_CONTACT,
                    PERMISSION_REQUEST_ALL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode){

            case PERMISSION_REQUEST_ALL:

                haveContactPermission = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                haveAudioPermission = grantResults[1]==PackageManager.PERMISSION_GRANTED;
                haveReadPermission = grantResults[2]==PackageManager.PERMISSION_GRANTED;
                haveWritePermission = grantResults[3]==PackageManager.PERMISSION_GRANTED;
                haveCameraPermission = grantResults[4]==PackageManager.PERMISSION_GRANTED;


                if (!haveAudioPermission){
                    requestSinglePermissions(Manifest.permission.RECORD_AUDIO);
                }
                else if (!haveReadPermission){
                    requestSinglePermissions(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
                else if (!haveWritePermission){
                    requestSinglePermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                else if (!haveCameraPermission){
                    requestSinglePermissions(Manifest.permission.CAMERA);
                }

                break;

            case PERMISSION_REQUEST_SINGLE:

                if (permissions[0].equalsIgnoreCase(Manifest.permission.RECORD_AUDIO)){
                    haveAudioPermission = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                }
                else if (permissions[0].equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    haveReadPermission = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                }
                if (permissions[0].equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    haveWritePermission = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                }
                if (permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA)){
                    haveCameraPermission = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                }

                if (!haveAudioPermission){
                    requestSinglePermissions(Manifest.permission.RECORD_AUDIO);
                }
                else if (!haveReadPermission){
                    requestSinglePermissions(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
                else if (!haveWritePermission){
                    requestSinglePermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                else if (!haveCameraPermission){
                    requestSinglePermissions(Manifest.permission.CAMERA);
                }

                break;

        }

    }

    private void requestSinglePermissions(String permission) {

//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//
//            ActivityCompat.requestPermissions(this,
//                    new String[]{permission},
//                    PERMISSION_REQUEST_SINGLE);
//
//        } else
        {

            ActivityCompat.requestPermissions(this, new String[]{permission},
                    PERMISSION_REQUEST_SINGLE);
        }
    }
}
