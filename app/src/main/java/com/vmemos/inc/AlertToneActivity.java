package com.vmemos.inc;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import Model.NotificationSound;
import Util.AppManager;
import Util.Constants;

public class AlertToneActivity extends Activity {

    boolean isMsg;
    MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_alert_tone);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        ListView listView = (ListView) findViewById(R.id.listView);

        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isMsg = bundle.getBoolean(Constants.FOR_MSG);
        }

        List<NotificationSound> list = AppManager.getInstance().getNotificationSounds();
        CustomAdapter adapter = new CustomAdapter(this, R.layout.custom_tone_cell, list);
        listView.setAdapter(adapter);

    }

    @Override
    protected void onPause() {
        super.onPause();

        stopPlay();

    }

    private void stopPlay(){
        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
        }
    }


    class CustomAdapter extends ArrayAdapter<NotificationSound> {

        Context context;
        List<NotificationSound> list;

        public CustomAdapter(Context context, int resource, List<NotificationSound> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        private class ViewHolder {
            TextView text;
            ImageView img;
            RelativeLayout relative;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder = null;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_tone_cell, parent, false);

                holder = new ViewHolder();
                holder.text = (TextView) view.findViewById(R.id.text);
                holder.img = (ImageView) view.findViewById(R.id.img);
                holder.relative = (RelativeLayout) view.findViewById(R.id.relative);

                holder.text.setTypeface(AppManager.getInstance().getRegularTypeface());

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.text.setTypeface(AppManager.getInstance().getRegularTypeface());
            holder.text.setText(list.get(position).getName().split("\\.")[0]);


            if (isMsg) {
                if (list.get(position).getId().equalsIgnoreCase(AppManager.getInstance().getMsgTone())) {
                    holder.img.setVisibility(View.VISIBLE);
                }
                else {
                    holder.img.setVisibility(View.INVISIBLE);
                }
            }
            else {
                if (list.get(position).getId().equalsIgnoreCase(AppManager.getInstance().getGroupTone())) {
                    holder.img.setVisibility(View.VISIBLE);
                }
                else {
                    holder.img.setVisibility(View.INVISIBLE);
                }
            }


            final ViewHolder finalHolder = holder;
            holder.relative.setOnClickListener(new View.OnClickListener() {
                int pos = position;
                ViewHolder h = finalHolder;

                @Override
                public void onClick(View v) {

                    try {

                        stopPlay();

                        String sound = list.get(pos).getName();

                        if (sound.split("\\.")[0].equalsIgnoreCase("tri-tone") || sound.split("\\.")[0].equalsIgnoreCase("bell")
                                || sound.split("\\.")[0].equalsIgnoreCase("glass")){
                            sound = sound.split("\\.")[0]+".mp3";
                        }

//                        int resID=getResources().getIdentifier(list.get(pos).getName().toLowerCase().split("\\.")[0], "raw", context.getPackageName());
                        player = new MediaPlayer(); //MediaPlayer.create(AlertToneActivity.this,resID);
                        String name = "alert_sound/" + sound.toLowerCase();
                        AssetFileDescriptor descriptor = context.getAssets().openFd(name);
                        player.reset();
                        player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                        descriptor.close();

                        player.prepare();
                        player.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    h.img.setVisibility(View.VISIBLE);

                    if (isMsg) {
                        AppManager.getInstance().setMsgTone(list.get(pos).getId());
                    } else {
                        AppManager.getInstance().setGroupTone(list.get(pos).getId());
                    }

                    notifyDataSetChanged();

                }
            });


            return view;
        }
    }


}
