package com.vmemos.inc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import Util.AppManager;

public class AccountActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_account);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        Button privacy = (Button) findViewById(R.id.btnPrivacy);
        Button info = (Button) findViewById(R.id.btnInfo);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        privacy.setTypeface(AppManager.getInstance().getRegularTypeface());
        info.setTypeface(AppManager.getInstance().getRegularTypeface());

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.btnPrivacy:
                Intent intent = new Intent(AccountActivity.this, AboutActivity.class);
                intent.putExtra("FLAG", false);
                startActivity(intent);
                break;
            case R.id.btnInfo:
                Intent intent2 = new Intent(AccountActivity.this, OtherInfoActivity.class);
                startActivity(intent2);
                break;
        }
    }

}
