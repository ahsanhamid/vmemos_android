package com.vmemos.inc;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.isseiaoki.simplecropview.CropImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import Util.AppManager;
import Util.Constants;

public class CropActivity extends Activity {

    Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_crop);


        final CropImageView mCropView = (CropImageView) findViewById(R.id.cropImageView);
        Button btnDone = (Button) findViewById(R.id.btnDone);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);

        context = CropActivity.this;

        Bundle bundle = getIntent().getExtras();
        final String path  = bundle.getString("Path");

        Picasso.with(CropActivity.this)
                .load(Uri.fromFile(new File(path)))
                .fit()
                .centerInside()
                .into(mCropView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.i("CropActivity", "Picasso Success Loading Thumbnail - " + path);
                    }

                    @Override
                    public void onError() {
                        Log.i("CropActivity", "Picasso Error Loading Thumbnail Small - " + path);
                    }
                });

//        Bitmap bitmap = BitmapFactory.decodeFile(path);

//        bitmap = ExifUtil.rotateBitmap(path,bitmap);


//        try {
//            ExifInterface ei = new ExifInterface(path);
//            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotateImage(bitmap, 90);
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotateImage(bitmap, 180);
//                    break;
//                // etc.
//            }
//        }catch (Exception e) {
//            e.printStackTrace();
//        }


//        mCropView.setImageBitmap(bitmap);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bitmap bitmap = mCropView.getCroppedBitmap();
                byte[] byteArray = null;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                byteArray = stream.toByteArray();

                try {
                    String tempImagePath = getFilesDir().getAbsolutePath() + "/" + Constants.FILE_PATH +"/";
                    File dir = new File(tempImagePath);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    File tempImageFile = new File(tempImagePath, "tempImage.jpg");
                    if (!tempImageFile.exists()) {
                        tempImageFile.createNewFile();
                    }
                    FileOutputStream stream2 = new FileOutputStream(tempImageFile);
                    stream2.write(byteArray);

                    AppManager.getInstance().setIsImageChanged(true);
                    AppManager.getInstance().setImgFile(tempImageFile);
                    AppManager.getInstance().setCropImage(bitmap);
                    AppManager.getInstance().setImg(bitmap);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }
}
