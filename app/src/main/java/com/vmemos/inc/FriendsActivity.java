package com.vmemos.inc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vmemos.inc.R;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Model.CreateThreadRes;
import Model.DefaultRes;
import Model.FriendRequest;
import Model.FrndRequestRes;
import Model.GetVocals;
import Model.GroupInfo;
import Model.Item;
import Model.SectionItem;
import Model.VocalsModel;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import database.MyDbHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FriendsActivity extends Activity {

    EditText txtSearch;
    List<FriendRequest> arrFrnds;
    List<FriendRequest> arrFrndRequests;
    List<FriendRequest> searchArrFrnds;
    List<FriendRequest> searchArrFrndRequests;
    ListView listView;
    SwipeRefreshLayout swipeContainer;
    List<Item> mergeList;
    MyDbHelper dbHelper;

    Activity context;

    Boolean isFromVocals = false;
    Boolean isFromGroup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_friends);

        AppManager.getInstance().setFont(this);
        dbHelper = new MyDbHelper(this);

        arrFrnds = new ArrayList<>();
        arrFrndRequests = new ArrayList<>();
        searchArrFrndRequests = new ArrayList<>();
        searchArrFrnds = new ArrayList<>();
        mergeList = new ArrayList<>();

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        ImageButton btnSetting = (ImageButton) findViewById(R.id.setting);
        RelativeLayout relBack = (RelativeLayout) findViewById(R.id.relBack);
        txtSearch = (EditText) findViewById(R.id.etSearch);
        listView = (ListView) findViewById(R.id.listView);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.white);
        swipeContainer.setProgressBackgroundColorSchemeResource(R.color.green);

        final ImageView btnClear = (ImageView) findViewById(R.id.imgClear);
        btnClear.setVisibility(View.INVISIBLE);


        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        txtSearch.setTypeface(AppManager.getInstance().getRegularTypeface());


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.FROM_VOCAL)) {
                isFromVocals = bundle.getBoolean(Constants.FROM_VOCAL);
                relBack.setVisibility(View.VISIBLE);
                btnSetting.setVisibility(View.INVISIBLE);
                context = FriendsActivity.this;
            }
            else if (bundle.containsKey(Constants.FROM_GROUP)) {
                isFromGroup = bundle.getBoolean(Constants.FROM_GROUP);
                relBack.setVisibility(View.VISIBLE);
                btnSetting.setVisibility(View.INVISIBLE);
                context = FriendsActivity.this;
            } else {
                relBack.setVisibility(View.GONE);
                btnSetting.setVisibility(View.VISIBLE);
                context = FriendsActivity.this;
            }
        }
        else {
            context = getParent();
        }

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getAllFrndsAndRequestAPI();

            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (txtSearch.getText().toString().equalsIgnoreCase("")) {

                    btnClear.setVisibility(View.INVISIBLE);

                    if (mergeList != null) {
                        mergeList.clear();
                        if (arrFrndRequests.size() > 0) {
                            mergeList.add(new SectionItem("FriendRequests"));
                            mergeList.addAll(arrFrndRequests);
                        }
                        if (arrFrnds.size() > 0) {
                            mergeList.add(new SectionItem("Friends"));
                            mergeList.addAll(arrFrnds);
                        }
                        CustomAdapter adapter = new CustomAdapter(context, R.layout.custom_friend_cell, mergeList);
                        listView.setAdapter(adapter);
                    }
                } else {

                    btnClear.setVisibility(View.VISIBLE);

                    searchArrFrnds.clear();
                    searchArrFrndRequests.clear();

                    String txt = txtSearch.getText().toString().toLowerCase();
                    for (int i = 0; i < arrFrnds.size(); i++) {
                        FriendRequest model = arrFrnds.get(i);
                        if (model.getFrndUsername().toLowerCase().contains(txt)) {
                            searchArrFrnds.add(model);
                        }
                    }

                    for (int i = 0; i < arrFrndRequests.size(); i++) {
                        FriendRequest model = arrFrndRequests.get(i);
                        if (model.getFrndUsername().toLowerCase().contains(txt)) {
                            searchArrFrndRequests.add(model);
                        }
                    }

                    mergeList.clear();
                    if (searchArrFrndRequests.size() > 0) {
                        mergeList.add(new SectionItem("FriendRequests"));
                        mergeList.addAll(searchArrFrndRequests);
                    }
                    if (searchArrFrnds.size() > 0) {
                        mergeList.add(new SectionItem("Friends"));
                        mergeList.addAll(searchArrFrnds);
                    }

                    CustomAdapter adapter = new CustomAdapter(context, R.layout.custom_friend_cell, mergeList);
                    listView.setAdapter(adapter);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        arrFrnds.clear();
        arrFrndRequests.clear();

        AppManager.getInstance().setCropImage(null);

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)) {
            userId = pref.getString(Constants.kUserID, "");
        }

        FrndRequestRes requestRes = dbHelper.getFrndsData(userId, Constants.kgetFriendRequest);
        if (requestRes != null){
            arrFrnds = requestRes.getFriends();
            arrFrndRequests = requestRes.getFriendRequests();

            mergeList.clear();
            if (arrFrndRequests.size() > 0) {
                mergeList.add(new SectionItem("FriendRequests"));
                mergeList.addAll(arrFrndRequests);
            }
            if (arrFrnds.size() > 0) {
                mergeList.add(new SectionItem("Friends"));
                mergeList.addAll(arrFrnds);
            }

            CustomAdapter adapter = new CustomAdapter(context, R.layout.custom_friend_cell, mergeList);
            listView.setAdapter(adapter);

        }

        getAllFrndsAndRequestAPI();

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.setting:
                startActivity(new Intent(FriendsActivity.this, SettingActivity.class));
                break;
            case R.id.btnAdd:
                Intent intent = new Intent(context, SearchFriendActivity.class);
                startActivity(intent);
//                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                parentActivity.startChildActivity("SearchFriendActivity", intent);
                break;
            case R.id.imgClear:
                txtSearch.setText("");
                break;
        }
    }

    private void getAllFrndsAndRequestAPI() {


        Handler mainHandler = new Handler(FriendsActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

//                final ProgressHUD progressHUD = ProgressHUD.show(FriendsActivity.this, "", true, true, null);
                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                final String finalUserId = userId;
                AppManager.getInstance().restAPI.getAllFrndsRequest(userId, new Callback<FrndRequestRes>() {
                    @Override
                    public void success(FrndRequestRes frndRequestRes, Response response) {
//                        progressHUD.dismiss();

                        if (frndRequestRes.getErrorCode() == 0) {

                            arrFrnds.clear();
                            arrFrndRequests.clear();

                            for (int i = 0; i < frndRequestRes.getFriends().size(); i++) {
                                FriendRequest requestRes = frndRequestRes.getFriends().get(i);
                                if (finalUserId.equalsIgnoreCase(requestRes.getUserId1())) {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId1());
                                    friend.setName(requestRes.getName());
                                    friend.setUsername(requestRes.getUsername());
                                    friend.setStatus(requestRes.getStatus());
                                    friend.setImage(requestRes.getImage());

                                    friend.setUserId2(requestRes.getUserId2());
                                    friend.setFrndName(requestRes.getFrndName());
                                    friend.setFrndUsername(requestRes.getFrndUsername());
                                    friend.setFrndStatus(requestRes.getFrndStatus());
                                    friend.setFrndImage(requestRes.getFrndImage());
                                    friend.setIsFrnd(true);

                                    if (friend.getFrndImage() != null && !friend.getFrndImage().equalsIgnoreCase("")) {
                                        String path = Functions.getImage(context,"thumb_" + friend.getFrndImage());
                                        File file = new File(path);
                                        if (!file.exists()){
                                            String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
                                        }
                                        String path2 = Functions.getImage(context,friend.getFrndImage());
                                        File file2 = new File(path2);
                                        if (!file2.exists()){
                                            String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());
                                        }
//                                        String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
//                                        String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/" + friend.getFrndImage();
//                                        new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
//                                        new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());
                                    }
                                    arrFrnds.add(friend);

                                } else {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId2());
                                    friend.setName(requestRes.getFrndName());
                                    friend.setUsername(requestRes.getFrndUsername());
                                    friend.setStatus(requestRes.getFrndStatus());
                                    friend.setImage(requestRes.getFrndImage());

                                    friend.setUserId2(requestRes.getUserId1());
                                    friend.setFrndName(requestRes.getName());
                                    friend.setFrndUsername(requestRes.getUsername());
                                    friend.setFrndStatus(requestRes.getStatus());
                                    friend.setFrndImage(requestRes.getImage());
                                    friend.setIsFrnd(true);

                                    if (friend.getFrndImage() != null && !friend.getFrndImage().equalsIgnoreCase("")) {
                                        String path = Functions.getImage(context,"thumb_" + friend.getFrndImage());
                                        File file = new File(path);
                                        if (!file.exists()){
                                            String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
                                        }
                                        String path2 = Functions.getImage(context,friend.getFrndImage());
                                        File file2 = new File(path2);
                                        if (!file2.exists()){
                                            String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());
                                        }

                                    }
//                                    String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
//                                    String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() +"/"+ friend.getFrndImage();
//                                    new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
//                                    new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());

                                    arrFrnds.add(friend);
                                }

                            }

                            for (int i = 0; i < frndRequestRes.getFriendRequests().size(); i++) {
                                FriendRequest requestRes = frndRequestRes.getFriendRequests().get(i);
                                if (finalUserId.equalsIgnoreCase(requestRes.getUserId1())) {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId1());
                                    friend.setName(requestRes.getName());
                                    friend.setUsername(requestRes.getUsername());
                                    friend.setStatus(requestRes.getStatus());
                                    friend.setImage(requestRes.getImage());

                                    friend.setUserId2(requestRes.getUserId2());
                                    friend.setFrndName(requestRes.getFrndName());
                                    friend.setFrndUsername(requestRes.getFrndUsername());
                                    friend.setFrndStatus(requestRes.getFrndStatus());
                                    friend.setFrndImage(requestRes.getFrndImage());
                                    friend.setIsFrnd(false);

                                    if (friend.getFrndImage() != null && !friend.getFrndImage().equalsIgnoreCase("")) {
                                        String path = Functions.getImage(context,"thumb_" + friend.getFrndImage());
                                        File file = new File(path);
                                        if (!file.exists()){
                                            String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
                                        }
                                        String path2 = Functions.getImage(context,friend.getFrndImage());
                                        File file2 = new File(path2);
                                        if (!file2.exists()){
                                            String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());
                                        }

                                    }

//                                    String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
//                                    String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() +"/"+ friend.getFrndImage();
//                                    new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
//                                    new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());

                                    arrFrndRequests.add(friend);

                                } else {
                                    FriendRequest friend = new FriendRequest();
                                    friend.setRequestId(requestRes.getRequestId());

                                    friend.setUserId1(requestRes.getUserId2());
                                    friend.setName(requestRes.getFrndName());
                                    friend.setUsername(requestRes.getFrndUsername());
                                    friend.setStatus(requestRes.getFrndStatus());
                                    friend.setImage(requestRes.getFrndImage());

                                    friend.setUserId2(requestRes.getUserId1());
                                    friend.setFrndName(requestRes.getName());
                                    friend.setFrndUsername(requestRes.getUsername());
                                    friend.setFrndStatus(requestRes.getStatus());
                                    friend.setFrndImage(requestRes.getImage());
                                    friend.setIsFrnd(false);

                                    if (friend.getFrndImage() != null && !friend.getFrndImage().equalsIgnoreCase("")) {
                                        String path = Functions.getImage(context,"thumb_" + friend.getFrndImage());
                                        File file = new File(path);
                                        if (!file.exists()){
                                            String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
                                        }
                                        String path2 = Functions.getImage(context,friend.getFrndImage());
                                        File file2 = new File(path2);
                                        if (!file2.exists()){
                                            String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/" + friend.getFrndImage();
                                            new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());
                                        }

                                    }

//                                    String url = Constants.IMG_BASE_ADDRESS + friend.getUserId2() + "/thumb_" + friend.getFrndImage();
//                                    String url2 = Constants.IMG_BASE_ADDRESS + friend.getUserId2() +"/"+ friend.getFrndImage();
//                                    new ThumbnailDownloadAndSave().execute(url, "thumb_" + friend.getFrndImage());
//                                    new ThumbnailDownloadAndSave().execute(url2, friend.getFrndImage());

                                    arrFrndRequests.add(friend);
                                }

                            }

                            Collections.sort(arrFrnds, new CustomSortComparator());
                            Collections.sort(arrFrndRequests, new CustomSortComparator());

                            FrndRequestRes requestRes = new FrndRequestRes();
                            requestRes.setFriends(arrFrnds);
                            requestRes.setFriendRequests(arrFrndRequests);

                            Gson gson = new Gson();
                            String value = gson.toJson(requestRes);

//                            Gson gson = new Gson();
//                            String value = gson.toJson(arrFrnds);

                            VocalsModel model = new VocalsModel();
                            model.setUserId(finalUserId);
                            model.setUrl(Constants.kgetFriendRequest);
                            model.setResponce(value);

                            dbHelper.addNewResponceList(model);

                            mergeList.clear();
                            if (arrFrndRequests.size() > 0) {
                                mergeList.add(new SectionItem("FriendRequests"));
                                mergeList.addAll(arrFrndRequests);
                            }
                            if (arrFrnds.size() > 0) {
                                mergeList.add(new SectionItem("Friends"));
                                mergeList.addAll(arrFrnds);
                            }

                            CustomAdapter adapter = new CustomAdapter(context, R.layout.custom_friend_cell, mergeList);
                            listView.setAdapter(adapter);

                        }

                        swipeContainer.setRefreshing(false);

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        swipeContainer.setRefreshing(false);
//                        progressHUD.dismiss();
//                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    private class ThumbnailDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String imgName = arg0[1];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                if (inputStream != null) {
                    byte[] byteImage = Functions.convertToByte(inputStream);

                    Functions.saveImageToExternalStorage(context,byteImage, imgName);
                }
            } catch (Exception io) {
                io.printStackTrace();
            }

            return null;
        }


    }

    private class CustomSortComparator implements Comparator<FriendRequest> {
        @Override
        public int compare(FriendRequest obj1, FriendRequest obj2) {
            return obj1.getFrndUsername().compareTo(obj2.getFrndUsername());
        }
    }

    class CustomAdapter extends ArrayAdapter<Item> {

        Context context;
        List<Item> list;
        private LayoutInflater vi;

        public CustomAdapter(Context context, int resource, List<Item> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

            vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            View v = view;

            final Item i = list.get(position);
            if (i != null) {
                if (i.isSection()) {
                    SectionItem si = (SectionItem) i;
                    v = vi.inflate(R.layout.list_item_section, null);

                    final TextView sectionView = (TextView) v.findViewById(R.id.list_item_section_text);
                    sectionView.setTypeface(AppManager.getInstance().getHeadingTypeface());
                    sectionView.setTextColor(Color.BLACK);
                    sectionView.setBackgroundColor(getResources().getColor(R.color.tab_bg));
                    sectionView.setTextSize(17);
                    sectionView.setText(si.getTitle());
                } else {
                    final FriendRequest model = (FriendRequest) i;
                    v = vi.inflate(R.layout.custom_friend_cell, null);

                    TextView userName = (TextView) v.findViewById(R.id.etUserName);
                    TextView name = (TextView) v.findViewById(R.id.etName);
                    TextView status = (TextView) v.findViewById(R.id.etStatus);
                    CircleImageView imgFriend = (CircleImageView) v.findViewById(R.id.imgFriend);
                    final ImageView tick = (ImageView) v.findViewById(R.id.tick);
                    Button btnAccept = (Button) v.findViewById(R.id.btnAccept);
                    Button btnReject = (Button) v.findViewById(R.id.btnReject);
                    RelativeLayout relative = (RelativeLayout) v.findViewById(R.id.relative);

                    if (model.isFrnd()) {
                        btnAccept.setVisibility(View.GONE);
                        btnReject.setText("Remove");
                    } else {
                        btnAccept.setVisibility(View.VISIBLE);
                        btnReject.setText("Reject");
                    }

                    userName.setTypeface(AppManager.getInstance().getRegularTypeface());
                    name.setTypeface(AppManager.getInstance().getRegularTypeface());
                    status.setTypeface(AppManager.getInstance().getRegularTypeface());
                    btnReject.setTypeface(AppManager.getInstance().getRegularTypeface());
                    btnAccept.setTypeface(AppManager.getInstance().getRegularTypeface());
                    tick.setVisibility(View.GONE);

                    userName.setText(model.getFrndUsername());
                    name.setText(Functions.decodeFromNonLossyAscii(model.getFrndName()));
                    status.setText(Functions.decodeFromNonLossyAscii(model.getFrndStatus()));


                    btnAccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String userId = "";
                            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                            if (pref.contains(Constants.kUserID)) {
                                userId = pref.getString(Constants.kUserID, "");
                            }
                            acceptFrndReq(userId, model.getRequestId());

                        }
                    });

                    btnReject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String userId = "";
                            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                            if (pref.contains(Constants.kUserID)) {
                                userId = pref.getString(Constants.kUserID, "");
                            }

                            if (model.isFrnd()) {
                                removeFrnd(userId, model.getUserId2());
                            } else {
                                rejectFrndReq(userId, model.getRequestId());
                            }
                        }
                    });

//                    String url = Constants.IMG_BASE_ADDRESS + model.getUserId2() + "/thumb_" + model.getFrndImage();
////                    String url = Constants.IMG_BASE_ADDRESS+"17/thumb_1453289373_photo.jpeg";
//                    Picasso.with(context).load(url).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgFriend);

                    String url = Functions.getImage(context,"thumb_" + model.getFrndImage());
                    Picasso.with(context).load(new File(url)).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgFriend);

                    relative.setOnClickListener(new View.OnClickListener() {
                        int pos = position;

                        @Override
                        public void onClick(View v) {

                            Item i = list.get(pos);
                            FriendRequest model = (FriendRequest) i;

                            if (model.isFrnd() && isFromVocals) {
                                GetVocals vocals = new GetVocals();
                                vocals.setReceiverId(model.getUserId2());
                                vocals.setName(model.getFrndUsername());
                                vocals.setImage(model.getFrndImage());
                                vocals.setThreadId("");
                                vocals.setIsGroup("0");

                                if (alreadyExist(vocals)) {

                                    Gson gson = new Gson();
                                    String value = gson.toJson(vocals);

                                    Intent intent = new Intent(context, FriendChatActivity.class);
                                    intent.putExtra("VOCAL", value);
                                    startActivity(intent);
//                                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                                    parentActivity.startChildActivity("FriendChatActivity", intent);

                                } else {

                                    createThread(vocals);

//                                    Gson gson = new Gson();
//                                    String value = gson.toJson(vocals);
//
//                                    Intent intent = new Intent(getParent(), FriendChatActivity.class);
//                                    intent.putExtra("VOCAL", value);
//                                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                                    parentActivity.startChildActivity("FriendChatActivity", intent);

                                }
                            } else if (model.isFrnd() && isFromGroup) {

                                GroupInfo info = new GroupInfo();
                                info.setUserId(model.getUserId2());
                                info.setUsername(model.getFrndUsername());
                                info.setMemberName(model.getFrndName());
                                info.setUserImage(model.getFrndImage());
                                info.setIsAdmin(model.getIsAdmin());

                                if (alreadyExistInGroup(info)) {
                                    tick.setVisibility(View.VISIBLE);
                                } else {
                                    tick.setVisibility(View.VISIBLE);

                                    for (int j = 0; j < AppManager.getInstance().getRemovedParticipants().size(); j++) {
                                        if (AppManager.getInstance().getRemovedParticipants().get(j).getUserId().equalsIgnoreCase(info.getUserId())) {
                                            AppManager.getInstance().getRemovedParticipants().remove(j);
                                            break;
                                        }
                                    }
                                    AppManager.getInstance().getNewlyAddedParticipants().add(info);
                                    AppManager.getInstance().getGroupParticipants().add(info);
                                }

                            }

                        }
                    });

                    imgFriend.setOnClickListener(new View.OnClickListener() {
                        int pos = position;
                        @Override
                        public void onClick(View v) {

                            Item i = list.get(pos);
                            FriendRequest model = (FriendRequest) i;

                            String url = Functions.getImage(context,model.getFrndImage()); //Constants.IMG_BASE_ADDRESS + model.getUserId2() + "/" + model.getFrndImage();
                            Intent intent = new Intent(FriendsActivity.this,ImageActivity.class);
                            intent.putExtra(Constants.FROM_FRND,true);
                            intent.putExtra(Constants.URL,url);
                            startActivity(intent);
                        }
                    });


                }
            }
            return v;
        }

        private boolean alreadyExistInGroup(GroupInfo model) {

            boolean exist = false;
            List<GroupInfo> list = AppManager.getInstance().getGroupParticipants();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getUserId().equalsIgnoreCase(model.getUserId())) {
                    exist = true;
                    break;
                } else {
                    exist = false;
                }
            }
            return exist;
        }

        private boolean alreadyExist(GetVocals vocals) {
            boolean flag = false;

            String userId = "";
            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.kUserID)) {
                userId = pref.getString(Constants.kUserID, "");
            }

            List<GetVocals> list = dbHelper.getVocalsData(userId, Constants.kGetAllVocals);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getReceiverId().equalsIgnoreCase(vocals.getReceiverId()) && list.get(i).getIsGroup().equalsIgnoreCase("0") && !list.get(i).getTotalVocalCount().equalsIgnoreCase("0")) {
                    vocals.setThreadId(list.get(i).getThreadId());
                    flag = true;
                    break;
                } else {
                    flag = false;
                }
            }
            return flag;
        }
    }

    private void createThread(final GetVocals vocals) {

        Handler mainHandler = new Handler(FriendsActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                AppManager.getInstance().restAPI.createThread(userId, vocals.getReceiverId(), new Callback<CreateThreadRes>() {
                    @Override
                    public void success(CreateThreadRes defaultRes, Response response) {
                        progressHUD.dismiss();
                        if (defaultRes.getErrorCode() == 0) {


                            vocals.setThreadId(defaultRes.getThreadId());

                            Gson gson = new Gson();
                            String value = gson.toJson(vocals);

                            Intent intent = new Intent(context, FriendChatActivity.class);
                            intent.putExtra("VOCAL", value);
                            startActivity(intent);
//                            TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                            parentActivity.startChildActivity("FriendChatActivity", intent);

//                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    private void rejectFrndReq(final String userId, final String requestId) {

        Handler mainHandler = new Handler(FriendsActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                AppManager.getInstance().restAPI.rejectFrndRequest(userId, requestId, new Callback<DefaultRes>() {
                    @Override
                    public void success(DefaultRes defaultRes, Response response) {
                        progressHUD.dismiss();
                        if (defaultRes.getErrorCode() == 0) {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    private void acceptFrndReq(final String userId, final String requestId) {

        Handler mainHandler = new Handler(FriendsActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                AppManager.getInstance().restAPI.acceptFrndRequest(userId, requestId, new Callback<DefaultRes>() {
                    @Override
                    public void success(DefaultRes defaultRes, Response response) {
                        progressHUD.dismiss();
                        if (defaultRes.getErrorCode() == 0) {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    private void removeFrnd(final String userId, final String frndId) {

        Handler mainHandler = new Handler(FriendsActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                AppManager.getInstance().restAPI.removeFriend(userId, frndId, new Callback<DefaultRes>() {
                    @Override
                    public void success(DefaultRes defaultRes, Response response) {
                        progressHUD.dismiss();
                        if (defaultRes.getErrorCode() == 0) {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), defaultRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

}
