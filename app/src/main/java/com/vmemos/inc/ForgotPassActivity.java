package com.vmemos.inc;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import Model.UpdateTokenRes;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ForgotPassActivity extends Activity {

    EditText txtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forgot_pass);


        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.btnBack);
        txtEmail = (EditText) findViewById(R.id.et_email);


        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());





    }

    public void onClick(View v){
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.btn_proceed:
                forgotPassAPI(txtEmail.getText().toString());
                break;
        }
    }

    private void forgotPassAPI(final String email) {

        Handler mainHandler = new Handler(ForgotPassActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(ForgotPassActivity.this, "", true, true, null,false);

                AppManager.getInstance().restAPI.forgotPassword(email, new Callback<UpdateTokenRes>() {
                    @Override
                    public void success(UpdateTokenRes signInRes, Response response) {

                        progressHUD.dismiss();
                        if (signInRes.getErrorCode() == 1) {

                            Toast.makeText(getApplicationContext(), signInRes.getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);


    }

}
