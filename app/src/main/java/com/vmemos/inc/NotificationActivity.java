package com.vmemos.inc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.vmemos.inc.R;

import java.util.List;

import Model.NotificationSound;
import Model.PushSetting;
import Model.UpdateSettingRes;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NotificationActivity extends Activity {

    ToggleButton btnShowMsgNotify,btnShowGroupNotify;
    Button btnMsgTone,btnGroupTone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_notification);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        TextView tvMsg = (TextView) findViewById(R.id.tvMsg);
        TextView tvShowMsg = (TextView) findViewById(R.id.tvShowMsg);
        TextView tvGroup = (TextView) findViewById(R.id.tvGroup);
        TextView tvShowGroup = (TextView) findViewById(R.id.tvShowGroup);
        TextView tvSound1 = (TextView) findViewById(R.id.tvSound1);
        TextView tvSound2 = (TextView) findViewById(R.id.tvSound2);
        btnShowMsgNotify = (ToggleButton) findViewById(R.id.showMsgNotify);
        btnShowGroupNotify = (ToggleButton) findViewById(R.id.showGroupNotify);
        btnMsgTone = (Button) findViewById(R.id.btnMsgTone);
        btnGroupTone = (Button) findViewById(R.id.btnGroupTone);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvMsg.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvShowMsg.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvGroup.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvShowGroup.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvSound1.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvSound2.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnShowMsgNotify.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnShowGroupNotify.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnMsgTone.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnGroupTone.setTypeface(AppManager.getInstance().getRegularTypeface());

        setSettings();

        btnShowMsgNotify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                updateSettings();

            }
        });

        btnShowGroupNotify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                updateSettings();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        List<NotificationSound> sounds = AppManager.getInstance().getNotificationSounds();
        for (int i=0;i<sounds.size();i++){
            NotificationSound sound = sounds.get(i);
            if (sound.getId().equalsIgnoreCase(AppManager.getInstance().getMsgTone())){
                String name[] = sound.getName().split("\\.");
                btnMsgTone.setText(name[0]);
            }

            if (sound.getId().equalsIgnoreCase(AppManager.getInstance().getGroupTone())){
                String name[] = sound.getName().split("\\.");
                btnGroupTone.setText(name[0]);
            }
        }

        updateSettings();

    }

    private void setSettings() {

        PushSetting setting = AppManager.getInstance().getPushSetting();
        if (setting.getMsgPush().equalsIgnoreCase("1")) {
            btnShowMsgNotify.setChecked(true);
        } else {
            btnShowMsgNotify.setChecked(false);
        }

        if (setting.getGroupMsgPush().equalsIgnoreCase("1")) {
            btnShowGroupNotify.setChecked(true);
        } else {
            btnShowGroupNotify.setChecked(false);
        }

        AppManager.getInstance().setMsgTone(setting.getMsgPushSound());
        AppManager.getInstance().setGroupTone(setting.getGroupMsgPushSound());

    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.relBack:
                finish();
                break;
            case R.id.btnMsgTone:
                Intent intent = new Intent(NotificationActivity.this, AlertToneActivity.class);
                intent.putExtra(Constants.FOR_MSG,true);
                startActivity(intent);
                break;
            case R.id.btnGroupTone:
                Intent intent2 = new Intent(NotificationActivity.this, AlertToneActivity.class);
                intent2.putExtra(Constants.FOR_MSG,false);
                startActivity(intent2);
                break;
        }
    }

    private void updateSettings(){

        Handler handler = new Handler(NotificationActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                String msgPush;
                String groupPush;

                if (btnShowMsgNotify.isChecked()){
                    msgPush = "1";
                }
                else {
                    msgPush = "0";
                }

                if (btnShowGroupNotify.isChecked()){
                    groupPush = "1";
                }
                else {
                    groupPush = "0";
                }

                AppManager.getInstance().restAPI.updateSettings(userId, msgPush, groupPush, AppManager.getInstance().getMsgTone(), AppManager.getInstance().getGroupTone(), new Callback<UpdateSettingRes>() {
                    @Override
                    public void success(UpdateSettingRes updateSettingRes, Response response) {

                        if (updateSettingRes.getErrorCode() == 1){
                            AppManager.getInstance().setPushSetting(Functions.getPushSettings(updateSettingRes.getUpdateSettings().get(0)));

                            setSettings();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

            }
        };
        handler.post(runnable);
    }

}
