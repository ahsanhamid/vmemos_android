package com.vmemos.inc;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import Model.RegisterRes;
import Model.User;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class ProfileActivity extends Activity {

    EditText txtName;
//    EmojiconEditText txtStatus;
    EditText txtStatus;
    TextView tvCount;
    User user;
    CircleImageView imgProfile;
    File tempImageFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile);

        AppManager.getInstance().setFont(this);

        AppManager.getInstance().setCropImage(null);
        AppManager.getInstance().setImg(null);

        TextView heading = (TextView) findViewById(R.id.heading);
        TextView text = (TextView) findViewById(R.id.text);
        Button back = (Button) findViewById(R.id.back);
        Button logout = (Button) findViewById(R.id.btnLogout);
        Button done = (Button) findViewById(R.id.btn_done);
        txtName = (EditText) findViewById(R.id.et_name);
        txtStatus = (EditText) findViewById(R.id.et_status);
        tvCount = (TextView) findViewById(R.id.tvCount);
        imgProfile = (CircleImageView) findViewById(R.id.imgProfile);
//        imgProfile.setBorderRadius(imgProfile.getWidth()/2);
//        imgProfile.requestLayout();

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvCount.setTypeface(AppManager.getInstance().getRegularTypeface());
        text.setTypeface(AppManager.getInstance().getRegularTypeface());
        logout.setTypeface(AppManager.getInstance().getRegularTypeface());
        done.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtStatus.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtName.setTypeface(AppManager.getInstance().getRegularTypeface());

//        final View rootView = findViewById(R.id.root_view);
//        final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
//
//
//        // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
//        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);
//
//        //Will automatically set size according to the soft keyboard size
//        popup.setSizeForSoftKeyboard();
//
//        //If the emoji popup is dismissed, change emojiButton to smiley icon
//        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
//
//            @Override
//            public void onDismiss() {
//                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
//            }
//        });
//
//        //If the text keyboard closes, also dismiss the emoji popup
//        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {
//
//            @Override
//            public void onKeyboardOpen(int keyBoardHeight) {
//
//            }
//
//            @Override
//            public void onKeyboardClose() {
//                if(popup.isShowing())
//                    popup.dismiss();
//            }
//        });
//
//        //On emoji clicked, add it to edittext
//        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {
//
//            @Override
//            public void onEmojiconClicked(Emojicon emojicon) {
//                if (txtStatus == null || emojicon == null) {
//                    return;
//                }
//
//                int start = txtStatus.getSelectionStart();
//                int end = txtStatus.getSelectionEnd();
//                if (start < 0) {
//                    txtStatus.append(emojicon.getEmoji());
//                } else {
//                    txtStatus.getText().replace(Math.min(start, end),
//                            Math.max(start, end), emojicon.getEmoji(), 0,
//                            emojicon.getEmoji().length());
//                }
//            }
//        });
//
//        //On backspace clicked, emulate the KEYCODE_DEL key event
//        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {
//
//            @Override
//            public void onEmojiconBackspaceClicked(View v) {
//                KeyEvent event = new KeyEvent(
//                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
//                txtStatus.dispatchKeyEvent(event);
//            }
//        });
//
//        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
//        emojiButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                //If popup is not showing => emoji keyboard is not visible, we need to show it
//                if(!popup.isShowing()){
//
//                    //If keyboard is visible, simply show the emoji popup
////                    if(popup.isKeyBoardOpen()){
////                        popup.showAtBottom();
////                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
////                    }
////
////                    //else, open the text keyboard first and immediately after that show the emoji popup
////                    else{
//                        txtStatus.setFocusableInTouchMode(true);
//                        txtStatus.requestFocus();
//                        popup.showAtBottomPending();
//                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        inputMethodManager.showSoftInput(txtStatus, InputMethodManager.SHOW_IMPLICIT);
////                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
////                    }
//                }
//
//                //If popup is showing, simply dismiss it to show the undelying text keyboard
//                else{
//                    popup.dismiss();
//                }
//            }
//        });



        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserArray)){
            Gson gson = new Gson();
            user = gson.fromJson(pref.getString(Constants.kUserArray,""),User.class);
        }

        if (user!=null) {
            txtName.setText(Functions.decodeFromNonLossyAscii(user.getVocalName()));
            String status = Functions.decodeFromNonLossyAscii(user.getStatusMessage());
            txtStatus.setText(status);
            tvCount.setText("" + (25 - status.length()));
            if (user.getImage() != null) {
                AppManager.getInstance().setImgFile(new File(Functions.getImage(ProfileActivity.this,"thumb_" + user.getImage())));

                Bitmap bitmap = BitmapFactory.decodeFile(Functions.getImage(ProfileActivity.this,"thumb_" + user.getImage()));
                AppManager.getInstance().setImg(bitmap);

            }
//            Picasso.with(ProfileActivity.this).load(new File(path)).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgProfile);

            String url = Constants.IMG_BASE_ADDRESS + user.getUserId() + "/thumb_" + user.getImage();
            new ThumbnailDownloadAndSave().execute(url, "thumb_" + user.getImage(),Constants.TRUE);

            String url2 = Constants.IMG_BASE_ADDRESS + user.getUserId() + "/" + user.getImage();
            new ThumbnailDownloadAndSave().execute(url2,user.getImage(),Constants.FALSE);
        }

        txtStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (txtStatus.getText().toString().equalsIgnoreCase("")){
                    tvCount.setText("25");
                }
                else {
                    tvCount.setText("" + (25 - txtStatus.getText().toString().length()));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
        iconToBeChanged.setImageResource(drawableResourceId);
    }

    @Override
    protected void onResume() {
        super.onResume();

        tempImageFile = AppManager.getInstance().getImgFile();
        Bitmap bitmap = AppManager.getInstance().getImg();
        if (bitmap != null) {
            imgProfile.setImageBitmap(bitmap);
//            String path = tempImageFile.getPath();
//            imgProfile.setImageResource(R.drawable.friends_profile_pic_placeholder);
//            Picasso.with(ProfileActivity.this).load(new File(path)).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgProfile);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.btnLogout:

                clearAppData();

                AppManager.getInstance().getNotificationSounds().clear();
                AppManager.getInstance().getGroupParticipants().clear();
                AppManager.getInstance().setPushSetting(null);
                AppManager.getInstance().setUser(null);

                Intent intent = new Intent(ProfileActivity.this, SignInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.btn_done:
                updateProfile();
                break;
            case R.id.imgProfile:
                String path = Functions.getImage(ProfileActivity.this,user.getImage());
                Intent in = new Intent(ProfileActivity.this,ImageActivity.class);
                in.putExtra(Constants.FROM_PROFILE,true);
                in.putExtra(Constants.URL,path);
                startActivity(in);
                break;
        }
    }

    private void updateProfile() {

        TypedFile typedImage = null;
        if (tempImageFile != null) {
            typedImage = new TypedFile("image/jpeg", tempImageFile);
        }

        Handler mainHandler = new Handler(ProfileActivity.this.getMainLooper());
        final TypedFile finalTypedImage = typedImage;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(ProfileActivity.this, "", true, true, null,false);

                String status = Functions.encodeToNonLossyAscii(txtStatus.getText().toString());
                String name = Functions.encodeToNonLossyAscii(txtName.getText().toString());

                AppManager.getInstance().restAPI.updateProfile(user.getUserId(), name, status,
                        finalTypedImage, new Callback<RegisterRes>() {
                            @Override
                            public void success(RegisterRes registerRes, Response response) {

                                progressHUD.dismiss();

                                if (registerRes.getErrorCode() == 0) {

                                    AppManager.getInstance().setUser(registerRes.getUser().get(0));

                                    User user = AppManager.getInstance().getUser();
                                    txtName.setText(Functions.decodeFromNonLossyAscii(user.getVocalName()));
                                    txtStatus.setText(Functions.decodeFromNonLossyAscii(user.getStatusMessage()));
                                    String url = Constants.IMG_BASE_ADDRESS + user.getUserId()+ "/thumb_" + user.getImage(); // + "/thumb_"
                                    Picasso.with(ProfileActivity.this).load(url).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgProfile);

                                    new ThumbnailDownloadAndSave().execute(url, "thumb_" + user.getImage(), Constants.TRUE); //"thumb_" +

                                    SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString(Constants.kUserID, user.getUserId());
                                    editor.putString(Constants.kUserStatus, user.getUserStatus());

                                    Gson gson = new Gson();
                                    String userArr = gson.toJson(user);
                                    editor.putString(Constants.kUserArray, userArr);
                                    String soundArr = gson.toJson(AppManager.getInstance().getNotificationSounds());
                                    editor.putString(Constants.kSounds, soundArr);

                                    editor.apply();

                                    Toast.makeText(getApplicationContext(), registerRes.getMsg(), Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(getApplicationContext(), registerRes.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressHUD.dismiss();
                                Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        };
        mainHandler.post(runnable);

    }

    private void clearAppData(){
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        pref.edit().clear().apply();
    }

    private class ThumbnailDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                final String imgName = arg0[1];
                String profile = arg0[2];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                if (inputStream != null) {
                    byte[] byteImage = Functions.convertToByte(inputStream);

                    Functions.saveImageToExternalStorage(ProfileActivity.this,byteImage, imgName);

                    if (profile.equalsIgnoreCase(Constants.TRUE)){

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String url2 = Functions.getImage(ProfileActivity.this,"thumb_" + imgName);
//                                Picasso.with(ProfileActivity.this).load(new File(url2)).placeholder(R.drawable.friends_profile_pic_placeholder).into(imgProfile);
                            }
                        });

                    }
                }
            } catch (Exception io) {
                io.printStackTrace();
            }

            return null;
        }


    }

}
