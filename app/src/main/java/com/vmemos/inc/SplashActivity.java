package com.vmemos.inc;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vmemos.inc.R;

import java.util.List;

import Model.NotificationSound;
import Model.RegisterRes;
import Model.User;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        AppManager.getInstance().setFont(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserName) && pref.getString(Constants.kUserStatus,"").equalsIgnoreCase(Constants.kUserActive)){

                    Gson gson = new Gson();
                    User user = gson.fromJson(pref.getString(Constants.kUserArray,""),User.class);
                    List<NotificationSound> sounds = gson.fromJson(pref.getString(Constants.kSounds,""), new TypeToken<List<NotificationSound>>(){}.getType());

                    AppManager.getInstance().setUser(user);
                    AppManager.getInstance().setNotificationSounds(sounds);
                    AppManager.getInstance().setPushSetting(Functions.getPushSettings(user));

                    signInAPI(pref.getString(Constants.kUserName, ""), pref.getString(Constants.kPassword, ""));


                    startActivity(new Intent(SplashActivity.this, TabActivity.class));
                }
                else {
                    startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                }
                finish();
            }
        }, 1000);

    }

    private void signInAPI(final String userName, final String pass){

        Handler mainHandler = new Handler(SplashActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

//                final ProgressHUD progressHUD = ProgressHUD.show(SplashActivity.this, "", true, true, null);

                AppManager.getInstance().restAPI.loginUser(userName, pass, "", "", new Callback<RegisterRes>() {
                    @Override
                    public void success(RegisterRes signInRes, Response response) {

//                        progressHUD.dismiss();
                        if (signInRes.getErrorCode()==0){

                            AppManager.getInstance().setUser(signInRes.getUser().get(0));
                            AppManager.getInstance().setNotificationSounds(signInRes.getNotificationSounds());
                            AppManager.getInstance().setPushSetting(Functions.getPushSettings(signInRes.getUser().get(0)));

                            User user = AppManager.getInstance().getUser();

                            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(Constants.kUserID,user.getUserId());
                            editor.putString(Constants.kUserName,userName);
                            editor.putString(Constants.kPassword,pass);
                            editor.putString(Constants.kUserStatus,user.getUserStatus());

                            Gson gson = new Gson();
                            String userArr = gson.toJson(user);
                            editor.putString(Constants.kUserArray, userArr);
                            String soundArr = gson.toJson(AppManager.getInstance().getNotificationSounds());
                            editor.putString(Constants.kSounds, soundArr);
                            editor.apply();

                        }
//                        else {
//                            Toast.makeText(getApplicationContext(), signInRes.getMsg(), Toast.LENGTH_SHORT).show();
//                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
//                        progressHUD.dismiss();
//                        Toast.makeText(getApplicationContext(),error.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }


}
