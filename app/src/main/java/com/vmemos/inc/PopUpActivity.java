package com.vmemos.inc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import Model.GetVocals;

public class PopUpActivity extends Activity {

    String threadId, frndId, isGroup, frndImage, groupImage, adminId, message;
    boolean isActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_ACTION_MODE_OVERLAY);
        setContentView(R.layout.activity_pop_up);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Bundle b = bundle.getBundle("BUNDLE");
//            isActive = bundle.getBoolean("IS_ACTIVE");
            if (b != null) {
                threadId = b.getString("thread_id", "");
                frndId = b.getString("friend_id", "");
                isGroup = b.getString("is_group", "");
                frndImage = b.getString("friend_img", "");
                groupImage = b.getString("group_img", "");
                adminId = b.getString("admin_id", "");
                message = b.getString("message", "");
            }
        }

        isActive = true;

//        if (isActive) {
//            GetVocals obj = new GetVocals();
//            obj.setThreadId(threadId);
//            obj.setIsGroup(isGroup);
//            obj.setImage(frndImage);
//            obj.setGroupImage(groupImage);
//            obj.setAdminId(adminId);
//            obj.setReceiverId(frndId);
//
//            AppManager.getInstance().setChatPuchData(obj);
//
////            FriendChatActivity activity = new FriendChatActivity();
////            activity.reloadView(obj);
//
//            Intent registrationComplete = new Intent("push");
//            LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
//
//
//            PopUpActivity.this.finish();
//            return;
//
//        }

        RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel);

//        String name = Functions.getTopActivity(this);


        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_popup_push, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

//        popupWindow.setOutsideTouchable(true);

        TextView msg = (TextView) popupView.findViewById(R.id.msg);
        LinearLayout linear = (LinearLayout) popupView.findViewById(R.id.linear);
        msg.setText(message);

//        popupWindow.showAtLocation(rel, Gravity.CENTER, width, 0);

        new Handler().postDelayed(new Runnable() {

            public void run() {
                popupWindow.showAtLocation(popupView, Gravity.TOP, 0, 0);
            }

        }, 100L);

        new Handler().postDelayed(new Runnable() {

            public void run() {
                if (isActive) {
                    popupWindow.dismiss();
                    PopUpActivity.this.finish();
                }
            }

        }, 1600L);

        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                GetVocals obj = new GetVocals();
                obj.setThreadId(threadId);
                obj.setIsGroup(isGroup);
                obj.setImage(frndImage);
                obj.setGroupImage(groupImage);
                obj.setAdminId(adminId);
                obj.setReceiverId(frndId);
                Gson gson = new Gson();
                String value = gson.toJson(obj);

                Intent intent = new Intent(PopUpActivity.this, FriendChatActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("VOCAL", value);
                intent.putExtra("FROM_PUSH", true);
                startActivity(intent);

                isActive = false;

                PopUpActivity.this.finish();

            }
        });


//        AlertDialog.Builder builder = new AlertDialog.Builder(this, android.R.style.Theme_Holo_Dialog);
//        builder.setMessage("VMemos\n" + message).setCancelable(true).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        })
//                .setPositiveButton("View", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Toast.makeText(getApplicationContext(), "VIEW", Toast.LENGTH_LONG).show();
//
//
//                                GetVocals obj = new GetVocals();
//                                obj.setThreadId(threadId);
//                                obj.setIsGroup(isGroup);
//                                obj.setImage(frndImage);
//                                obj.setGroupImage(groupImage);
//                                obj.setAdminId(adminId);
//                                obj.setReceiverId(frndId);
//                                Gson gson = new Gson();
//                                String value = gson.toJson(obj);
//
//
//                                Intent intent = new Intent(PopUpActivity.this, FriendChatActivity.class);
//                                intent.putExtra("VOCAL", value);
//                                intent.putExtra("FROM_PUSH", true);
//                                startActivity(intent);
//
//
//                                PopUpActivity.this.finish();
//
//                            }
//                        }
//
//                );
//
//
//        final AlertDialog dialog = builder.create();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
////
//        wmlp.gravity = Gravity.TOP;
//
//
//        dialog.show();
//
//        dialog.getWindow().setLayout(width, 300);
//
//        builder.setOnCancelListener(new DialogInterface.OnCancelListener()
//
//                                    {
//                                        @Override
//                                        public void onCancel(DialogInterface dialog) {
//                                            finish();
//                                        }
//                                    }
//
//        );


//        rel.setOnClickListener(new View.OnClickListener()
//
//                               {
//                                   @Override
//                                   public void onClick(View v) {
//                                       popupWindow.dismiss();
//                                       finish();
//                                   }
//                               }
//
//        );

    }

}
