package com.vmemos.inc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vmemos.inc.R;

import java.util.ArrayList;
import java.util.List;

import Model.CaptchaArray;
import Model.CaptchaRes;
import Model.DefaultRes;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ImageCaptchaActivity extends Activity {

    List<String> selectedIDs;
    List<CaptchaArray> captchaArray;
    CustomGridAdapter adapter;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_image_captcha);

        AppManager.getInstance().setFont(this);

        selectedIDs= new ArrayList<>();

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.btnBack);
        Button proceed = (Button) findViewById(R.id.btn_proceed);
        gridView = (GridView) findViewById(R.id.gridView);

        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        proceed.setTypeface(AppManager.getInstance().getRegularTypeface());
        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());

        captchaArray = new ArrayList<>();
        refreshCaptchaAPI();

        adapter = new CustomGridAdapter(this,captchaArray);
        gridView.setAdapter(adapter);

    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_proceed:
                verifyCaptchaAPI();
                break;
            case R.id.relBack:
                finish();
                break;
            case R.id.btn_refresh:
                selectedIDs.clear();
                refreshCaptchaAPI();
                break;
        }
    }

    private void refreshCaptchaAPI(){

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)){
            userId = pref.getString(Constants.kUserID,"");
        }

        Handler mainHandler = new Handler(ImageCaptchaActivity.this.getMainLooper());
        final String finalUserId = userId;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(ImageCaptchaActivity.this, "", true, true, null,true);

                AppManager.getInstance().restAPI.refreshCaptcha(finalUserId, new Callback<CaptchaRes>() {
                    @Override
                    public void success(CaptchaRes captchaRes, Response response) {

                        progressHUD.dismiss();

                        if (captchaRes.getErrorCode() == 0){

                            captchaArray.clear();
                            captchaArray = captchaRes.getCaptchaArray();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter = new CustomGridAdapter(ImageCaptchaActivity.this, captchaArray);
                                    gridView.setAdapter(adapter);
                                }
                            });


                        }
                        else {
                            Toast.makeText(getApplicationContext(),captchaRes.getMsg(),Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        mainHandler.post(runnable);

    }

    private void verifyCaptchaAPI(){

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)){
            userId = pref.getString(Constants.kUserID,"");
        }

        Handler mainHandler = new Handler(ImageCaptchaActivity.this.getMainLooper());
        final String finalUserId = userId;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(ImageCaptchaActivity.this, "", true, true, null,true);

                AppManager.getInstance().restAPI.verifyCaptcha(finalUserId,selectedIDs, new Callback<DefaultRes>() {
                    @Override
                    public void success(DefaultRes verifyCaptchaRes, Response response) {

                        progressHUD.dismiss();

                        if (verifyCaptchaRes.getErrorCode() == 0) {

                            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(Constants.kUserStatus, Constants.kUserActive);
                            editor.apply();

                            startActivity(new Intent(ImageCaptchaActivity.this, TabActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));//.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                        } else {
                            Toast.makeText(getApplicationContext(), verifyCaptchaRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        mainHandler.post(runnable);

    }


    private class CustomGridAdapter extends BaseAdapter {
        private Activity context;

        private final List<CaptchaArray> grid_values;

        public CustomGridAdapter(Activity context, List<CaptchaArray> grid_values) {
            this.context = context;
            this.grid_values = grid_values;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return grid_values.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        private class ViewHolder {
            ImageView img;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {

            ViewHolder holder = null;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_grid_item, parent, false);

                holder = new ViewHolder();
                holder.img = (ImageView) view.findViewById(R.id.img);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            try {
                int resourceId = context.getResources().getIdentifier("c" + grid_values.get(position).getId(), "drawable", context.getPackageName());
                holder.img.setImageResource(resourceId);
            }catch (OutOfMemoryError e){
                e.printStackTrace();
            }
//            Resources resources = getResources();
//            final int resourceId = resources.getIdentifier(grid_values.get(position).getImage(), "drawable", getPackageName());
//            holder.img.setImageDrawable(getResources().getDrawable(resourceId));
////            holder.img.setImageResource(resourceId);

            final ViewHolder finalHolder = holder;
            holder.img.setOnClickListener(new View.OnClickListener() {
                int pos = position;
                @Override
                public void onClick(View v) {


                    if (!ifExist(grid_values.get(pos).getId())){
                        selectedIDs.add(grid_values.get(pos).getId());
                        finalHolder.img.setBackgroundColor(getResources().getColor(R.color.green));
                    }
                    else {
                        finalHolder.img.setBackgroundColor(getResources().getColor(R.color.white));
                    }

                }
            });

            return view;
        }

        private boolean ifExist(String id){
            for (int i=0;i<selectedIDs.size();i++){
                if (selectedIDs.get(i).equalsIgnoreCase(id)){
                    selectedIDs.remove(i);
                    return true;
                }
            }
            return false;
        }
    }
}
