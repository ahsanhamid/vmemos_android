package com.vmemos.inc;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.vmemos.inc.R;

import Util.AppManager;
import Util.Constants;
import Util.NetworkUtil;

public class SettingActivity extends Activity {

    public static TextView tvStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_setting);

        AppManager.getInstance().setFont(this);

        TextView heading = (TextView) findViewById(R.id.heading);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        Button back = (Button) findViewById(R.id.btnBack);
        Button about = (Button) findViewById(R.id.btnAbout);
        Button btnTellFrnd = (Button) findViewById(R.id.btnTellFrnd);
        Button btnWEb = (Button) findViewById(R.id.btnWeb);
        Button btnProfile = (Button) findViewById(R.id.btnProfile);
        Button btnAccount = (Button) findViewById(R.id.btnAccount);
        Button btnNotify = (Button) findViewById(R.id.btnNotification);
        Button btnStatus = (Button) findViewById(R.id.btnStatus);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvStatus.setTypeface(AppManager.getInstance().getRegularTypeface());
        about.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnTellFrnd.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnWEb.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnProfile.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnAccount.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnNotify.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnStatus.setTypeface(AppManager.getInstance().getRegularTypeface());

    }

    @Override
    protected void onResume() {
        super.onResume();

        String status = NetworkUtil.getConnectivityStatusString(SettingActivity.this);
        if (tvStatus != null) {
            if (status.equalsIgnoreCase(Constants.CONNECTED)) {
                tvStatus.setText(Constants.CONNECTED);
                tvStatus.setTextColor(getResources().getColor(R.color.green));
            } else {
                tvStatus.setText(Constants.NotCONNECTED);
                tvStatus.setTextColor(getResources().getColor(R.color.red));
            }
        }

    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.relBack:
                finish();
                break;
            case R.id.btnAbout:
                Intent intent = new Intent(SettingActivity.this,AboutActivity.class);
                intent.putExtra("FLAG",true);
                startActivity(intent);
                break;
            case R.id.btnTellFrnd:
//                popUpView();
                try {
                    Intent intnt = new Intent(Intent.ACTION_SEND);
                    intnt.setType("text/plain");//"text/plain"
                    intnt.putExtra(Intent.EXTRA_TEXT, "Check out VMemos for your smartphone. Download it today from http://vmemos.com/download_app");

                    startActivity(Intent.createChooser(intnt, "invite friend via..."));
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.btnWeb:
                startActivity(new Intent(SettingActivity.this,WebActivity.class));
                break;
            case R.id.btnProfile:
                startActivity(new Intent(SettingActivity.this,ProfileActivity.class));
                break;
            case R.id.btnAccount:
                startActivity(new Intent(SettingActivity.this,AccountActivity.class));
                break;
            case R.id.btnNotification:
                startActivity(new Intent(SettingActivity.this,NotificationActivity.class));
                break;
        }
    }


//    private void popUpView(){
//        LayoutInflater layoutInflater
//                = (LayoutInflater)getBaseContext()
//                .getSystemService(LAYOUT_INFLATER_SERVICE);
//        View popupView = layoutInflater.inflate(R.layout.custom_popup, null);
//        final PopupWindow popupWindow = new PopupWindow(
//                popupView,
//                AbsListView.LayoutParams.WRAP_CONTENT,
//                AbsListView.LayoutParams.WRAP_CONTENT);
//
//        Button btnDismiss = (Button)popupView.findViewById(R.id.btnDismiss);
//        Button btnMsg = (Button)popupView.findViewById(R.id.btnMsg);
//        Button btnEmail = (Button)popupView.findViewById(R.id.btnEmail);
//        Button btnTwtr = (Button)popupView.findViewById(R.id.btnTwtr);
//        btnDismiss.setOnClickListener(new Button.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                popupWindow.dismiss();
//            }
//        });
//        btnMsg.setOnClickListener(new Button.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                popupWindow.dismiss();
//
//                try {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
//                    intent.putExtra("sms_body", "");
//                    startActivity(intent);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//            }
//        });
//        btnEmail.setOnClickListener(new Button.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                popupWindow.dismiss();
//                try {
//                    Intent intent = new Intent(Intent.ACTION_SEND);
//                    intent.setType("text/html");
//                    intent.putExtra(Intent.EXTRA_TEXT, "");
//
//                    startActivity(Intent.createChooser(intent, "Send Email"));
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//            }
//        });
//        btnTwtr.setOnClickListener(new Button.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                popupWindow.dismiss();
//
//                try
//                {
//                    // Check if the Twitter app is installed on the phone.
//                    getPackageManager().getPackageInfo("com.twitter.android", 0);
//                    Intent intent = new Intent(Intent.ACTION_SEND);
//                    intent.setClassName("com.twitter.android", "com.twitter.android.composer.ComposerActivity");
//                    intent.setType("text/plain");
//                    intent.putExtra(Intent.EXTRA_TEXT, "Your text");
//                    startActivity(intent);
//
//                }
//                catch (Exception e)
//                {
//                    Toast.makeText(getApplicationContext(), "Twitter is not installed on this device", Toast.LENGTH_LONG).show();
//
//                }
//            }
//        });
//        popupWindow.showAtLocation(SettingActivity.this.findViewById(R.id.linear), Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
//    }
}