package com.vmemos.inc;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import ThirdParty.ProgressHUD;
import Util.AppManager;

public class AboutActivity extends Activity {

    boolean loadingFinished = true;
    boolean redirect = false;
    ProgressHUD progressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_about);

        AppManager.getInstance().setFont(this);

        WebView webView = (WebView) findViewById(R.id.webView);
        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.btnBack);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        back.setTypeface(AppManager.getInstance().getRegularTypeface());

        progressHUD = ProgressHUD.show(AboutActivity.this, "", true, true, null,false);

        Boolean flag=false;
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            flag = bundle.getBoolean("FLAG");
        }

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);


        webView.setWebViewClient(new HelloWebViewClient());
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        if (flag) {
            heading.setText("About");
            webView.loadUrl("http://vmemos.com/about");
        }else {
            heading.setText("Privacy");
            webView.loadUrl("http://vmemos.com/privacy_policy"); //http://vmemos.com/terms
        }
    }

    public void onClick(View v){
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
        }
    }

    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

//
            if (!loadingFinished) {
                redirect = true;
            }
//
            loadingFinished = false;
//            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                progressHUD.dismiss();
            } else {
                redirect = false;
            }
            //what you want to do when the page finished loading, eg. give some message, show progress bar, etc
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            loadingFinished = false;


            //what you want to do when the page starts loading, eg. give some message
        }

    }

}
