package com.vmemos.inc;

import android.content.Intent;
import android.os.Bundle;

public class TabGroup2Activity extends TabGroupActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startChildActivity("FriendsActivity", new Intent(this, FriendsActivity.class));
    }
}
