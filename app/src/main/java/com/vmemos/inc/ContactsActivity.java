package com.vmemos.inc;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Model.ContactModel;
import ThirdParty.quickscroll.QuickScroll;
import ThirdParty.quickscroll.Scrollable;
import Util.AppManager;
import Util.PermissionUtil;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsActivity extends AppCompatActivity
        implements ActivityCompat.OnRequestPermissionsResultCallback {

    EditText txtSearch;
    ListView listView;
    ArrayList<ContactModel> contactList;
    ArrayList<ContactModel> searchContactList;

    Cursor phones, email;
    ContentResolver resolver;
    ImageView btnClear;

    QuickScroll quickscroll;

    Activity activity;

    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS};

    View mLayout;

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 13;

    private static final int PERMISSION_REQUEST_CONTACTS = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_contacts);


        AppManager.getInstance().setFont(this);

        contactList = new ArrayList<>();
        searchContactList = new ArrayList<>();

        activity = getParent(); // getParent();


        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        txtSearch = (EditText) findViewById(R.id.etSearch);
        listView = (ListView) findViewById(R.id.listView);
        btnClear = (ImageView) findViewById(R.id.imgClear);
        mLayout = (RelativeLayout) findViewById(R.id.mLayout);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relative);
        LinearLayout linear = (LinearLayout) findViewById(R.id.linear);
        btnClear.setVisibility(View.INVISIBLE);

        quickscroll = (QuickScroll) findViewById(R.id.quickscroll);
        quickscroll.bringToFront();


        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        txtSearch.setTypeface(AppManager.getInstance().getRegularTypeface());

        relativeLayout.addView(createAlphabetTrack());

        getContacts();

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (txtSearch.getText().toString().equalsIgnoreCase("")) {

                    btnClear.setVisibility(View.INVISIBLE);
                    CustomAdapter adapter = new CustomAdapter(getParent(), R.layout.custom_contact_cell, contactList);
                    listView.setAdapter(adapter);

                    quickscroll.init(QuickScroll.TYPE_POPUP, listView, adapter, QuickScroll.STYLE_NONE);
                    quickscroll.setFixedSize(2);
                    quickscroll.setPopupColor(QuickScroll.GREEN_LIGHT, QuickScroll.BLUE_LIGHT_SEMITRANSPARENT, 1, Color.WHITE, 1);

                } else {

                    btnClear.setVisibility(View.VISIBLE);
                    searchContactList.clear();

                    String txt = txtSearch.getText().toString().toLowerCase();
                    for (int i = 0; i < contactList.size(); i++) {
                        ContactModel model = contactList.get(i);
                        if (model.getName().toLowerCase().contains(txt)) {
                            searchContactList.add(model);
                        }
                    }

                    CustomAdapter adapter = new CustomAdapter(getParent(), R.layout.custom_contact_cell, searchContactList);
                    listView.setAdapter(adapter);

                    quickscroll.init(QuickScroll.TYPE_POPUP, listView, adapter, QuickScroll.STYLE_NONE);
                    quickscroll.setFixedSize(2);
                    quickscroll.setPopupColor(QuickScroll.GREEN_LIGHT, QuickScroll.BLUE_LIGHT_SEMITRANSPARENT, 1, Color.WHITE, 1);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private ViewGroup createAlphabetTrack() {

        final LinearLayout layout = new LinearLayout(getParent());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) (30 * getResources().getDisplayMetrics().density), LinearLayout.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        params.addRule(RelativeLayout.BELOW, R.id.tv_title);
        params.setMargins(0, (int) (20 * getResources().getDisplayMetrics().density), 0, (int) (20 * getResources().getDisplayMetrics().density));
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);

        final LinearLayout.LayoutParams textparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textparams.weight = 1;
        final int height = getResources().getDisplayMetrics().heightPixels;
        int iterate = 0;
        if (height >= 1024) {
            iterate = 1;
            layout.setWeightSum(26);
        } else {
            iterate = 2;
            layout.setWeightSum(13);
        }
        for (char character = 'a'; character <= 'z'; character += iterate) {
            final TextView textview = new TextView(getParent());
            textview.setLayoutParams(textparams);
            textview.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            textview.setText(Character.toString(character).toUpperCase());
            textview.setTextSize(12.0f);
            textview.setTextColor(getResources().getColor(R.color.gray));
            layout.addView(textview);
        }

        return layout;
    }

    private void getContacts() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

                resolver = getParent().getContentResolver();
                phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

                new LoadContact().execute();

            }

        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            resolver = getParent().getContentResolver();
            phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

            new LoadContact().execute();
        }

    }

    private void requestContactsPermissions() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // Display a SnackBar with a button to request the missing permission.
            Snackbar.make(mLayout, "READ_CONTACTS access is required to display the CONTACTS preview.",
                    Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    ActivityCompat.requestPermissions(activity,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACTS);
                }
            }).show();

        } else {
            Snackbar.make(mLayout,
                    "Permission is not available. Requesting camera permission.",
                    Snackbar.LENGTH_SHORT).show();
            // Request the permission. The result will be received in onRequestPermissionResult().
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS},
                    PERMISSION_REQUEST_CONTACTS);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (PermissionUtil.verifyPermissions(grantResults)) {
                // All required permissions have been granted, display contacts fragment.
                Snackbar.make(mLayout, R.string.permision_available_contacts,
                        Snackbar.LENGTH_SHORT)
                        .show();


            } else {
                Log.i("", "Contacts permissions were NOT granted.");
                Snackbar.make(mLayout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT)
                        .show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

//        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // Permission is granted
//
//                resolver = this.getContentResolver();
//                phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
//
//                new LoadContact().execute();
//            } else {
//                Toast.makeText(getApplicationContext(), "Until you grant the permission, we cannot display the contacts", Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtSearch.setText("");
    }

    class LoadContact extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone

            if (phones != null) {
//                Log.e("count", "" + phones.getCount());
//                if (phones.getCount() == 0) {
//                    Toast.makeText(getParent(), "No contacts in your contact list.", Toast.LENGTH_LONG).show();
//                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        while (phones.moveToNext()) {
                            Bitmap bit_thumb = null;
//                    String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                    String EmailAddr = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA2));
//                    String image_thumb = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
//                    try {
//                        if (image_thumb != null) {
//                            bit_thumb = MediaStore.Images.Media.getBitmap(resolver, Uri.parse(image_thumb));
//                        } else {
//                            Log.e("No Image Thumb", "--------------");
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }

                            ContactModel model = new ContactModel();
//                    selectUser.setThumb(bit_thumb);
                            model.setName(name);
                            model.setPhone(phoneNumber);
//                    selectUser.setEmail(id);
//                    selectUser.setCheckedBox(false);
                            contactList.add(model);
                        }
                    }
                });
            } else {
//                Log.e("Cursor close 1", "----------------");
            }
            //phones.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ArrayList<ContactModel> list = contactList;
            CustomAdapter adapter = new CustomAdapter(getParent(), R.layout.custom_contact_cell, list);
            listView.setAdapter(adapter);

            quickscroll.init(QuickScroll.TYPE_POPUP, listView, adapter, QuickScroll.STYLE_NONE);
            quickscroll.setFixedSize(2);
            quickscroll.setPopupColor(QuickScroll.GREEN_LIGHT, QuickScroll.BLUE_LIGHT_SEMITRANSPARENT, 1, Color.WHITE, 1);
//            quickscroll.init(QuickScroll.TYPE_INDICATOR_WITH_HANDLE, listView, adapter, QuickScroll.STYLE_HOLO);
//            quickscroll.setFixedSize(1);
//            quickscroll.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 48);

//            listView.setFastScrollEnabled(true);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting:
                startActivity(new Intent(ContactsActivity.this, SettingActivity.class));
                break;
            case R.id.imgClear:
                txtSearch.setText("");
                break;
        }
    }

    class CustomAdapter extends ArrayAdapter<ContactModel> implements Scrollable {

        Context context;
        List<ContactModel> list;

        public CustomAdapter(Context context, int resource, List<ContactModel> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public String getIndicatorForPosition(int childposition, int groupposition) {
            return list.get(childposition).getIndicator();
        }

        @Override
        public int getScrollPosition(int childposition, int groupposition) {
            return childposition;
        }

        private class ViewHolder {
            TextView name;
            CircleImageView imgProfile;
            ImageView btnAdd;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder = null;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_contact_cell, parent, false);

                holder = new ViewHolder();
                holder.name = (TextView) view.findViewById(R.id.etName);
                holder.imgProfile = (CircleImageView) view.findViewById(R.id.imgProfile);
                holder.btnAdd = (ImageView) view.findViewById(R.id.btnAdd);


                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.imgProfile.setVisibility(View.GONE);

            holder.name.setTypeface(AppManager.getInstance().getRegularTypeface());
            holder.name.setText(list.get(position).getName());

            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                int pos = position;

                @Override
                public void onClick(View v) {

                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("sms:" + list.get(pos).getPhone()));
                    sendIntent.putExtra("sms_body", "Check out VMemos for your smartphone. Download it today from http://vmemos.com/download_app");
                    startActivity(sendIntent);

                }
            });

            return view;
        }
    }


}
