package com.vmemos.inc;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vmemos.inc.R;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import Model.GetVocals;
import Model.GetVocalsRes;
import Model.VocalsModel;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import database.MyDbHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class VocalsActivity extends Activity {

    EditText txtSearch;
    ListView listView;
    List<GetVocals> vocalsList;
    List<GetVocals> searchVocalsList;
    SwipeRefreshLayout swipeContainer;
    CustomAdapter adapter;

    MyDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_vocals);

        AppManager.getInstance().setFont(this);
        dbHelper = new MyDbHelper(this);

        vocalsList = new ArrayList<>();
        searchVocalsList = new ArrayList<>();

        TextView heading = (TextView) findViewById(R.id.heading);
        Button back = (Button) findViewById(R.id.back);
        Button newChat = (Button) findViewById(R.id.newChat);
        Button newGroup = (Button) findViewById(R.id.newGroup);
        txtSearch = (EditText) findViewById(R.id.etSearch);
        listView = (ListView) findViewById(R.id.listView);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.white);
        swipeContainer.setProgressBackgroundColorSchemeResource(R.color.green);

        final ImageView btnClear = (ImageView) findViewById(R.id.imgClear);
        btnClear.setVisibility(View.INVISIBLE);

        back.setTypeface(AppManager.getInstance().getRegularTypeface());
        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        newChat.setTypeface(AppManager.getInstance().getRegularTypeface());
        newGroup.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtSearch.setTypeface(AppManager.getInstance().getRegularTypeface());

        adapter = new CustomAdapter(getParent(), R.layout.custom_vocal_cell, vocalsList);
        listView.setAdapter(adapter);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getAllVocalsAPI();

            }
        });


        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (txtSearch.getText().toString().equalsIgnoreCase("")) {

                    btnClear.setVisibility(View.INVISIBLE);

                    CustomAdapter adapter = new CustomAdapter(getParent(), R.layout.custom_vocal_cell, vocalsList);
                    listView.setAdapter(adapter);

                } else {

                    btnClear.setVisibility(View.VISIBLE);

                    searchVocalsList.clear();

                    String txt = txtSearch.getText().toString().toLowerCase();
                    for (int i = 0; i < vocalsList.size(); i++) {
                        GetVocals model = vocalsList.get(i);

                        if (model.getIsGroup().equalsIgnoreCase("0")) {
                            if (model.getName() != null && model.getName().toLowerCase().contains(txt)) {
                                searchVocalsList.add(model);
                            }
                        }else {
                            if (model.getGroupName() != null && model.getGroupName().toLowerCase().contains(txt)) {
                                searchVocalsList.add(model);
                            }
                        }
                    }

                    CustomAdapter adapter = new CustomAdapter(getParent(), R.layout.custom_vocal_cell, searchVocalsList);
                    listView.setAdapter(adapter);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private BroadcastReceiver onNotice= new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // intent can contain anydata
            Log.d("VocalsActivity", "onReceive called");

            reloadView();

        }
    };

    private void reloadView() {
        getAllVocalsAPI();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)) {
            userId = pref.getString(Constants.kUserID, "");
        }

        AppManager.getInstance().setIsVocalActive(true);

        IntentFilter iff= new IntentFilter("vocal");
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, iff);

        List<GetVocals> list = dbHelper.getVocalsData(userId, Constants.kGetAllVocals);
        if (list.size()>0) {
            vocalsList.clear();

            for (int i=0;i<list.size();i++){
                if(list.get(i).getIsGroup().equalsIgnoreCase("0")) {
                    if (list.get(i).getTotalVocalCount() != null && !list.get(i).getTotalVocalCount().equalsIgnoreCase("0")) {
                        vocalsList.add(list.get(i));
                    }
                }
                else {
                    vocalsList.add(list.get(i));
                }
            }
//          vocalsList.addAll(list);
            adapter.notifyDataSetChanged();
        }

        getAllVocalsAPI();

    }

    @Override
    protected void onPause() {
        super.onPause();

        AppManager.getInstance().setIsVocalActive(false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onNotice);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting:
                startActivity(new Intent(VocalsActivity.this, SettingActivity.class));
                break;
            case R.id.relNewGroup:
                Intent intent = new Intent(getParent(), NewGroupActivity.class);
                intent.putExtra("BOOL",true);
                startActivity(intent);
                break;
            case R.id.relNewChat:
                Intent intent2 = new Intent(getParent(), FriendsActivity.class);
                intent2.putExtra(Constants.FROM_VOCAL,true);
                startActivity(intent2);
//                TabGroupActivity parentActivity2 = (TabGroupActivity) getParent();
//                parentActivity2.startChildActivity("NewGroupActivity", intent2);
                break;
            case R.id.imgClear:
                txtSearch.setText("");
                break;
        }
    }

    private void getAllVocalsAPI() {

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)) {
            userId = pref.getString(Constants.kUserID, "");
        }

        Handler mainHandler = new Handler(VocalsActivity.this.getMainLooper());
        final String finalUserId = userId;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                AppManager.getInstance().restAPI.getAllVocals(finalUserId, new Callback<GetVocalsRes>() {
                    @Override
                    public void success(GetVocalsRes getVocalsRes, Response response) {

                        if (getVocalsRes.getErrorCode() == 0) {

                            List<GetVocals> getVocalsList = getVocalsRes.getVocals();
                            List<GetVocals> delArr = new ArrayList<GetVocals>();

                            for (int i = 0; i < vocalsList.size(); i++) {

                                GetVocals vocals = vocalsList.get(i);
                                if (!isExist(vocals.getThreadId(), getVocalsList)) {
                                    delArr.add(vocals);
                                }
                            }

                            for (int i = 0; i < getVocalsList.size(); i++) {

                                GetVocals vocals = getVocalsList.get(i);

                                if (vocals.getIsGroup().equalsIgnoreCase("1")) {

                                    if (vocals.getGroupImage()!=null && !vocals.getGroupImage().equalsIgnoreCase("")) {
                                        String path = Functions.getImage(getParent(),"thumb_" + vocals.getGroupImage());
                                        File file = new File(path);
                                        if (!file.exists()){
                                            String url = Constants.GROUP_IMG_BASE_ADDRESS + vocals.getThreadId() + "/thumb_" + vocals.getGroupImage();
                                            new ThumbnailDownloadAndSave().execute(url, "thumb_" + vocals.getGroupImage());
                                        }
                                        String path2 = Functions.getImage(getParent(),vocals.getGroupImage());
                                        File file2 = new File(path2);
                                        if (!file2.exists()){
                                            String url2 = Constants.GROUP_IMG_BASE_ADDRESS + vocals.getThreadId() + "/" + vocals.getGroupImage();
                                            new ThumbnailDownloadAndSave().execute(url2, vocals.getGroupImage());
                                        }
                                    }

                                } else {
                                    if (vocals.getImage()!=null && !vocals.getImage().equalsIgnoreCase("")) {
                                        String path = Functions.getImage(getParent(),"thumb_" + vocals.getImage());
                                        File file = new File(path);
                                        if (!file.exists()){
                                            String url = Constants.IMG_BASE_ADDRESS + vocals.getReceiverId() + "/thumb_" + vocals.getImage();
                                            new ThumbnailDownloadAndSave().execute(url, "thumb_" + vocals.getImage());
                                        }
                                        String path2 = Functions.getImage(getParent(),vocals.getImage());
                                        File file2 = new File(path2);
                                        if (!file2.exists()){
                                            String url2 = Constants.IMG_BASE_ADDRESS + vocals.getReceiverId() + "/" + vocals.getImage();
                                            new ThumbnailDownloadAndSave().execute(url2, vocals.getImage());
                                        }
//                                        String url = Constants.IMG_BASE_ADDRESS + vocals.getReceiverId() + "/thumb_" + vocals.getImage();
//                                        String url2 = Constants.IMG_BASE_ADDRESS + vocals.getReceiverId() + "/" + vocals.getImage();
//                                        new ThumbnailDownloadAndSave().execute(url, "thumb_" + vocals.getImage());
//                                        new ThumbnailDownloadAndSave().execute(url2, vocals.getImage());
                                    }
                                }

                            }

                            Gson gson = new Gson();
                            String value = gson.toJson(getVocalsList);

                            VocalsModel model = new VocalsModel();
                            model.setUserId(finalUserId);
                            model.setUrl(Constants.kGetAllVocals);
                            model.setResponce(value);

                            if (dbHelper.addNewResponceList(model)) {

                                List<GetVocals> list = dbHelper.getVocalsData(finalUserId, Constants.kGetAllVocals);
                                if (list.size()>0) {
                                    vocalsList.clear();

                                    for (int i=0;i<list.size();i++){
                                        if(list.get(i).getIsGroup().equalsIgnoreCase("0")) {
                                            if (!list.get(i).getTotalVocalCount().equalsIgnoreCase("0") && list.get(i).getIsGroup().equalsIgnoreCase("0")) {
                                                vocalsList.add(list.get(i));
                                            }
                                        }else {
                                            vocalsList.add(list.get(i));
                                        }
                                    }
//                                    vocalsList.addAll(list);
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            for (int i=0;i<delArr.size();i++){

                                dbHelper.deleteThread(delArr.get(i).getThreadId());

                            }


                        }

                        swipeContainer.setRefreshing(false);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        swipeContainer.setRefreshing(false);
                    }
                });

            }
        };
        mainHandler.post(runnable);

    }

    private boolean isExist(String threadId, List<GetVocals> getVocalsList) {

        boolean flag = false;
        for (int i = 0; i < getVocalsList.size(); i++) {
            if (threadId.equalsIgnoreCase(getVocalsList.get(i).getThreadId())) {
                flag = true;
                break;
            } else {
                flag = false;
            }
        }
        return flag;
    }

    private void setListViewAdapter() {

        CustomAdapter adapter = new CustomAdapter(getParent(), R.layout.custom_vocal_cell, vocalsList);
        listView.setAdapter(adapter);

    }

    private class ThumbnailDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String imgName = arg0[1];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                if (inputStream != null) {
                    byte[] byteImage = Functions.convertToByte(inputStream);

                    Functions.saveImageToExternalStorage(getParent(),byteImage, imgName);
                }
            } catch (Exception io) {
                io.printStackTrace();
            }

            return null;
        }


    }

    class CustomAdapter extends ArrayAdapter<GetVocals> {

        Context context;
        List<GetVocals> list;

        public CustomAdapter(Context context, int resource, List<GetVocals> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        private class ViewHolder {
            TextView name;
            ImageView imgVocal;
            CircleImageView imgProfile;
            RelativeLayout relative;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder = null;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_vocal_cell, parent, false);

                holder = new ViewHolder();
                holder.name = (TextView) view.findViewById(R.id.etUserName);
                holder.relative = (RelativeLayout) view.findViewById(R.id.relative);
                holder.imgProfile = (CircleImageView) view.findViewById(R.id.imgProfile);
                holder.imgVocal = (ImageView) view.findViewById(R.id.imgVocal);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.name.setTypeface(AppManager.getInstance().getRegularTypeface());

            GetVocals vocals = list.get(position);

            if (vocals.getIsGroup().equalsIgnoreCase("1")) {
                holder.name.setText(Functions.decodeFromNonLossyAscii(vocals.getGroupName()));

                String url = Functions.getImage(getParent(),"thumb_" + vocals.getGroupImage()); //Constants.GROUP_IMG_BASE_ADDRESS+vocals.getThreadId()+"/thumb_"+vocals.getGroupImage();
                Uri uri = Uri.fromFile(new File(url));
                Picasso.with(context).load(uri).placeholder(R.drawable.friends_profile_pic_placeholder).into(holder.imgProfile);
//                Uri uri = Uri.fromFile(new File(url));
//                holder.imgProfile.setImageURI(uri);
            } else {
                holder.name.setText(Functions.decodeFromNonLossyAscii(vocals.getName()));

                String url = Functions.getImage(getParent(),"thumb_" + vocals.getImage()); //Constants.IMG_BASE_ADDRESS+vocals.getReceiverId()+"/thumb_"+vocals.getImage();
                Uri uri = Uri.fromFile(new File(url));
                Picasso.with(context).load(uri).placeholder(R.drawable.friends_profile_pic_placeholder).into(holder.imgProfile);
//                Uri uri = Uri.fromFile(new File(url));
//                holder.imgProfile.setImageURI(uri);
            }

            if (vocals.getUnreadVocalCount().equalsIgnoreCase("0")) {
                holder.imgVocal.setImageResource(R.drawable.unread_icon);
            } else {
                holder.imgVocal.setImageResource(R.drawable.read_icon);
            }


            final ViewHolder finalHolder = holder;
            holder.relative.setOnClickListener(new View.OnClickListener() {
                int pos = position;
                ViewHolder holder = finalHolder;
                @Override
                public void onClick(View v) {

                    holder.imgVocal.setImageResource(R.drawable.unread_icon);

                    GetVocals obj = list.get(pos);
                    Gson gson = new Gson();
                    String value = gson.toJson(obj);

                    Intent intent = new Intent(context, FriendChatActivity.class);
                    intent.putExtra("VOCAL",value);
                    startActivity(intent);
//                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                    parentActivity.startChildActivity("FriendChatActivity", intent);
                }
            });

            return view;
        }
    }

}
