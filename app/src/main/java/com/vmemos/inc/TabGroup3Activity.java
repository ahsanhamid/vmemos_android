package com.vmemos.inc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

public class TabGroup3Activity extends TabGroupActivity  implements ActivityCompat.OnRequestPermissionsResultCallback  {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        startActivity(new Intent(this, ContactsActivity.class));
        startChildActivity("ContactsActivity", new Intent(this, ContactsActivity.class));

    }
}
