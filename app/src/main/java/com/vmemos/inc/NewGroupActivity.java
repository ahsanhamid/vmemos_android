package com.vmemos.inc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.squarecamera.CameraActivity;
import com.desmond.squarecamera.ImageUtility;
import com.google.gson.Gson;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.squareup.picasso.Picasso;
import com.vmemos.inc.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import Model.CreateGroupRes;
import Model.FriendRequest;
import Model.GetVocals;
import Model.GroupInfo;
import Model.GroupInfoRes;
import Model.UpdateGroupRes;
import ThirdParty.ProgressHUD;
import Util.AppManager;
import Util.Constants;
import Util.Functions;
import database.MyDbHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class NewGroupActivity extends Activity implements
        ImageChooserListener {

    EditText txtGroupName;
    TextView tvCount;
    TextView tvGroupMemberCount;
    GetVocals getVocals;
    boolean isForNew;
    boolean isAdmin = false;
    CircleImageView img;
    Button btnCreate, btnAdd, btnExit;
    RelativeLayout viewAdd;
    TextView lblPart;
    List<GroupInfo> groupMembers;
    MyDbHelper dbHelper;
    ListView listView;
    File tempImageFile;
    String imgName;
    boolean isCapture;

    Activity context;
    String oldInfoJsonValue="";

    Point mSize;
    ImageChooserManager imageChooserManager;
    String filePath;
    String previousGroup;
    String adminIds = "";

    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_group);

        AppManager.getInstance().setFont(this);
        dbHelper = new MyDbHelper(this);

        groupMembers = new ArrayList<>();

        AppManager.getInstance().setCropImage(null);

        context = NewGroupActivity.this;

        TextView heading = (TextView) findViewById(R.id.heading);
        Button btnBack = (Button) findViewById(R.id.back);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnCreate = (Button) findViewById(R.id.btnCreate);
        btnExit = (Button) findViewById(R.id.btnExit);
        txtGroupName = (EditText) findViewById(R.id.etName);
        tvCount = (TextView) findViewById(R.id.tvCount);
        tvGroupMemberCount = (TextView) findViewById(R.id.groupMember);
        img = (CircleImageView) findViewById(R.id.imgProfile);
        viewAdd = (RelativeLayout) findViewById(R.id.viewAdd);
        lblPart = (TextView) findViewById(R.id.lblPart);
        listView = (ListView) findViewById(R.id.listView);

        heading.setTypeface(AppManager.getInstance().getHeadingTypeface());
        btnBack.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvCount.setTypeface(AppManager.getInstance().getRegularTypeface());
        tvGroupMemberCount.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnAdd.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnCreate.setTypeface(AppManager.getInstance().getRegularTypeface());
        btnExit.setTypeface(AppManager.getInstance().getRegularTypeface());
        txtGroupName.setTypeface(AppManager.getInstance().getRegularTypeface());
        lblPart.setTypeface(AppManager.getInstance().getRegularTypeface());

        Display display = getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("VOCAL")) {
                Gson gson = new Gson();
                getVocals = gson.fromJson(bundle.getString("VOCAL"), GetVocals.class);
            }
            if (bundle.containsKey("BOOL")) {
                isForNew = bundle.getBoolean("BOOL");
            }
        }

        AppManager.getInstance().getGroupParticipants().clear();
        AppManager.getInstance().getNewlyAddedParticipants().clear();
        AppManager.getInstance().getRemovedParticipants().clear();
        AppManager.getInstance().setImgFile(null);
        AppManager.getInstance().setImg(null);
        AppManager.getInstance().setIsImageChanged(false);
        AppManager.getInstance().setIsImageDelete(false);


        if (getVocals != null && getVocals.getThreadId() != null && !getVocals.getThreadId().equalsIgnoreCase("")) {
            btnCreate.setText("Done");
            heading.setText("Group Info");
            getGroupInfoAPI(getVocals.getThreadId(), false);
        } else {
            heading.setText("New Group");
            btnCreate.setText("Create");
        }

        txtGroupName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (txtGroupName.getText().toString().equalsIgnoreCase("")) {
                    tvCount.setText("25");
                } else {
                    tvCount.setText("" + (25 - txtGroupName.getText().toString().length()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isForNew) {
            String userId = "";
            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.kUserID)) {
                userId = pref.getString(Constants.kUserID, "");
            }

            ///// tiemr for update view /////
//            timer = new Timer();
//            timer.schedule(new MyTimerTask(), 5000, 5000);
            /////// END ///////


            if (getVocals.getUserGroupCurrentStatus() == null || !getVocals.getUserGroupCurrentStatus().equalsIgnoreCase("0")) {
                btnExit.setVisibility(View.VISIBLE);
                btnAdd.setVisibility(View.VISIBLE);
                btnCreate.setVisibility(View.VISIBLE);
                txtGroupName.setEnabled(true);
                tvCount.setVisibility(View.VISIBLE);
                img.setEnabled(true);
            } else {
                btnExit.setVisibility(View.INVISIBLE);
                btnAdd.setVisibility(View.INVISIBLE);
                btnCreate.setVisibility(View.INVISIBLE);
                txtGroupName.setEnabled(false);
                tvCount.setVisibility(View.INVISIBLE);
                img.setEnabled(false);
            }

//            btnExit.setVisibility(View.VISIBLE);

            if (checkIsAdmin(getVocals.getAdminId())) {
                isAdmin = true;
                txtGroupName.setEnabled(true);
                btnCreate.setVisibility(View.VISIBLE);
                viewAdd.setVisibility(View.VISIBLE);
                lblPart.setVisibility(View.INVISIBLE);
//                img.setEnabled(true);
                tempImageFile = AppManager.getInstance().getImgFile();
                Bitmap bitmap = AppManager.getInstance().getImg();
                if (bitmap != null) {
                    img.setImageBitmap(bitmap);

                }
                CustomAdapter adapter = new CustomAdapter(context, R.layout.custom_group_cell, AppManager.getInstance().getGroupParticipants());
                listView.setAdapter(adapter);

            } else {
                isAdmin = false;
                tempImageFile = AppManager.getInstance().getImgFile();
                Bitmap bitmap = AppManager.getInstance().getImg();
                if (bitmap != null) {
                    img.setImageBitmap(bitmap);
                }
//                txtGroupName.setEnabled(true);
//                btnCreate.setVisibility(View.VISIBLE);
                viewAdd.setVisibility(View.GONE);
                lblPart.setVisibility(View.VISIBLE);
            }
        } else {

            btnExit.setVisibility(View.GONE);

            viewAdd.setVisibility(View.VISIBLE);
            lblPart.setVisibility(View.INVISIBLE);

            Bitmap bitmap = AppManager.getInstance().getCropImage();
            if (bitmap != null) {
                img.setImageBitmap(bitmap);
            }
            tempImageFile = AppManager.getInstance().getImgFile();
            img.setEnabled(true);
            CustomAdapter adapter = new CustomAdapter(context, R.layout.custom_group_cell, AppManager.getInstance().getGroupParticipants());
            listView.setAdapter(adapter);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();

        if (timer != null) {
            timer.cancel();
            timer = null;
        }

    }

    private boolean checkIsAdmin(String adminId) {
        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)) {
            userId = pref.getString(Constants.kUserID, "");
        }

        if (adminId != null && !adminId.equalsIgnoreCase("")) {
            String[] arr = adminId.split(",");
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].equalsIgnoreCase(userId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relBack:
                finish();
                break;
            case R.id.btnAdd:
                Intent intent = new Intent(context, FriendsActivity.class);
                intent.putExtra(Constants.FROM_GROUP, true);
                startActivity(intent);
//                TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                parentActivity.startChildActivity("FriendsActivity", intent);
                break;
            case R.id.btnCreate:

                if (txtGroupName.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Enter group name!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!(AppManager.getInstance().getGroupParticipants().size() > 0)) {
                    Toast.makeText(getApplicationContext(), "Atleast select 1 participant!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (getVocals != null && getVocals.getThreadId() != null && !getVocals.getThreadId().equalsIgnoreCase("")) {


                    Handler handler = new Handler(NewGroupActivity.this.getMainLooper());
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            ProgressHUD progressHUD = null;

                            progressHUD = ProgressHUD.show(context, "", true, true, null,false);


                            final ProgressHUD finalProgressHUD = progressHUD;
                            AppManager.getInstance().restAPI.getGroupInfo(getVocals.getThreadId(), new Callback<GroupInfoRes>() {
                                @Override
                                public void success(GroupInfoRes groupInfoRes, Response response) {

                                    finalProgressHUD.dismiss();

                                    Gson gson = new Gson();
                                    String val = gson.toJson(groupInfoRes);

                                    if (oldInfoJsonValue.equalsIgnoreCase(val)) {
                                        updateGroup(getVocals.getThreadId());
                                    }
                                    else {
                                        oldInfoJsonValue  = val;
                                        if (groupInfoRes.getErrorCode() == 0) {

                                            List<GroupInfo> groupInfos = groupInfoRes.getGroupInfo();

                                            getInfoRes(groupInfos, false);

                                        }

                                        Toast.makeText(NewGroupActivity.this, "Your desired changes are not saved please try again", Toast.LENGTH_LONG).show();

                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {

                                    finalProgressHUD.dismiss();
                                    Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }


                            });

                        }
                    };
                    handler.post(runnable);


                } else {
                    createGroup();
                }

                break;
            case R.id.btnExit:

                new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT)
                        //.setTitle()
                        .setMessage("Exit \"" + txtGroupName.getText().toString() + "\" group?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                exitGroup(getVocals.getThreadId());
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();

                break;
            case R.id.imgProfile:

                if (isForNew) {
                    chooseImage();
                } else {
                    String url = Functions.getImage(context, getVocals.getGroupImage());
                    Intent in = new Intent(NewGroupActivity.this, ImageActivity.class);
                    in.putExtra(Constants.FROM_GROUP, true);
                    if (isAdmin) {
                        in.putExtra("ADMIN", true);
                    } else {
                        in.putExtra("ADMIN", true); // send true upload for image All user
                    }
                    in.putExtra(Constants.URL, url);
                    startActivity(in);
                }
                break;
        }
    }

    private void exitGroup(final String threadId) {

        Handler handler = new Handler(NewGroupActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                AppManager.getInstance().restAPI.exitGroup(userId, threadId, new Callback<GroupInfoRes>() {
                    @Override
                    public void success(GroupInfoRes groupInfoRes, Response response) {
                        progressHUD.dismiss();
                        if (groupInfoRes.getErrorCode() == 0) {

                            List<GroupInfo> groupInfos = groupInfoRes.getGroupInfo();

                            getInfoRes(groupInfos, false);

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        handler.post(runnable);

    }

    private void updateGroup(final String threadId) {

        Handler handler = new Handler(NewGroupActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                if (isAdmin) {

                    TypedFile typedImage = null;
                    if (tempImageFile != null && AppManager.getInstance().isImageChanged()) {
                        typedImage = new TypedFile("image/jpeg", tempImageFile);
                    }

                    String isDel;
                    if (AppManager.getInstance().isImageDelete()){
                        isDel = "1";
                    }
                    else {
                        isDel = "0";
                    }

                    String ids = "", remove_ids = "", new_ids = "";
                    for (int i = 0; i < AppManager.getInstance().getGroupParticipants().size(); i++) {
                        ids += "," + AppManager.getInstance().getGroupParticipants().get(i).getUserId();
                    }
                    for (int i = 0; i < AppManager.getInstance().getNewlyAddedParticipants().size(); i++) {
                        new_ids += "," + AppManager.getInstance().getNewlyAddedParticipants().get(i).getUserId();
                    }
                    for (int i = 0; i < AppManager.getInstance().getRemovedParticipants().size(); i++) {
                        remove_ids += "," + AppManager.getInstance().getRemovedParticipants().get(i).getUserId();
                    }

                    if (!ids.equalsIgnoreCase("")) {
                        ids = ids.substring(1);
                    }
                    if (!new_ids.equalsIgnoreCase("")) {
                        new_ids = new_ids.substring(1);
                    }
                    if (!remove_ids.equalsIgnoreCase("")) {
                        remove_ids = remove_ids.substring(1);
                    }

                    String grupName = Functions.encodeToNonLossyAscii(txtGroupName.getText().toString());
                    if (previousGroup.equalsIgnoreCase(grupName)) {
                        grupName = "";
                    }

                    if (typedImage != null) {
                        AppManager.getInstance().restAPI.updateGroup(adminIds, userId, grupName, threadId,isDel, ids, new_ids, remove_ids, typedImage, new Callback<CreateGroupRes>() {
                            @Override
                            public void success(CreateGroupRes defaultRes, Response response) {


                                progressHUD.dismiss();
                                if (defaultRes.getErrorCode() == 0) {
                                    AppManager.getInstance().setIsImageChanged(false);
                                    AppManager.getInstance().setIsImageDelete(false);
                                    AppManager.getInstance().getRemovedParticipants().clear();
                                    AppManager.getInstance().getNewlyAddedParticipants().clear();
                                    Toast.makeText(NewGroupActivity.this, "Group Updated", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressHUD.dismiss();
                                Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        AppManager.getInstance().restAPI.updateGroupWithoutImage(adminIds, userId, grupName, threadId, ids, new_ids, remove_ids, new Callback<CreateGroupRes>() {
                            @Override
                            public void success(CreateGroupRes defaultRes, Response response) {
                                progressHUD.dismiss();
                                if (defaultRes.getErrorCode() == 0) {
                                    AppManager.getInstance().setIsImageChanged(false);
                                    AppManager.getInstance().setIsImageDelete(false);
                                    Toast.makeText(NewGroupActivity.this, "Group Updated", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressHUD.dismiss();
                                Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } else {
                    ///// non admin ////
                    TypedFile typedImage = null;
                    if (tempImageFile != null && AppManager.getInstance().isImageChanged()) {
                        typedImage = new TypedFile("image/jpeg", tempImageFile);
                    }

                    String grupName = Functions.encodeToNonLossyAscii(txtGroupName.getText().toString());
                    if (previousGroup.equalsIgnoreCase(grupName)) {
                        grupName = "";
                    }

                    String isDel;
                    if (AppManager.getInstance().isImageDelete()){
                        isDel = "1";
                    }
                    else {
                        isDel = "0";
                    }

                    if (typedImage != null) {
                        AppManager.getInstance().restAPI.updateGroupIconAndImage(userId, userId, grupName, threadId,isDel, typedImage, new Callback<UpdateGroupRes>() {
                            @Override
                            public void success(UpdateGroupRes defaultRes, Response response) {
                                progressHUD.dismiss();
                                if (defaultRes.getErrorCode() == 0) {
                                    AppManager.getInstance().setIsImageChanged(false);
                                    AppManager.getInstance().setIsImageDelete(false);
                                    Toast.makeText(NewGroupActivity.this, "Group Updated", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressHUD.dismiss();
                                Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if (!grupName.equalsIgnoreCase("")) {
                        AppManager.getInstance().restAPI.updateGroupIconAndImageWithoutImage(userId, userId, grupName, threadId, new Callback<UpdateGroupRes>() {
                            @Override
                            public void success(UpdateGroupRes defaultRes, Response response) {
                                progressHUD.dismiss();
                                if (defaultRes.getErrorCode() == 0) {
                                    AppManager.getInstance().setIsImageChanged(false);
                                    AppManager.getInstance().setIsImageDelete(false);
                                    Toast.makeText(NewGroupActivity.this, "Group Updated", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressHUD.dismiss();
                                Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    else {
                        progressHUD.dismiss();
                    }
                }
            }
        };
        handler.post(runnable);

    }

    private void createGroup() {

        Handler handler = new Handler(NewGroupActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);

                String userId = "";
                SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
                if (pref.contains(Constants.kUserID)) {
                    userId = pref.getString(Constants.kUserID, "");
                }

                TypedFile typedImage = null;
                if (tempImageFile != null && AppManager.getInstance().isImageChanged()) {
                    typedImage = new TypedFile("image/jpeg", tempImageFile);
                }

                String ids = "";
                for (int i = 0; i < AppManager.getInstance().getGroupParticipants().size(); i++) {
                    ids += "," + AppManager.getInstance().getGroupParticipants().get(i).getUserId();
                }
                ids += "," + userId;
                ids = ids.substring(1);

                String grupName = Functions.encodeToNonLossyAscii(txtGroupName.getText().toString());

                AppManager.getInstance().restAPI.createGroup(userId, userId, grupName, "", ids, typedImage, new Callback<CreateGroupRes>() {
                    @Override
                    public void success(CreateGroupRes defaultRes, Response response) {
                        progressHUD.dismiss();
                        if (defaultRes.getErrorCode() == 0) {
                            Toast.makeText(NewGroupActivity.this, "Group Created", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        handler.post(runnable);

    }

    private void getGroupInfoAPI(final String threadId, final boolean isTimer) {

        Handler handler = new Handler(NewGroupActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ProgressHUD progressHUD = null;
                if (!isTimer) {
                    progressHUD = ProgressHUD.show(context, "", true, true, null,false);
                }

                final ProgressHUD finalProgressHUD = progressHUD;
                AppManager.getInstance().restAPI.getGroupInfo(threadId, new Callback<GroupInfoRes>() {
                    @Override
                    public void success(GroupInfoRes groupInfoRes, Response response) {
                        if (!isTimer) {
                            finalProgressHUD.dismiss();
                        }
                        if (groupInfoRes.getErrorCode() == 0) {

                            Gson gson = new Gson();
                            oldInfoJsonValue = gson.toJson(groupInfoRes);

                            List<GroupInfo> groupInfos = groupInfoRes.getGroupInfo();

                            getInfoRes(groupInfos, isTimer);

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (!isTimer) {
                            finalProgressHUD.dismiss();
                            Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }
        };
        handler.post(runnable);
    }

    private void getInfoRes(List<GroupInfo> groupInfos, boolean isTimer) {

        if (groupInfos.size() > 0) {

            adminIds = groupInfos.get(0).getAdminId();
            getVocals.setAdminId(groupInfos.get(0).getAdminId());

            if (checkIsUserActive(groupInfos)) {
                btnExit.setVisibility(View.VISIBLE);
                btnAdd.setVisibility(View.VISIBLE);
                btnCreate.setVisibility(View.VISIBLE);
                txtGroupName.setEnabled(true);
                tvCount.setVisibility(View.VISIBLE);
                img.setEnabled(true);
            } else {
                btnExit.setVisibility(View.INVISIBLE);
                btnAdd.setVisibility(View.INVISIBLE);
                btnCreate.setVisibility(View.INVISIBLE);
                txtGroupName.setEnabled(false);
                tvCount.setVisibility(View.INVISIBLE);
                img.setEnabled(false);
            }

            if (checkIsAdmin(groupInfos.get(0).getAdminId())) {
                isAdmin = true;
                txtGroupName.setEnabled(true);
                btnCreate.setVisibility(View.VISIBLE);
                viewAdd.setVisibility(View.VISIBLE);
                lblPart.setVisibility(View.INVISIBLE);

            } else {
                isAdmin = false;
                viewAdd.setVisibility(View.GONE);
                lblPart.setVisibility(View.VISIBLE);
            }

            String name = Functions.decodeFromNonLossyAscii(groupInfos.get(0).getChatTitle());
            txtGroupName.setText(name);
            previousGroup = groupInfos.get(0).getChatTitle();
            tvCount.setText("" + (25 - name.length()));

            String url2 = Constants.GROUP_IMG_BASE_ADDRESS + getVocals.getThreadId() + "/" + groupInfos.get(0).getChatImage();
            new ThumbnailDownloadAndSave().execute(url2, groupInfos.get(0).getChatImage());

            String url = Constants.GROUP_IMG_BASE_ADDRESS + getVocals.getThreadId() + "/thumb_" + groupInfos.get(0).getChatImage();
            Picasso.with(NewGroupActivity.this).load(url).placeholder(R.drawable.friends_profile_pic_placeholder).into(img);

        }

        if (!isTimer) {
            String userId = "";
            SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
            if (pref.contains(Constants.kUserID)) {
                userId = pref.getString(Constants.kUserID, "");
            }

            groupMembers.clear();
            for (int i = 0; i < groupInfos.size(); i++) {
                if (groupInfos.get(i).getUserStatus() == null || !groupInfos.get(i).getUserStatus().equalsIgnoreCase("0")) {
                    groupMembers.add(groupInfos.get(i));
                }
            }
            tvGroupMemberCount.setText(groupInfos.size() + "/25");
            AppManager.getInstance().setGroupParticipants(groupMembers);
            CustomAdapter adapter = new CustomAdapter(context, R.layout.custom_group_cell, groupMembers);
            listView.setAdapter(adapter);
        }

    }

    private boolean checkIsUserActive(List<GroupInfo> groupInfos) {

        String userId = "";
        SharedPreferences pref = getSharedPreferences(Constants.USER_PREF, Activity.MODE_PRIVATE);
        if (pref.contains(Constants.kUserID)) {
            userId = pref.getString(Constants.kUserID, "");
        }

        for (int i = 0; i < groupInfos.size(); i++) {
            if (groupInfos.get(i).getUserId().equalsIgnoreCase(userId)) {
                return true;
            }
        }
        return false;

    }

    @Override
    public void onImageChosen(final ChosenImage image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
//                Log.i(TAG, "Chosen Image: O - " + image.getFilePathOriginal());
//                Log.i(TAG, "Chosen Image: T - " + image.getFileThumbnail());
//                Log.i(TAG, "Chosen Image: Ts - " + image.getFileThumbnailSmall());
//                isActivityResultOver = true;
                String originalFilePath = image.getFilePathOriginal();

                Intent intent = new Intent(context, CropActivity.class);
                intent.putExtra("Path", originalFilePath);
                startActivity(intent);
//                thumbnailFilePath = image.getFileThumbnail();
//                thumbnailSmallFilePath = image.getFileThumbnailSmall();
//                pbar.setVisibility(View.GONE);
//                if (image != null) {
////                    Log.i(TAG, "Chosen Image: Is not null");
////                    textViewFile.setText(image.getFilePathOriginal());
////                    loadImage(imageViewThumbnail, image.getFileThumbnail());
////                    loadImage(imageViewThumbSmall, image.getFileThumbnailSmall());
//                } else {
////                    Log.i(TAG, "Chosen Image: Is null");
//                }
            }
        });
    }

    @Override
    public void onError(String reason) {

    }

    @Override
    public void onImagesChosen(ChosenImages images) {

    }

    class CustomAdapter extends ArrayAdapter<GroupInfo> {

        Context context;
        List<GroupInfo> list;

        public CustomAdapter(Context context, int resource, List<GroupInfo> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        private class ViewHolder {
            TextView name;
            TextView gruopAdnmin;
            CircleImageView imgProfile;
            ImageView btnRmove;
            RelativeLayout relative;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder = null;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_group_cell, parent, false);

                holder = new ViewHolder();
                holder.name = (TextView) view.findViewById(R.id.etName);
                holder.gruopAdnmin = (TextView) view.findViewById(R.id.groupAdmin);
                holder.imgProfile = (CircleImageView) view.findViewById(R.id.imgProfile);
                holder.btnRmove = (ImageView) view.findViewById(R.id.btnRemove);
                holder.relative = (RelativeLayout) view.findViewById(R.id.relative);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.name.setTypeface(AppManager.getInstance().getRegularTypeface());

            final GroupInfo info = list.get(position);
            holder.name.setText(info.getUsername());

            if (isAdmin){
                holder.btnRmove.setVisibility(View.VISIBLE);
            }
            else {
                holder.btnRmove.setVisibility(View.INVISIBLE);
            }

            if (info.getIsAdmin() != null && info.getIsAdmin().equalsIgnoreCase("1")) {
                holder.gruopAdnmin.setVisibility(View.VISIBLE);
                holder.btnRmove.setVisibility(View.INVISIBLE);

            } else {
                holder.gruopAdnmin.setVisibility(View.GONE);
            }

            String url = Constants.IMG_BASE_ADDRESS + info.getUserId() + "/thumb_" + info.getUserImage();
            Picasso.with(NewGroupActivity.this).load(url).placeholder(R.drawable.friends_profile_pic_placeholder).into(holder.imgProfile);

            holder.relative.setOnClickListener(new View.OnClickListener() {
                int pos = position;

                @Override
                public void onClick(View v) {

                    if (isAdmin && (list.get(pos).getIsAdmin() == null || list.get(pos).getIsAdmin().equalsIgnoreCase("0"))) {
                        final CharSequence[] items = {"Make Group Admin", "Cancel"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
                        builder.setTitle("Admin");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (items[item].equals("Make Group Admin")) {

                                    makeGroupAdmin(getVocals.getThreadId(), list.get(pos).getUserId());

                                } else if (items[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    }

                }
            });

            holder.btnRmove.setOnClickListener(new View.OnClickListener() {
                int pos = position;

                @Override
                public void onClick(View v) {

                    for (int j = 0; j < AppManager.getInstance().getNewlyAddedParticipants().size(); j++) {
                        if (AppManager.getInstance().getNewlyAddedParticipants().get(j).getUserId().equalsIgnoreCase(list.get(pos).getUserId())) {
                            AppManager.getInstance().getNewlyAddedParticipants().remove(j);
                            break;
                        }
                    }
                    AppManager.getInstance().getRemovedParticipants().add(info);

                    list.remove(pos);
                    AppManager.getInstance().setGroupParticipants(list);
                    notifyDataSetChanged();

                }
            });

            return view;
        }
    }

    private void makeGroupAdmin(final String threadId, final String frndId) {

        Handler handler = new Handler(NewGroupActivity.this.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final ProgressHUD progressHUD = ProgressHUD.show(context, "", true, true, null,false);

                AppManager.getInstance().restAPI.makeGroupAdmin(threadId, frndId, new Callback<GroupInfoRes>() {
                    @Override
                    public void success(GroupInfoRes groupInfoRes, Response response) {
                        progressHUD.dismiss();
                        if (groupInfoRes.getErrorCode() == 0) {

                            List<GroupInfo> groupInfos = groupInfoRes.getGroupInfo();

                            getInfoRes(groupInfos, false);

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressHUD.dismiss();
                        Toast.makeText(NewGroupActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        };
        handler.post(runnable);

    }

    class CustomAdapterAdmin extends ArrayAdapter<FriendRequest> {

        Context context;
        List<FriendRequest> list;
        private LayoutInflater vi;

        public CustomAdapterAdmin(Context context, int resource, List<FriendRequest> objects) {
            super(context, resource, objects);

            this.context = context;
            this.list = objects;

            vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        private class ViewHolder {
            TextView name;
            TextView gruopAdnmin;
            CircleImageView imgProfile;
            ImageView btnRmove;
            RelativeLayout relative;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder = null;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.custom_group_cell, parent, false);

                holder = new ViewHolder();
                holder.name = (TextView) view.findViewById(R.id.etName);
                holder.imgProfile = (CircleImageView) view.findViewById(R.id.imgProfile);
                holder.btnRmove = (ImageView) view.findViewById(R.id.btnRemove);
                holder.gruopAdnmin = (TextView) view.findViewById(R.id.groupAdmin);
                holder.relative = (RelativeLayout) view.findViewById(R.id.relative);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.name.setTypeface(AppManager.getInstance().getRegularTypeface());

            final FriendRequest info = list.get(position);
            holder.name.setText(info.getFrndUsername());

            if (info.getIsAdmin() != null && info.getIsAdmin().equalsIgnoreCase("1")) {
                holder.gruopAdnmin.setVisibility(View.VISIBLE);
                holder.btnRmove.setVisibility(View.INVISIBLE);
            } else {
                holder.gruopAdnmin.setVisibility(View.GONE);
            }

            String url = Constants.IMG_BASE_ADDRESS + info.getUserId2() + "/thumb_" + info.getFrndImage();
            Picasso.with(NewGroupActivity.this).load(url).placeholder(R.drawable.friends_profile_pic_placeholder).into(holder.imgProfile);

            holder.btnRmove.setOnClickListener(new View.OnClickListener() {
                int pos = position;

                @Override
                public void onClick(View v) {

                    list.remove(pos);
//                    AppManager.getInstance().setGroupParticipants(list);
                    notifyDataSetChanged();

                }
            });

            holder.relative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (info.getIsAdmin() == null || info.getIsAdmin().equalsIgnoreCase("0")) {
                        final CharSequence[] items = {"Make Group Admin", "Cancel"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
                        builder.setTitle("Admin");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (items[item].equals("Make Group Admin")) {


                                } else if (items[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    }

                }
            });

            return view;
        }
    }

    private class ThumbnailDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                String imgName = arg0[1];
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();

                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }

                if (inputStream != null) {
                    byte[] byteImage = Functions.convertToByte(inputStream);

                    Functions.saveImageToExternalStorage(context, byteImage, imgName);
                }
            } catch (Exception io) {
                io.printStackTrace();
            }

            return null;
        }


    }

    private void chooseImage() {


        selectImage();

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Delete Photo", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    isCapture = true;

                    Intent startCustomCameraIntent = new Intent(context, CameraActivity.class);
//                    startActivity(startCustomCameraIntent);
                    startActivityForResult(startCustomCameraIntent, 1);
//                    TabGroupActivity parentActivity = (TabGroupActivity) getParent();
//                    parentActivity.startActivityForResult(startCustomCameraIntent, 1);


                } else if (items[item].equals("Choose from Library")) {
                    isCapture = false;


                    chooseImageFromGallary();


                } else if (items[item].equals("Delete Photo")) {

                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.friends_profile_pic_placeholder);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    largeIcon.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] byteArray = stream.toByteArray();
                    Functions.saveImageToExternalStorage(context, byteArray, "profile_pic");
                    img.setImageBitmap(largeIcon);
//                    tempImageFile  =new File(Functions.getImage("profile_pic"));
//                    AppManager.getInstance().setImg(largeIcon);
                    AppManager.getInstance().setImgFile(new File(Functions.getImage(context, "profile_pic")));
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void chooseImageFromGallary() {
        imageChooserManager = new ImageChooserManager(context,
                291, false);
        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
//            pbar.setVisibility(View.VISIBLE);
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == 1) && (resultCode == RESULT_OK)) {


            Uri photoUri = data.getData();
            // Get the bitmap in according to the width of the device
            Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(photoUri.getPath(), mSize.x, mSize.x);

            Intent intent = new Intent(context, CropActivity.class);
            intent.putExtra("Path", photoUri.getPath());
            startActivity(intent);


        } else if ((requestCode == 291) && (resultCode == RESULT_OK)) {


            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);

        }
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(context, 291, false);
        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            if (getVocals != null && getVocals.getThreadId() != null && !getVocals.getThreadId().equalsIgnoreCase("")) {
                getGroupInfoAPI(getVocals.getThreadId(), true);
            }
        }

    }

}
