package Util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;

import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import API.VMemosAPI;
import Model.GetVocals;
import Model.GroupInfo;
import Model.NotificationSound;
import Model.PushSetting;
import Model.User;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class AppManager {
    private static AppManager ourInstance;

    OkHttpClient client = new OkHttpClient();
    public RestAdapter restAdapter = new RestAdapter.Builder().setClient(new OkClient(client)).setEndpoint(VMemosAPI.API).build();
    public VMemosAPI restAPI = restAdapter.create(VMemosAPI.class);

    private Typeface headingTypeface;
    private Typeface regularTypeface;
    private Typeface textMsgTypeface;

    private User user;
    private List<NotificationSound> notificationSounds = new ArrayList<>();
    private List<GroupInfo> groupParticipants = new ArrayList<>();
    private List<GroupInfo> removedParticipants = new ArrayList<>();
    private List<GroupInfo> newlyAddedParticipants = new ArrayList<>();
    private File imgFile;
    private Bitmap img;
    private Bitmap cropImage;
    private PushSetting pushSetting;
    private String msgTone;
    private String groupTone;
    private String currentChatThreadID;
    private boolean isChatActive;
    private boolean isVocalActive;
    private GetVocals chatPuchData;
    private boolean isImageChanged;
    private boolean isImageDelete;


    public Typeface getHeadingTypeface() {
        return headingTypeface;
    }

    public void setHeadingTypeface(Typeface headingTypeface) {
        this.headingTypeface = headingTypeface;
    }

    public Typeface getRegularTypeface() {
        return regularTypeface;
    }

    public void setRegularTypeface(Typeface regularTypeface) {
        this.regularTypeface = regularTypeface;
    }

    public Typeface getTextMsgTypeface() {
        return textMsgTypeface;
    }

    public void setTextMsgTypeface(Typeface regularTypeface) {
        this.textMsgTypeface = regularTypeface;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<NotificationSound> getNotificationSounds() {
        return notificationSounds;
    }

    public void setNotificationSounds(List<NotificationSound> notificationSounds) {
        this.notificationSounds = notificationSounds;
    }

    public List<GroupInfo> getGroupParticipants() {
        return groupParticipants;
    }

    public void setGroupParticipants(List<GroupInfo> groupParticipants) {
        this.groupParticipants = groupParticipants;
    }

    public boolean isImageDelete() {
        return isImageDelete;
    }

    public void setIsImageDelete(boolean isImageDelete) {
        this.isImageDelete = isImageDelete;
    }

    public boolean isImageChanged() {
        return isImageChanged;
    }

    public void setIsImageChanged(boolean isImageChanged) {
        this.isImageChanged = isImageChanged;
    }

    public boolean isVocalActive() {
        return isVocalActive;
    }

    public void setIsVocalActive(boolean isVocalActive) {
        this.isVocalActive = isVocalActive;
    }

    public boolean isChatActive() {
        return isChatActive;
    }


    public GetVocals getChatPuchData() {
        return chatPuchData;
    }

    public void setChatPuchData(GetVocals chatPuchData) {
        this.chatPuchData = chatPuchData;
    }

    public String getCurrentChatThreadID() {
        return currentChatThreadID;
    }

    public void setCurrentChatThreadID(String currentChatThreadID) {
        this.currentChatThreadID = currentChatThreadID;
    }

    public List<GroupInfo> getRemovedParticipants() {
        return removedParticipants;
    }

    public void setRemovedParticipants(List<GroupInfo> removedParticipants) {
        this.removedParticipants = removedParticipants;
    }

    public List<GroupInfo> getNewlyAddedParticipants() {
        return newlyAddedParticipants;
    }

    public void setNewlyAddedParticipants(List<GroupInfo> newlyAddedParticipants) {
        this.newlyAddedParticipants = newlyAddedParticipants;
    }

    public void setIsChatActive(boolean isActive) {
        this.isChatActive = isActive;
    }

    public PushSetting getPushSetting() {
        return pushSetting;
    }

    public String getMsgTone() {
        return msgTone;
    }

    public void setMsgTone(String msgTone) {
        this.msgTone = msgTone;
    }

    public String getGroupTone() {
        return groupTone;
    }

    public void setGroupTone(String groupTone) {
        this.groupTone = groupTone;
    }

    public void setPushSetting(PushSetting pushSetting) {
        this.pushSetting = pushSetting;
    }

    public File getImgFile() {
        return imgFile;
    }

    public void setImgFile(File imgFile) {
        this.imgFile = imgFile;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public Bitmap getCropImage() {
        return cropImage;
    }

    public void setCropImage(Bitmap cropImage) {
        this.cropImage = cropImage;
    }

    public static AppManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new AppManager();

        }
        return ourInstance;
    }

    public void setFont(Context ctx)
    {
        if (ctx != null) {
            regularTypeface = Typeface.createFromAsset(ctx.getAssets(), "font/MyriadPro-Regular.ttf");
            headingTypeface = Typeface.createFromAsset(ctx.getAssets(), "font/MyriadPro-Semibold.otf");
            textMsgTypeface = Typeface.createFromAsset(ctx.getAssets(), "font/MuseoSans300.ttf");
        }
    }

    private AppManager() {
    }
}
