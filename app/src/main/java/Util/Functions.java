package Util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Model.PushSetting;
import Model.User;

/**
 * Created by developerclan on 08/02/2016.
 */
public class Functions {

    private static final Pattern UNICODE_HEX_PATTERN = Pattern.compile("\\\\u([0-9A-Fa-f]{4})");
    private static final Pattern UNICODE_OCT_PATTERN = Pattern.compile("\\\\([0-7]{3})");

    public static int GetPixalFromDP(Context context,float dp){
        Resources r = context.getResources();
        int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return px;
    }

    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static String decodeFromNonLossyAscii(String original) {
        if (original != null && !original.equalsIgnoreCase("")) {
            Matcher matcher = UNICODE_HEX_PATTERN.matcher(original);
            StringBuffer charBuffer = new StringBuffer(original.length());
            while (matcher.find()) {
                String match = matcher.group(1);
                char unicodeChar = (char) Integer.parseInt(match, 16);
                matcher.appendReplacement(charBuffer, Character.toString(unicodeChar));
            }
            matcher.appendTail(charBuffer);
            String parsedUnicode = charBuffer.toString();

            matcher = UNICODE_OCT_PATTERN.matcher(parsedUnicode);
            charBuffer = new StringBuffer(parsedUnicode.length());
            while (matcher.find()) {
                String match = matcher.group(1);
                char unicodeChar = (char) Integer.parseInt(match, 8);
                matcher.appendReplacement(charBuffer, Character.toString(unicodeChar));
            }
            matcher.appendTail(charBuffer);
            return charBuffer.toString();
        }
        else {
            return "";
        }
    }

    public static String encodeToNonLossyAscii(String original) {
        Charset asciiCharset = Charset.forName("US-ASCII");
        if (asciiCharset.newEncoder().canEncode(original)) {
            return original;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < original.length(); i++) {
            char c = original.charAt(i);
            if (c < 128) {
                stringBuffer.append(c);
            } else if (c < 256) {
                String octal = Integer.toOctalString(c);
                stringBuffer.append("\\");
                stringBuffer.append(octal);
            } else {
                String hex = Integer.toHexString(c);
                stringBuffer.append("\\u");
                stringBuffer.append(hex);
            }
        }
        return stringBuffer.toString();
    }

    public static byte[] convertToByte(InputStream input) throws Exception {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }

        byte[] arr = output.toByteArray();

        output.close();
        output = null;

        return arr;
    }

    public static void saveImageToExternalStorage(Context context, byte[] image, String name) {

        String path = context.getFilesDir().getAbsolutePath() +"/thumbnails/";

        try {
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            OutputStream fOut = null;
            File file = new File(path, name);

            if (!file.exists()) {
                file.createNewFile();
            }
            fOut = new FileOutputStream(file);

// 100 means no compression, the lower you go, the stronger the compression
//            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.write(image);
            fOut.flush();
            fOut.close();


        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }
    }




    public static String getImage(Context context, String name){

        String path = context.getFilesDir().getAbsolutePath() +"/thumbnails/";
        return  path+name;

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static PushSetting getPushSettings(User user){

        PushSetting setting = new PushSetting();
        setting.setMsgPush(user.getMessagePush());
        setting.setMsgPushSound(user.getMessagePushSound());
        setting.setGroupMsgPush(user.getGroupMessagePush());
        setting.setGroupMsgPushSound(user.getGroupMessagePushSound());

        return setting;
    }

    public static String getTopActivity(Context context){
        ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        return componentInfo.getShortClassName();
    }

    public static int getAppVersion(Context context) {
        int mode = Activity.MODE_PRIVATE;
        SharedPreferences preferences = context.getSharedPreferences(Constants.MYPREFS, mode);
        return preferences.getInt(Constants.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
    }

}
