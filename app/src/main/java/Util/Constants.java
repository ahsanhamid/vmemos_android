package Util;

/**
 * Created by developerclan on 22/02/2016.
 */
public class Constants {

    public static String MYPREFS = "Preferences";
    public static String PROPERTY_APP_VERSION = "appVersion";
    public static String PROPERTY_REG_ID = "registration_id";


    public static int NUM_OF_ITEMS_TO_ADD = 25;


//    public static String SENDER_ID = "537540694418";

    public static String CONNECTED = "Connected";
    public static String NotCONNECTED = "Not Connected";

    public static String FROM_VOCAL = "FROM_VOCAL";
    public static String FROM_GROUP = "FROM_GROUP";
    public static String FROM_CHAT = "FROM_CHAT";
    public static String FROM_FRND = "FROM_FRND";
    public static String FROM_PROFILE = "FROM_PROFILE";
    public static String URL = "URL";
    public static String FOR_MSG = "forMsg";

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static String DEVICE_TYPE = "android";

    public static int AUDIO_DURATION_LIMIT = 10;
    public static String FILE_PATH = "vMemos"; //Environment.getExternalStorageDirectory()


    public static String USER_PREF = "user_pref";

    public static String kUserID = "userID";
    public static String kUserName = "userName";
    public static String kPassword = "password";
    public static String kImage = "image";
    public static String kUserStatus = "status";
    public static String kUserArray = "array";
    public static String kSounds = "sounds";
    public static String kDeviceToken = "devicetoken";
    public static String kName = "name";

    public static String kUserActive = "ACTIVE";
    public static String kUserInActive = "INACTIVE";

    public static String kStatusUpload = "1";
    public static String kStatusDelivered = "2";
    public static String kStatusListened = "3";
    public static String kStatusSaved = "4";
    public static String kStatusDelete = "5";

    public static String TRUE = "true";
    public static String FALSE = "false";

    //APIs BASE ADDRESS
    public static String API_BASE_ADDRESS = "http://vmemos.com/index.php/api/";
    public static String IMG_BASE_ADDRESS = "http://vmemos.com/uploads/app_users/";
    public static String GROUP_IMG_BASE_ADDRESS = "http://vmemos.com/uploads/group_media/";
    public static String CHAT_IMG_BASE_ADDRESS = "http://vmemos.com/uploads";
    public static String VOCAL_BASE_ADDRESS = "http://vmemos.com/uploads/vocals/";

    // DAQTABASE
    public static String DB_NAME = "db_vmemo";
    public static int DB_VERSION = 2;

    //TABLE NAME
    public static String TBL_JSON_RESPONCE = "tbl_jsonresponce";
    public static String TBL_FRIEND_CHAT = "tbl_friendchat";

    //TBL_JSONRESPONCE Column names
    public static String KEY_RESPONCE = "responce";
    public static String KEY_URL = "url";
    public static String KEY_USER_ID = "user_id";

    //TBL_FRIEND_CHAT Column names
    public static String KEY_CHAT_TITLE = "chat_title";
    public static String KEY_COLOR = "color";
    public static String KEY_DOWNLOAD_STATUS = "download_status";
    public static String KEY_DURATION = "duration";
    public static String KEY_FILE_PATH = "file_path";
    public static String KEY_FRIEND_CHAT_ID = "frndchat_id";
    public static String KEY_IS_DOWNLOAD_ERROR = "is_download_error";
    public static String KEY_IS_GROUP = "is_group";
    public static String KEY_IS_RESEND_ERROR = "is_resend_error";
    public static String KEY_MSG_ID = "msg_id";
    public static String KEY_MSG_STATUS = "msg_status";
    public static String KEY_RECIEVER_ID = "reciever_id";
    public static String KEY_SENDER_ID = "sender_id";
    public static String KEY_SENDER_NAME = "sender_name";
    public static String KEY_THREAD_ID = "thread_id";
    public static String KEY_TEXT_MSG_STATUS = "text_message_status";
    public static String KEY_USER_GROUP_STATUS_ON_MSG_POST = "user_group_status_on_msg_post";
    public static String KEY_MSG_TEXT = "message_text";

    //APIs NAME
    public static String kUserSignupAPI = "registerUser";
    public static String kUserSigninAPI = "loginApplicationUser";
    public static String kUserReSendVeerifyCodeAPI = "resendVerificationCode";
    public static String kUserVerifyCodeAPI = "verifyVerificationCode";
    public static String kUserReSendCaptchaAPI = "refreshCaptcha";
    public static String kUserVerifyCaptchaAPI = "verifyCaptcha";
    public static String kUpdateProfile = "updateProfile";
    public static String kPostVocal = "postVocal";
    public static String kGetAllVocals = "getAllVocals";
    public static String kRecieveThread = "receiveThread";
    public static String kSearchUsers = "searchUsersByUsername";
    public static String kSearchUsersEmail = "searchUsersByEmail";
    public static String kSendFriendRequest = "sendFriendRequest";
    public static String kgetFriendRequest = "getAllFriendsAndRequests";
    public static String kAcceptFriendRequest = "acceptFriendRequest";
    public static String kRejectFriendRequest = "rejectFriendRequest";
    public static String kRemoveFriend = "unFriend";
    public static String kForgotPass = "forgotPassword";
    public static String kUpdateVocalStatus = "updateVocalStatus";
    public static String kUpdateDeviceToken = "updateDeviceToken";
    public static String kUpdateEmail = "updateEmail";
    public static String kUpdateContactNumber = "updateContactNumber";
    public static String kUpdateNotificationSettings = "updateNotificationSettings";
    public static String kUpdateGroupSettings = "updateGroupSettings";
    public static String kGetGroupInfo = "getGroupInfo";

}
